 // COPY 
 //alfa-image-transform-ui 19.3.6NEW   https://stackoverflow.com/questions/46647138/zoom-in-on-a-mousewheel-point-using-scale-and-translate

//  import showInfo from './alfa-image-transform-ui/show-info.js'; // forDEBUG
import AlfaConsoleLog from './alfa-console-log.mjs'; 
var alfaConsoleLog = new AlfaConsoleLog('alfa-image-transform-ui')
var log =     alfaConsoleLog.log.bind(alfaConsoleLog)
var logWarn = alfaConsoleLog.logWarn.bind(alfaConsoleLog)
var logErr =  alfaConsoleLog.logErr.bind(alfaConsoleLog)


 const $dce = document.createElement.bind(document)
 const $dqs = document.querySelector.bind(document)
 
/**
 * 
 * @param {Object} $viewport - !!jQuery element 
 * @param {Object} opts - see defaults
 */
function ImageTransform($viewport, callbacks, opts){

    callbacks = callbacks || {}
    callbacks.onload = callbacks.onload || function(){}
    callbacks.showInfo = callbacks.showInfo || function(){}
    callbacks.zoomSetting = callbacks.zoomSetting || function(){}
    this.callbacks = callbacks;

    // var self = this;
    opts = opts || {};

    var defaults = {
        maxScale: 16,
        scaleStepWheel: 0.25,    // scale increment added for each mouse wheel click
        scaleStepClick: 2,      // scale multiplier for each mouse btn click
        transition: '1s',       // animation time for mouse btn click
        arrowStep: 20           // keyboard arrows movement steps in px
    };
//  this.cfg = $.extend({}, opts, defaults);
    this.cfg = Object.assign({}, opts, defaults)

    this.$viewport = $viewport;   
    build_html.call(this, opts);

    this.pos = {x: 0, y: 0};
    this.scale = 1;
    this.imageRotation = 0

    // image events
    this.$image.onload = function(){
        // self.updateImageData.call(self);  
        this.updateImageData.call(this); 
        this.callbacks.onload()
    }.bind(this);
    this.$image.onerror = function(e){
        console.error(e);
    };

    // mouse events
    // this.$container.on('wheel DOMMouseScroll', function(e){
    
    //  this.$container.on('wheel', function(e){
    this.$container.addEventListener('wheel', function(ev){
        ev.preventDefault(); // prevent page scrolling
        // self.zoom(ev, 'wheel');
        this.zoom(ev, 'wheel');
    }.bind(this));


    /** events */

    // this.$container.click(function(e){
    this.$container.addEventListener('click', function(e){
        if (e.ctrlKey) {this.clear(); return;} // --- >

        var originalScale = false;
        var zoom = 1; // zoom in
        if(e.altKey) zoom = -1; // zoom out
        if(e.shiftKey) originalScale = true;

        this.zoom(e, 'click', {zoom, originalScale});
    }.bind(this));


    $dqs('body').addEventListener("keydown", function(ev) {
        // console.log('ev.key=', ev.key);
        
        switch(ev.key){
            case 'ArrowRight':
            case 'ArrowLeft':
            case 'ArrowUp':
            case 'ArrowDown':
                this.arrow(ev.key); break;
            case 'r':
            case 'R':
                this.rotate(90); break;
        }
    }.bind(this))
        

}//end: ImageTransform() Constructor

ImageTransform.prototype.rotate = function(deg){

    this.imageRotation += deg

    this.$image.style.transformOrigin = '50% 50%'
    this.$image.style.transform = `rotate(${this.imageRotation}deg)`

    // var w = this.$image.naturalWidth
    // var h = this.$image.naturalHeight

    // this.$image.naturalWidth = h
    // this.$image.naturalHeight = w

    // this.updateImageData()
}
ImageTransform.prototype.clearPreviousTransforms = function(){
    if(this.imageRotation!==0){
        this.imageRotation = 0
        this.$image.style.visibility = 'hidden'
        this.$image.style.transformOrigin = 'unset'
        this.$image.style.transform = 'rotate(0deg)'
        /** make visible in onload event of other image */
        // this.$image.style.visibility = 'unset'
    }
}
 
/**
 * 
 * @param {*} image OPTIONAL, needed when called from image-navigator
 */
ImageTransform.prototype.updateImageData = function(image){
     
    var img = image ? image : this.$image;

    // this.size = {w: $(img).width(), h: $(img).height()}; // NOTE: image is stretched to container, this is a size of container
    this.size = {w: img.clientWidth, h: img.clientHeight}; // NOTE: image is stretched to container, this is a size of container

    // 1. find how image fits to viewport - margins, y=vertical or x=horizontal
    this.renderedImgInfo = this.getRenderedImgInfo(img); // pass optional image, it can be from image-navigator, not from here
    this.marg = {
        y: this.renderedImgInfo.top, // y=height of margin, margins are Horizontal above and below image, 
        x: this.renderedImgInfo.left // x=width of margin,  margins are Vertical on left and right of image, 
    };

    // 2. what is the scale of image in container
    if (this.marg.y){ this.scaleInContainer = img.naturalWidth / this.size.w; }
    if (this.marg.x){ this.scaleInContainer = img.naturalHeight / this.size.h; }
    if (this.marg.x===0 & this.marg.y===0) { 
        this.scaleInContainer = img.naturalWidth / this.size.w; 
    }
    /** 3. make sure that image is visible after cleanup of previous */
    this.$image.style.visibility = 'unset'

    log(`updateImageData: ${img.getAttribute("src")}\n`, 'renderedImgInfo:', this.renderedImgInfo, 'scaleInContainer:', this.scaleInContainer); // log( 'marg y:', !!this.marg.y, ', x:',!!this.marg.x);

};

function build_html(opts){

    /** 1. <img img-scalable> */
    this.$image = $dce('img');
    this.$image.style = 'width: 100%; height: 100%; object-fit: contain';

    /** 2. <div scalable-img-container> */

    this.$container = $dce('div')
    this.$container.classList.add('zoom-container')
    this.$container.style = 'height: 100%; transform-origin: 0 0'

    // if (!opts.fromNavigator) {
    //     // this.$container.css({'height':'100%'}); !! height is NOT SET ????
    //     // this.$container.height('100%');
    //     log('fromNavigator');
    // } 
    this.$container.append(this.$image);
    // 3. 
    this.$viewport.append(this.$container);

}

/** LOAD
 * load image to <img> $image
 * @param {*} imgUrl 
 */
ImageTransform.prototype.load = function(image, opts){

    this.clear(opts);

    if (typeof image ==='string') {
        var imgUrl = image;
        this.clearPreviousTransforms()
        this.$image.src = imgUrl; // NOTE: img.onload function MUST preceed the actual load on this line. onload event runs updateImageData()
    }
    else {
console.error('TODO');
    }

};

/** ZOOM
 * 
 * @param {Event} e - click or wheel event
 * @param {String} mode - wheel|click|set
 * @param {Object} opts - options
 */
ImageTransform.prototype.zoom = function(e, mode, opts){
    var log=function(){}; //forDEBUG
    opts = opts || {};
    // var mode = opts.mode;

    var zoom_target = {x: 0, y: 0};
    var zoom_point =  {x: 0, y: 0};

    var center = {x: this.$container.clientWidth/2, y: this.$container.clientHeight/2}; // center of container
    var offset = this.$viewport.getBoundingClientRect()

    // zoom_point is relative to container:
    if (mode=='set'){
        zoom_point = opts.zoom_point;
    } else {
        zoom_point.x = e.pageX - offset.left;
        zoom_point.y = e.pageY - offset.top;
    }
    

    // 1. find zoom delta (in or out) based on mouse wheel direction or click with ALT
    var delta;
    switch (mode){
        // case 'wheel': delta = e.originalEvent.deltaY; break;
        case 'wheel': delta = e.deltaY; break;
        case 'click': delta = opts.zoom || 1; break;
        case 'set':   delta = 1; break;
        default: console.error('mode:`', mode, '` unknown');
    }
    delta = Math.max(-1, Math.min(1, delta)); // cap the delta to [-1,1] for cross browser consistency

log('%cmarg', 'color:magenta', this.marg, 'mode:', mode);  // NOTE: scale is not set yet , 'scale:', this.scale
log('INIT pos', this.pos);

    // 2. determine the point on where the image is zoomed in
    zoom_target.x = (zoom_point.x - this.pos.x)/this.scale;
    zoom_target.y = (zoom_point.y - this.pos.y)/this.scale;


    // 3. set scale value
    var newScale;
    switch (mode){
        case 'wheel': newScale = this.scale + this.cfg.scaleStepWheel * delta; break;
        case 'click':
            if (opts.originalScale){
                newScale = this.scaleInContainer;
            } else {
                if (delta > 0) newScale = this.scale * this.cfg.scaleStepClick;
                if (delta < 0) newScale = this.scale / this.cfg.scaleStepClick;
            }
            break;
        case 'set': 
            newScale = opts.scale;
            // this.cfg.transition = opts.transition ? opts.transition : this.cfg.transition;
    }
    if (newScale < 1 ) return;
    if (newScale > this.cfg.maxScale) {log('maxScale'); return; }

    // 3a. make sure that image area, not margin is clicked, needed only when scale=1
    if (this.scale==1){
        var imgAndMarg = {w: this.renderedImgInfo.width + this.marg.x, h:this.renderedImgInfo.height + this.marg.y};
        if (zoom_point.x > imgAndMarg.w || zoom_point.x < this.marg.x) {log('click out of img x:', zoom_point.x); return; }                                            // --- >
        if (zoom_point.y > imgAndMarg.h || zoom_point.y < this.marg.y) {log('click out of img y:', zoom_point.y); return; }
    }
    this.scale = newScale;


    // 4. calculate pos x and y based on zoom
    this.pos.x = -zoom_target.x * this.scale + zoom_point.x;
    this.pos.y = -zoom_target.y * this.scale + zoom_point.y;
    log('ZOOM pos', this.pos, 'zoom:', zoom_target);

    // 5. center on clicked point
    if (mode=='click') {
        this.pos.x += (center.x - zoom_point.x); 
        this.pos.y += (center.y - zoom_point.y); 
    }
    log('CENT pos', this.pos, 'scale:', this.scale);

    // *********************************************************************
    // 6. Make sure the image stays in its container area when zooming out
    
    
    adjustEdge.call(this);

    function adjustEdge(){
        var adj='ADJ: ';

        var quadClicked = this.quadClicked(zoom_point, this.$container);

        // 6a. horizontal adjustments
        // 6aa. left
        if (this.marg.x && mode=='click' && quadClicked.left){
            if (this.pos.x > -this.marg.x * this.scale) {
                this.pos.x = - this.marg.x * this.scale;
                adj +='left+marg';
            }
        } else {
            if(this.pos.x > 0) {
                this.pos.x = 0;
                adj +='left';
            }
        }
        // 6ab. right
        if (this.marg.x && mode=='click'  && quadClicked.right){
            var w = this.size.w - this.marg.x;
            if(this.pos.x + w*this.scale < this.size.w) {
                this.pos.x =  -(w*this.scale - w) + this.marg.x; 
                adj +=`right+marg w=${w}`;  
            }
        } else {
            if(this.pos.x + this.size.w * this.scale < this.size.w) {
                this.pos.x = -this.size.w * (this.scale-1);
                adj +='right';
            }
        }

        // 6b. vertical adjustments
        // 6ba. top 
        if (this.marg.y && mode=='click' && quadClicked.top){
            if (this.pos.y > -this.marg.y * this.scale) {
                this.pos.y = - this.marg.y * this.scale;
                adj +=' - top+marg';
            }
        } else {
            if(this.pos.y > 0) {
                this.pos.y = 0;
                adj +=' - top';
            }
        }
        // 6bb. bottom 
        if (this.marg.y && mode=='click' && quadClicked.bottom){
            var h = this.size.h - this.marg.y;
            if(this.pos.y + h*this.scale < this.size.h) {
                this.pos.y = -(h*this.scale - h) + this.marg.y;
                adj +=` - bott+marg h=${h}`;  
            }
        } else {
            if(this.pos.y + this.size.h*this.scale < this.size.h) {
                this.pos.y = -this.size.h * (this.scale-1);
                adj +=' - bott';
            }
        }
        log('EDGE pos', this.pos, adj);
    }

    // record zoom setting
    this.callbacks.zoomSetting({scale: this.scale, zoom_target})


    // 7. make CSS transform
    switch (mode){
        case 'wheel': this.$container.style.transition = 'unset'; break;
        case 'click': this.$container.style.transition = this.cfg.transition; break;
        case 'set':   this.$container.style.transition = opts.transition ? opts.transition : this.cfg.transition;
    }
    // log('mode', mode, 'transition:', this.$container[0].style.transition);

    // update transform
    // this.$container.css('transform', 'translate('+(this.pos.x)+'px, '+(this.pos.y)+'px) scale('+this.scale+','+this.scale+')');
    this.$container.style.transform = 'translate('+(this.pos.x)+'px, '+(this.pos.y)+'px) scale('+this.scale+','+this.scale+')';

    this.callbacks.showInfo.call(this, delta, zoom_point, zoom_target, center); // forDEBUG

    // var self=this; setTimeout(function(){ log('renderedImgInfo', self.renderedImgInfo);}, 1500);

};//end: ImageTransform.prototype.zoom()


ImageTransform.prototype.quadClicked = function  (clk, $rectElem){
        // which quadrant is clicked
    var clkQuad = {
        top:    clk.y < $rectElem.clientHeight/2 ? true : false,
        bottom: clk.y > $rectElem.clientHeight/2 ? true : false,
        left:   clk.x < $rectElem.clientWidth/2 ? true : false,
        right:  clk.x > $rectElem.clientWidth/2 ? true : false,
    };
    var quadTxt = clkQuad.top ? 'top' : clkQuad.bottom ? 'bott' : 'mid';
    quadTxt += clkQuad.left ? '-left' : clkQuad.right ? '-right': '-mid'; 
    log('quad=', quadTxt, clk);
    return clkQuad;
};

/**
 * move image using keyboard arrows
 * @param {*} arrow 
 */
ImageTransform.prototype.arrow = function(arrow){
    switch (arrow){
        case 'ArrowRight':  this.pos.x += this.cfg.arrowStep; break;
        case 'ArrowLeft':   this.pos.x -= this.cfg.arrowStep; break;
        case 'ArrowUp':     this.pos.y -= this.cfg.arrowStep; break;
        case 'ArrowDown':   this.pos.y += this.cfg.arrowStep; break;
    }

    this.$container.style.transition = 'unset';
    this.$container.style.transform = `translate(${(this.pos.x)+'px'}, ${(this.pos.y)+'px'}) scale(${this.scale}, ${this.scale})`;

    log('pos', this.pos);
};

ImageTransform.prototype.setZoom = function(settings){

};

/** clear transformations */
ImageTransform.prototype.clear = function(opts){
    opts = opts || {};
    this.scale = 1;
    this.pos = {x: 0, y: 0};

    var transition = opts.noTransition ? 'none' : this.cfg.transition;

    this.$container.style.transition = 'transition'
    this.$container.style.transform = 'translate(0, 0) scale(1, 1)' 

this.callbacks.showInfo.call(this, 0, null, null, null);
};
  
/**
 * get image size after being fit inside container
 * NOTE: returned value does not change after CSS transform
 *  @param {*} image OPTIONAL, needed when called from image-navigator
 */
ImageTransform.prototype.getRenderedImgInfo = function (image) {
    var img = image ? image : this.$image;
    var position = window.getComputedStyle(img).getPropertyValue('object-position').split(' ');
    // return this.getRenderedSize(
    return getRenderedSize(
                true,               // object-fit: contain (chyba?)
                img.width,          // scaled img width in container
                img.height,         // scaled img height in container
                img.naturalWidth,   // original image size
                img.naturalHeight,  // original image size
                position
    );
    
    function getRenderedSize(contains, cWidth, cHeight, width, height, position){
        var oRatio = width / height,
            cRatio = cWidth / cHeight,
            pos0 = parseInt(position[0]),
            pos1 = parseInt(position[1]);
        return function() {
            if (contains ? (oRatio > cRatio) : (oRatio < cRatio)) {
                this.width = cWidth;
                this.height = cWidth / oRatio;
            } else {
                this.width = cHeight * oRatio;
                this.height = cHeight;
            }      
            this.left = (cWidth - this.width)*(pos0/100);
            this.right = this.width + this.left;

            this.top = (cHeight - this.height)*(pos1/100);
            this.bottom = this.height + this.top;

            // this.position = position;
            return this;
        }.call({});
    }
};


export default ImageTransform

/**  helper */
