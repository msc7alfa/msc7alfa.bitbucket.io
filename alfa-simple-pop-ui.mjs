/** alfa-simple-pop-ui.mjs
 * shared function for other ui elements 
 * this message box is attached to an existing container  */

const $dce = document.createElement.bind(document); 

/** internal message box */
var popMessage = function(html, opts){

    /** expected, that container */
    if(!this.container) {console.error('[alfa-simple-pop-ui.mjs] this.container is falsy!', html ); return;}

    /** create object only once on first use */
    if(!this.popMessageBox){
        var msgBox = $dce('div')
        msgBox.style = `
        display: 'none';
        position: absolute; 
        top:10px; left:0; right:0; 
        width: 60%; 
        margin: 0 auto; 
        color: black;
        border: 2px solid red; 
        background: lightyellow;
        padding: 1em; 
        text-align: center;`;
        msgBox.addEventListener('click', function(ev){
            if(ev.target.innerHTML === '✖')
                ev.target.parentElement.style.display = 'none'
            else    
                ev.target.style.display = 'none'
        })

        this.container.style.position = 'relative'
        this.container.append(msgBox)
        this.popMessageBox = msgBox
    }
    this.popMessageBox.style.display = 'block'
    this.popMessageBox.innerHTML = html;

    /** add close symbol after html of the message is applied */
    var eks = $dce('div')
        eks.style = 'position: absolute; top: 0; right: 6px; cursor: pointer'
        eks.innerHTML = '✖'; // '&#x2716'
        this.popMessageBox.append(eks)
}

export default popMessage;
