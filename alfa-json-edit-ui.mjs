/** example for UI element */

const $dqs = document.querySelector.bind(document); 
const $dce = document.createElement.bind(document); 


/** constructor 
 * 
 *  @param {String} elementSelector - a CSS selector, unique for element to create
 *  @param {Object} options - {onValidated: fnCallback(validatedObj)}
 */
function AlfaJsonEdit(elementSelector, options){

    var defaults = {
        maxRows: 25,
        author: 'alfalabs.net',
        name: 'AlfaJsonEdit'
    }

    /** ui element has an element on page */
    this.elementSelector = elementSelector;
    this.container = $dqs(elementSelector)

    this.getCfgValues(options, defaults)  /** element configuration comes from options and html attributes */
   

    this.createElement()
    this.insertStyleTag()

}
/** get configuration from defauls, options and HTML element attributes  */
AlfaJsonEdit.prototype.getCfgValues = function(options, defaults){

    options = options || {}
    
    /** assing simple properties */
    this.cfg = Object.assign({}, defaults, options)
    
    /** assign complex properties */
    this.cfg.onValidated = options.onValidated || function(){};

    /** get cfg from html element attributes */
    Array.from(this.container.attributes).forEach(function(attr){
        var name = camelize(attr.name)
        this.cfg[name] = attr.value
    }.bind(this));

    // log('getAttributes() this.cfg:', this.cfg)
}

AlfaJsonEdit.prototype.load = function(obj, opts){
    
    var json = JSON.stringify(obj, null, 4);
    /** count lines */
    var l = json.split(/\r*\n/).length;
    this.textArea.rows = l < this.cfg.maxRows ? l : this.cfg.maxRows;

    if(opts && opts.notClear){} else {this.clear()}

    // this.textArea.innerHTML = json;
    this.textArea.value = json
    this.obj = obj;
}
AlfaJsonEdit.prototype.getObject = function(){
    return this.obj;
}
AlfaJsonEdit.prototype.clear = function(){
    this.textArea.value = ''
    // this.validateBtn.removeAttribute('invalid');
    this.validateBtn.removeAttribute('valid');
}

AlfaJsonEdit.prototype.validateJson = function(jsonStr){
    // if (this.notJson) return jsonStr;
    this.textArea.focus();
    try {
        var obj1 = JSON.parse(jsonStr); 
        // this.validateBtn.removeAttribute('invalid');
        this.validateBtn.setAttribute('valid', 'true');
        this.cfg.onValidated(obj1)
        return obj1;
    } 
    catch(err){
        // this.validateBtn.setAttribute('invalid', '');
        this.validateBtn.setAttribute('valid', 'false');
        this.popMessage(`Validation of JSON failed:<pre>${err.message}</pre>`)
        // alert(`Validation of JSON failed:\n${err.message}`); 
        return null; 
    }
}

AlfaJsonEdit.prototype.createElement = function(){
    // this.container.innerHTML = 'alfa-json-edit-ui'+this.elementSelector
 
    var textArea = $dce('textarea')
    // textArea.addEventListener('change', function(ev){
    textArea.addEventListener('keyup', function(ev){
        validateBtn.removeAttribute('disabled');
        validateBtn.removeAttribute('valid');
    })
    this.textArea = textArea;

    var validateBtn =  $dce('button')
    validateBtn.innerHTML = 'validate JSON'
    validateBtn.setAttribute('disabled', '')
    validateBtn.addEventListener('click', function(ev){
        this.obj = this.validateJson(this.textArea.value)
    }.bind(this))
    this.validateBtn = validateBtn;

    this.container.append(textArea)
    this.container.append(validateBtn)

    /** assing attributes */
    if(this.cfg.elemCss){this.container.style = this.cfg.elemCss}
}

/** internal message box */
import popupMesage from './alfa-simple-pop-ui.mjs';
AlfaJsonEdit.prototype.popMessage = popupMesage;

AlfaJsonEdit.prototype.popMessage_OLD = function(html, opts){
    if(!this.popMessageBox){
        var msgBox = $dce('div')
        msgBox.style = `
        display: 'none';
        position: absolute; 
        top:10px; left:0; right:0; 
        width: 80%; 
        margin: 0 auto; 
        color: black;
        border: 2px solid red; 
        background: lightyellow;
        padding: 1em; 
        text-align: center;`;
        msgBox.addEventListener('click', function(ev){ev.target.style.display = 'none'})

        this.container.style.position = 'relative'
        this.container.append(msgBox)
        this.popMessageBox = msgBox
    }
    this.popMessageBox.style.display = 'block'
    this.popMessageBox.innerHTML = html;

    /** add close symbol after html of the message is applied */
    var eks = $dce('div')
        eks.style = 'position: absolute; top: 0; right: 6px;'
        eks.innerHTML = '&#x2716'
        this.popMessageBox.append(eks)
}

AlfaJsonEdit.prototype.insertStyleTag = function(){
    var styleTag = $dce('style');
    styleTag.setAttribute('alfa-json-edit-ui'+'-style', '');

    styleTag.innerHTML = `
        ${this.elementSelector} {border: 1px solid silver; font-size: 1.1rem;}
        ${this.elementSelector} pre{border: unset; white-space: pre-wrap;}
        ${this.elementSelector} textarea{width: 99%; font-size: 14px}

        ${this.elementSelector} button{border: 3px solid silver}
       
        ${this.elementSelector} button:not([disabled]){border: 3px solid orange}
        
        ${this.elementSelector} button[valid=true] {border: 3px solid green}
        ${this.elementSelector} button[valid=false]{border: 3px solid red}

    `; // ${this.elementSelector} button[invalid]{border: 3px solid red}

    /* NOTE: calling body.append will add your new styles to the bottom of the page and override any existing ones */
    $dqs('head').append(styleTag);
}

export default AlfaJsonEdit;



    // helpers

    /** 
     * @param text 
     * delimiter: underscore or hyphen
     */
    function camelize(text) {
        return text.replace(/^([A-Z])|[\s-_]+(\w)/g, function(match, p1, p2, offset) {
            if (p2) return p2.toUpperCase();
            return p1.toLowerCase();        
        });
    }
