var obj1 = {
    es6module: 'export-obj.mjs',
    message: 'export {obj1} (new)'
}

var obj2 = {
    es6module: 'export-obj.mjs',
    message: 'export {obj2} (new)'
}

export {obj1, obj2};

/** REQUIRE-es6module.js replaces above to
module.exports = {obj1, obj2}
 */