# REQUIRE-es6module.js
## why
ECMAScript commitee gave us awkward modules, not near as good as native modules in NodeJS. For the last 10 years (that is TEN YEARS) modules are available in NodeJS. New Node v.14 has support for es6 modules, but a lot of code is in old reliable format. This utility is for those who are not willing to convert.  
Also **ElectronJS** does not support es6 modules.  
### Keyword `export` is toxic, it can not be evaluated by JavaScript.  
Utility `REQUIRE-es6module.js` parses *es6 modules* and replaces string "`export default`" or "`export`" with "`module.exports = `"  
That's how ES6 modules can be used both in browser and server side code.

## eval( )
Is `eval()` evil?  If code runs on server side or inside ElectronJS app, you can decide if it is secure enough.

## usage in NodeJS or Electron

```javascript
// declare requireEs6 at the top
const requireEs6 = require('../../REQUIRE-es6module'); // case sensitive !
// then load a module
const myModule = requireEs6('./my-module.mjs')
```
## relative path to es6module
```javascript
requireEs6('./my-module.mjs')
```
path `./my-module.mjs` is relative to the loaction of `REQUIRE-es6module.js` It is recommended to keep *REQUIRE-es6module.js* in the same folder with a library of es6modules. In that case path will always be `./`

## requirements for es6 module code

keyword `export` should be first on the line of code, close to the left margin.   
**MUST be** at least 4 chars from last NewLine char (to allow 1 tab)  
  
GOOD:
```javascript
export default aaaTempl;
```
Good enough:  
```javascript
    export default aaaTempl;
```

### keyword `export` may be used accidentally as a word in code  
 - placing string `export` further than 4 chars on line is allowed.  
### string `export` close to the left margin (witing 4 chars)
 - placing keyword as a string, close to the line beginning, in quotes "export" or 'export' is allowed,  
 - quote \`export\` is NOT allowed!
### string `export` in comments close to the left margin (witing 4 chars)
- preceeeded by `//` is allowed
- preceeded by `*` is allowed
- multiline comment **NOT allowed**:
```javascript
/* some multiline comment
   export keyword is 3 chars from left margin, not preceeded by any character
*/ 
```
- multiline comment, allowed:
```javascript
/* some multiline comment
 *  export keyword is 3 chars from left margin, separated by *
 */ 
```

