/*! alfa-browser-storage Copyright (c) 2019 alfalabs.net */

/** uses alfaDatabinding which should be setup on host page
 *  assumnig that alfaDatabinding datsasource ds has root = window, like window.ds
 * 
 * @param itemNames 
 */

function AlfaBrowserStorage(storageName, alfaDatabinding, itemNames){

    this.storage = {}
    itemNames.forEach(function(itemName){
        this.storage[itemName] = {}
    }.bind(this))

    this.storageName = storageName
    this.alfaDatabinding = alfaDatabinding
    this.dataSourcePath = 'ds'  /** assuming windows.ds */
}
AlfaBrowserStorage.prototype.get_and_bind = function(){
    for(var itemName in this.storage){
        var item = window[this.storageName].getItem(itemName)
        /** convert localStorage item names to legal JS variable names */
        var propName = camelize(itemName)
        this.storage[itemName].propName = propName
        this.alfaDatabinding.set(`${this.dataSourcePath}.${propName}`, item, null)
    }
}
AlfaBrowserStorage.prototype.set = function(){
    for(var itemName in this.storage){
        var propName = this.storage[itemName].propName
        window[this.storageName].setItem(itemName, window[this.dataSourcePath][propName])
    }
}

export default AlfaBrowserStorage

// helpers /////////////
/** interesting: https://gist.github.com/jpetitcolas/4481778
 *  from: https://ourcodeworld.com/articles/read/608/how-to-camelize-and-decamelize-strings-in-javascript
 * @param text 
 * delimiter:  hyphen
 */
function camelize(text) {
    if(text.indexOf('-')===-1) return text  /** no hyphen, no change */
    return text.replace(/^([A-Z])|[\s-]+(\w)/g, function(match, p1, p2, offset) {
        if (p2) return p2.toUpperCase();
        return p1.toLowerCase();        
    });
}