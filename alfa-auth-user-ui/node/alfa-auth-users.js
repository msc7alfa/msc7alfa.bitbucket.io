

/* using short hash - (in web browser client module: build in) */
var list = require('./alfa-auth-users-short.json')
var cfg = {hash_type: 'short'}

/* OR */

/* using default MD5 hash - (in web browser client module: loads on demand MD5 module) */
// var list = require('./alfa-auth-users.json')
// var cfg = {hash_type: null} /* default is MD5 */

module.exports = {list, cfg}