const express = require('express');
const routes = express.Router();

// http://localhost:3001/node/alfa-auth-user-ui-new/test
// routes.get('/test', function(req, res){
//     res.end('test response from app alfa-auth-user-ui-new');
// });

// routes.post('/', function(req, res){
//     require('./alfa-auth.js')(req, res); 
// })
// routes.get('/:cmd?', function(req, res){
//     require('./alfa-auth.js')(req, res); 
// })

routes.post('/', function(req, res){
    require('./index.js')(req, res); 
})
routes.get('/:cmd?', function(req, res){
    require('./index.js')(req, res); 
})

module.exports = routes;