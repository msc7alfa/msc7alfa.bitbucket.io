replacement for `alfa_lib_es6modules`  

in target dev environment (like Node_API) this folder is named:  

**`ALFA_es6modules`** and contains symlinks to here

## folder with library of ES6 modules common for: 

- front end es6 modules (in browser)
- ElectronJS (converted) 
- server side NodeJS (converted) 

each module will be symlinked to proper target folder 
target folder should contain linking command and list of items to link

## folder layout

 - root folder contains all library files  *modules.mjs*  
   modules having dependencies can be easily referenced to the same dir
 - other folders are for development and examples  
each module has it's folder named exactly as *module.mjs*  
where: 
    - usage example code is located in `index.html`
    - `node` folder with NodeJS code in `index.js` (optional)



## `REQUIRE-es6module.js`
(as of 2020-04-15) 
It is impossible to have code for `module.exports` and `export` in the same file.  
utility `REQUIRE-es6module.js` replaces string "`export default`" with "`module.exports = `"  
That's how ES6 modules are used in browser and server side.
[REQUIRE-es6module.md](REQUIRE-es6module.md) use `ctrl+click` on a link.

## _node_server

this folder has build in NodeJS server on port **:3001**



## to add a new module  

1. create file module-name.mjs in root dir
2. create folder module-name
    create index.html
    optionally create NodeJS examples
3. add module-name to _module-list.js
4. add route to _node-server/a-routes.js

index.html will be updated from `_module-list.js`