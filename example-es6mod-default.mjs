/** es6 module - example with default object   */

var myObj = {
    name: 'example-with-default.mjs',
    a: 'alfa',
    b: 'beta',
    log: console.log,
    warn: console.warn
}
export default myObj

/* default object


    STATIC import

    import myNewObject from './example-with-default.mjs'



    DYNAMIC import()
    NOTE: this is NOT a function, just syntax with () looking as JS function

    import('./example-with-default.mjs')
    .then(function(myObj){
        myObj.default.log('hidere 1')   <== NOTE: how to access export default
    })

*/