/* alfa-json-box.js 19.4.5c   es6 MODULE  (c)alfalabs.net 2018

<div class="alfa-json-box" 
    box-id="ajb1"           - optional 
    box-name="first box - &lt;code&gt;keep-closed&lt;/code&gt; on load"  - optional, can be HTML text
    keep-closed             - keep closed on load
    read-only               - can not clear content
    no-indicator            - no data presence indicator
    indicarot-color         - color
    >
</div>

*/

import AlfaConsoleLog from './alfa-console-log.mjs'; 
var alfaConsoleLog = new AlfaConsoleLog('alfa-json-box')
var log =     alfaConsoleLog.log.bind(alfaConsoleLog)
var logWarn = alfaConsoleLog.logWarn.bind(alfaConsoleLog)
var logErr =  alfaConsoleLog.logErr.bind(alfaConsoleLog)


// poor man's jQuery:
const $dqs = document.querySelector.bind(document); 
const $dce = document.createElement.bind(document); 
const $dcf = document.createDocumentFragment.bind(document);
const $dct = document.createTextNode.bind(document);

var alfaJsonBoxes = {}; //  for clearAll(), closeAll(), openAll()

/**
 * 
 * @param {*} containerSelector - unique selector for an instance of this particular box, example: '.alfa-json-box[box-id=ajb1]'
 * @param {*} opts - {err, raw}
 */
export default function AlfaJsonBox (containerSelector, opts){

    opts = opts || {};

    var defaults = {
        menuGroupClass: 'alfa-cbox-menu',
        uniqueAttr: 'alfa-json-box-from-alfalabs-net', // unique attr for CSS top container selector - by using common selector instead of containerSelector, only one <style> tag can be used
        indicatorColor: 'green'
    };
    Object.assign(this, defaults, opts);
    this.containerSelector = containerSelector;
    this.container = $dqs(this.containerSelector);
    if(!this.container) {logErr(`element not found: '${this.containerSelector}'`); return;}
    alfaConsoleLog.setCfg({id: this.container.id})
    log('constructor()')
    this.getAttributes();

    this.create();
    this.createControlsWidget();
    this.insertStyleTag();

    this.collection('add');
}
AlfaJsonBox.prototype.getAttributes = function(){
    var container = $dqs(this.containerSelector);
    this.id = container.getAttribute('box-id');
    this.name = container.getAttribute('box-name');
    this.readOnly = container.hasAttribute('read-only');
    this.keepClosed = container.hasAttribute('keep-closed');
    this.noIndicator = container.hasAttribute('no-indicator');
    this.indicatorColor = container.getAttribute('indicator-color') || this.indicatorColor;
    
   
};
AlfaJsonBox.prototype.collection = function(cmd){
    var id = this.id || + new Date();  // make unique id
    alfaJsonBoxes[id] = this;
};

AlfaJsonBox.prototype.create = function(){  

    // this.container = $dqs(this.containerSelector);
    // if(!this.container) {logErr(`element not found: '${this.containerSelector}'`); return;}
    this.container.style.border = '1px solid silver';
    this.container.setAttribute(this.uniqueAttr, ''); 

    // title-bar
    this.titleBar = $dce('div');
    this.titleBar.setAttribute('title', 'toggle visibility');
    this.titleBar.setAttribute('style', 'display: flex; justify-content: space-between;');
    this.titleBar.classList.add('title-bar');

    // data indicator
    this.isData = $dce('div');
    this.isData.setAttribute('title', 'data presence indicator');
    this.isData.classList.add('is-data');
    this.titleBar.append(this.isData);

    // title-bar text
    this.titleBarHtml = $dce('span');
    this.titleBarHtml.innerHTML = this.name || '';
    this.titleBar.append(this.titleBarHtml);

    // clear button
    this.clearBtn = $dce('a');
    var tipTxt='';
    if (!this.readOnly){this.clearBtn.innerHTML = ' &#10006; '; tipTxt ='clear content';}
    else {tipTxt = 'read only';}
    this.clearBtn.setAttribute('title', tipTxt);
    this.titleBar.append(this.clearBtn);

    this.container.append(this.titleBar);

    this.body = $dce('div');

    // create JSON PRE on load
    // this.jsonDisplay = this.addJsonPRE();
    // this.jsonDisplay = $dce('pre');
    // this.jsonDisplay.setAttribute('class', 'json-display');
    // this.body.append(this.jsonDisplay);

    this.container.append(this.body);

    // events
    this.titleBar.onclick = this.toggleVisibility.bind(this);
    if (!this.readOnly){this.clearBtn.onclick = this.clear.bind(this);}
};

AlfaJsonBox.prototype.toggleVisibility = function(e){
    log('toggleVisibility', e);
    if (this.body.style.display==='none') this.body.style.display='block';
    else this.body.style.display='none';
};
AlfaJsonBox.prototype.open = function(e){
    this.body.style.display='block';
};
AlfaJsonBox.prototype.close = function(e){
    this.body.style.display='none';
};
AlfaJsonBox.prototype.clear = function(e){
    if (e) e.stopPropagation();
    
    //  this.jsonDisplay.innerHTML = '';
    this.body.innerHTML = '';

    this.isData.removeAttribute('has-data');
    this.titleBar.removeAttribute('is-error');
};
AlfaJsonBox.prototype.closeAll = function(){
    for (var id in alfaJsonBoxes){
        alfaJsonBoxes[id].close();
    }
};
AlfaJsonBox.prototype.openAll = function(){
    for (var id in alfaJsonBoxes){
        alfaJsonBoxes[id].open();
    }
};
AlfaJsonBox.prototype.clearAll = function(){
    for (var id in alfaJsonBoxes){
        alfaJsonBoxes[id].clear();
    }
};
AlfaJsonBox.prototype.createControlsWidget = function(){

    var ctrlWidget = $dqs('.alfa-json-boxes-controls');
    if (!ctrlWidget) return;

    // create only once, not by all instances
    if($dqs('.alfa-json-boxes-controls a')) return;

    ctrlWidget.append($dct('JSON boxes: '));

    var closeAll = $dce('a');
    closeAll.innerHTML='close All';
    closeAll.onclick = this.closeAll.bind(this);
    ctrlWidget.append(closeAll);

    var openAll = $dce('a');
    openAll.innerHTML='open All';
    openAll.onclick = this.openAll.bind(this);
    ctrlWidget.append(openAll);

    var clearAll = $dce('a');
    clearAll.innerHTML='clear All';
    clearAll.onclick = this.clearAll.bind(this);
    ctrlWidget.append(clearAll);

}

AlfaJsonBox.prototype.insertStyleTag = function(){

    if ($dqs('style[alfa-json-box-colors]')) return;

    var sel = `[${this.uniqueAttr}]`; // instead of ${this.containerSelector}

    /* calling body append will add your new styles to the bottom of the page and override any existing ones */
    var styleTag = $dce('style');

    styleTag.innerHTML = `
    ${sel} .json-display .string { color: green; }
    ${sel} .json-display .number { color: rgb(139, 0, 0); }
    ${sel} .json-display .boolean { color: blue; }
    ${sel} .json-display .null { color: rgb(161, 1, 161); }
    ${sel} .json-display .key { color: rgb(0, 70, 161); }   
    
    ${sel} {
        font-family: Verdana, Arial;
        margin-bottom: 3px;
    }
    ${sel} .title-bar { background: gainsboro}
    ${sel} .title-bar code{ color: blue}
    ${sel} [is-error] {background: red; color:white;}
    ${sel} pre.json-display {margin: 0; border-top: 1px solid silver;}
    ${sel} .is-data {width: 1em; height: 1em; background: #9c9c9c; margin: 3px;}
    ${sel} .is-data[has-data] {background: ${this.indicatorColor};}
    ${sel}[no-indicator] .is-data {background: unset;}
    ${sel} a {cursor: pointer; padding: 0 5px;}

    .alfa-json-boxes-controls {
        color: green;
        background: rgba(0, 128, 0, 0.27);
        border: 1px solid green;
        font-size: 12px;
        font-family: Verdana, Arial;
        margin-bottom: 3px;
        padding: 2px 3px;
    }
    .alfa-json-boxes-controls a{ 
        text-decoration: none;
        cursor: pointer; 
        margin-right: 5px;
        padding: 0 4px;
        color: gainsboro;
        background: #00c700;
    }
    .alfa-json-boxes-controls a:hover{
        color: yellow;
    }
    `;
    styleTag.setAttribute('alfa-json-box-colors', '');

    $dqs('head').append(styleTag);
};

/**
 * 
 */
AlfaJsonBox.prototype.load = function(obj, opts){
    opts = opts || {};

    if (opts._add) {} // keep previous
    else this.clear();
    
    var jsonPRE = createPRE.call(this);

    var json;
    if(opts.isJSON) {json = obj;}
    else json = this.safeStringify(obj, null, 4);

    json = this.syntaxHighlight(json);
    if (opts.raw) jsonPRE.append($dct(json)); // to see HTML of the colorized JSON
    else jsonPRE.innerHTML = json;

    this.isData.setAttribute('has-data','');

    if(opts.err) this.titleBar.setAttribute('is-error','');
    else         this.titleBar.removeAttribute('is-error','');

    if (this.keepClosed) this.close();
    else this.open();

    changeAttrs.call(this);
    //// helpers

    function changeAttrs(){
        // NOTE: this will override original settings and will not be reverted on subsequent load()
        if (opts.name)        {this.name = opts.name;                                                      this.titleBarHtml.innerHTML = this.name; }
        if (opts.readOnly)    {this.readOnly = opts.readOnly;   this.container.setAttribute('read-only', ''); this.clearBtn.innerHTML = ''; }
        if (opts.keepClosed)  {this.keepClosed= opts.keepClosed; this.container.setAttribute('keep-closed', ''); this.close();}
        if (opts.noIndicator) {this.noIndicator = opts.noIndicator; this.container.setAttribute('no-indicator', '');}

        // if(this.indicatorColor){  DO NOT, this will prevent color change on attribute has-data removal !!!
        //     $dqs(`${this.containerSelector} .is-data[has-data]`).style.background = this.indicatorColor;
        // }
    }

    function createPRE(){  
        var jsonDisplay = $dce('pre');
            jsonDisplay.setAttribute('class', 'json-display');
        this.body.append(jsonDisplay);
        return jsonDisplay;
    }
};
AlfaJsonBox.prototype.add = function(obj, opts){
    opts = opts || {};
    opts._add = true;
    this.load(obj, opts);
};

/** could be called from outside:
 *      
    txtElem.setAttribute(alfaJsonBox.uniqueAttr, '');
    var colorizedTxt = alfaJsonBox.syntaxHighlight(txtElem.innerText);
    txtElem.innerHTML = `<span class="json-display">${colorizedTxt}</span>`;
 */
AlfaJsonBox.prototype.syntaxHighlight = function(json) {
    if(!json) return;
    
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
};

AlfaJsonBox.prototype.safeStringify = function(obj, replacer, spacer) {
    // https://github.com/davidmarkclements/fast-safe-stringify
    var arr = [];

    decirc(obj, '', [], undefined);
    var res = JSON.stringify(obj, replacer, spacer);
    while (arr.length !== 0) {
        var part = arr.pop();
        part[0][part[1]] = part[2];
    }
    return res;

    //////////// helpers

    function decirc (val, k, stack, parent) {
        var i;
        if (typeof val === 'object' && val !== null) {
            for (i = 0; i < stack.length; i++) {
                if (stack[i] === val) {
                    parent[k] = '[Circular]';
                    arr.push([parent, k, val]);
                    return;
                }
            }
            stack.push(val);
            // Optimize for Arrays. Big arrays could kill the performance otherwise!
            if (Array.isArray(val)) {
            for (i = 0; i < val.length; i++) {
                decirc(val[i], i, stack, val);
            }
            } else {
            var keys = Object.keys(val)
            for (i = 0; i < keys.length; i++) {
                var key = keys[i];
                decirc(val[key], key, stack, val);
            }
            }
            stack.pop();
        }
    }
};//end:safeStringify


/**  helper */
