/** for node links to work, the must be setup in _node-server/apps-routes.js
 *  CSV:
        module-name,  - exactly as dir name containing index.html for testing
        type, 
        nodeRoute, - route in _node-server/a-routes.js (route: /node/APP-NAME/:cmd/:value?    here, skip APP-NAME: /node/cmd/index)
        description  
        optional - {disabled}
        str - for callback makeLinkHref()
*/

var list =  `
alfa-index-ui           ,,,     this page code
AAA-template            ,, node/, template, {"disabled":"true"} 
AAA-template-ui         ,,     , template with UI, {"disabled":"true"} 
example-es6mod-default    ,,,     import and dynamic import( ) of es6 module with default Obj,{"disabled":"true"}, /example-es6mod/index.html  
example-es6mod-named      ,,,     import and dynamic import( ) of es6 module with named Obj,{"disabled":"true"},   /example-es6mod/index.html 

alfa-console-log
REQUIRE-es6module-test  ,,,     test es6module to NodeJS module converter

alfa-fetch              ,, node/info, fetch with callbacks. timeout. wait spinner
alfa-foreach-sync            ,, node/, synchronously enumerate an Array

alfa-databinding        ,,, bind a variable to any html element
alfa-setproperty-bypath ,,,      set or create property of an object (dependency for alfa-databinding)
alfa-template-literals  ,,,     template with databinding

alfa-browser-storage    ,,,     sessionStorage and localStorage
alfa-action-manager     ,,,

alfa-drawer-ui          ,,,     UI for a page with animated sliding drawers
alfa-resizable-panels-ui,,,     draggable split handle
alfa-image-transform-ui ,,,     animated zoom

alfa-json-input-pop-ui  ,,,     input popup box attached to an html element
alfa-json-box-ui        ,,,     collapsible boxes for data display in JSON format. not animated
alfa-json-edit-ui       ,,,     &lt;textarea&gt; for JSON editing and validating

alfa-json-pretty-ui     ,,,     display JSON formatted and colorized (creates UI container)
alfa-print-object       ,,,     display object formatted and colorized (creates HTML for &lt;pre&gt;)

alfa-modal-spinner-ui   ,,,     page service for displaying wait spinner
alfa-simple-pop-ui      ,,,     popup box attached to an html element
alfa-modal-popup-ui     ,,,     page service for displaying popup boxes
alfa-audio-clips-ui     ,,,     service to play audio clips

alfa-animation-vert-ui   ,,,     animated dropdown box
alfa-collapsible-ui         ,,, NEWEST: animated collapsible boxes with header - new for alfa-collapsible-anim-ui alfa-collapsible-boxes-ui
alfa-collapsible-boxes-ui   ,,, OLD: animated collapsible boxes with header, {"disabled":"true"} 
alfa-collapsible-anim-ui    ,,, NEW: animated collapsible boxes with header, {"disabled":"true"} 
alfa-menu-ui
alfa-drag-drop-ui
alfa-checkbox-ui        ,,, no module - example of CSS
alfa-nano-auth          ,,node/, abandoned - use alfa-auth-user-ui, {"disabled":"true"} 
alfa-auth-user-ui       ,,node/, user authentication - modal popup asking for user credentials - has NodeJS back end
md5-hash                ,,,calculate hash values
alfa-data-table-ui      ,,, in development
`

export default list
