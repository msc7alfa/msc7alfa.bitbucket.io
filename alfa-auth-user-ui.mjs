/*! Copyright (c) 2018 alfalabs.net license MIT */
const $dce = document.createElement.bind(document)
const $dqs = document.querySelector.bind(document)
const MODULE_NAME = 'alfa-auth-user'

/** settings and optins can be done in constructor() and open() 
 * 
 * @param settings/opts = {
 *              max_count: 5
                email_as_id: false, 
                no_last_user,
                no_choice_name_or_email, 
                hash_type: default=undefined to use MD5 loaded on demand, 'short' to use small build in hash function
                no_icon,
                overlayBackground,
                boxShadow,
                forgotPasswordLink,
                uniqueTagAttr,
                spinnerElemSelector, - spinner is covered by this popup and must be bring to top
           }
*/

class AlfaAuthUserUI {

    constructor(settings={}){
        /* defaults: */
        this.max_count = 5
        this.overlayBackground = 'background: black; opacity:.4;'
        this.boxShadow = 'box-shadow:2px 2px 4px #000000;'
        this.uniqueTagAttr = 'alfa-authenticate-ui'

        /* defaults above can be overridden: */
        Object.assign(this, settings)
    }

    /**
     * @param cb - callback on successful authentication @optinal
     * @param opts - see at the top
     */
    open(cb, opts={}){

        this.callback = cb

        Object.assign(this, opts)
        this.count = 0

        /** protect page by showing overlay first in case of code failure, it will stay */
        this.showPopup() /** style is hard coded */   

        if(this.hash_type!=='short' && !window.hashMD5){ /** load hashMD5 module on demend */
            this.loadMD5module(function(){
                makeUI.call(this)
            }.bind(this))
        } else {
            makeUI.call(this)
        }

        function makeUI(){
            this.setStyle()
            this.buildForm()
            this.userIdInput.focus()
        }
    }
    close(){
        $dqs('body').removeChild(this.overlay);
        $dqs('body').removeChild(this.popContainer);
    }

    loadMD5module(cb){
        async function loadHashModule() {
            let {Hashes} = await import('./md5-hash.mjs');
            window.hashMD5 = new Hashes.MD5(); 
            cb.call(this)
        }
        loadHashModule.call(this)
    }

    buildPopup(){

        this.overlay = $dce('div');
        // this.overlay.setAttribute(`${this.uniqueTagAttr}-overlay`,'');
        this.overlay.style = `
            display: none; 
            position: fixed; left:0; top:0; bottom:0; right:0;
            ${this.overlayBackground}`
        $dqs('body').appendChild(this.overlay);
    
        this.popContainer =  $dce('div');   // popContainer is full screen as overlay
        this.popContainer.setAttribute(this.uniqueTagAttr,''); // make target for CSS to select by attribute
        this.popContainer.style = `
            display: none; 
            position: fixed; left:0; top:0; bottom:0; right:0; 
            justify-content: center; 
            align-items: center;`
        this.popContainer.setAttribute('tabindex', '0');
    
        this.popBox = $dce('div'); // centered popup box
        this.popBox.style = `
            display:flex; 
            flex-direction:column;  
            max-width:90%; max-height:85%; 
            min-width:180px; 
            color:black; background:white; ${this.boxShadow};`
    
        if(!this.no_icon){
            this.img = $dce('img')
            this.img.src = this.icon()
            this.img.style = 'width: 98px; align-self: center; padding-top: 1.5em;'
            this.popBox.appendChild(this.img);
        }
        this.popBody = $dce('div');
        this.popBody.style = 'padding: 20px; overflow-y: auto' //  max-height: 85%;
        this.popBox.appendChild(this.popBody);
    
        this.popContainer.appendChild(this.popBox);
        $dqs('body').append(this.popContainer);

        if(this.spinnerElemSelector){ /* move wait spinner above this popup to be seen */
            var spinner = $dqs(this.spinnerElemSelector)
            $dqs('body').append(spinner);
        }
     
        // this.buildForm()
    }

    showPopup(){
        this.buildPopup()
        this.overlay.style.display = 'block';
        this.popContainer.style.display = 'flex';
        // this.userIdInput.focus()
    }

    buildForm(){

        this.form = $dce('form')
        this.form.style = 'display: flex; align-items: center; flex-direction: column;'
 
       var htm = `
        <div alfa-lbl>
            <label>${this.email_as_id ? 'user email' : 'user ID'}</label>
        </div>
        <div input-wrap>
            <input name="user" type="${this.email_as_id ? 'email': 'text'}" placeholder="${this.email_as_id ? 'user email': 'user ID'}" autocomplete="username" required>
            <img btn-userid title="user name or email">
        </div>
        <div input-wrap>
            <input name="pass" type="password" placeholder="password" autocomplete="current-password" required>
            <img btn-tgl-pass title="password visibility">
        </div>
        <div alfa-msg>&nbsp;</div>
        <input type="submit" value="Authenticate">`
        if(this.forgotPasswordLink){
            htm +=`
        <div>
            <a href="${this.forgotPasswordLink}">forgot password</a>
        </div>`
        }
        this.form.innerHTML =  htm

        this.form.addEventListener('submit', this.submitForm.bind(this))
        this.popBody.appendChild(this.form)

        const $fqs = this.form.querySelector.bind(this.form)
        this.userIdInput = $fqs('input[name=user]')
        this.userIdPass  = $fqs('input[name=pass]')
        this.msg =         $fqs('div[alfa-msg]')
        this.forgotPassA = $fqs('a')

        this.userIdInput.addEventListener('keydown', this.onUserIdKeydown.bind(this))
        
        var btnShowPass =  $fqs('img[btn-tgl-pass]')
        btnShowPass.addEventListener('click', this.showPass.bind(this))
        btnShowPass.src = this.iconEye()

        var btnSwitchUserID =  $fqs('img[btn-userid]')
        btnSwitchUserID.src = this.iconEmail()
        if(this.no_choice_name_or_email){btnSwitchUserID.style.visibility = 'hidden'}
        else {  btnSwitchUserID.addEventListener('click', this.switchUserID.bind(this))  }

        var last_user_id = localStorage.getItem('alfa-auth-last_user_id')
        var last_user_email = localStorage.getItem('alfa-auth-last_user_email')
        if(this.email_as_id){if(last_user_email){this.userIdInput.value = last_user_email}} 
        else {              if(last_user_id){this.userIdInput.value = last_user_id} }
    }
    setStyle(){
        if($dqs(`style[${this.uniqueTagAttr}-style]`)) return

        var blu = '#0a66c2' // '#0066ff' // #3f51b5  //vertical-align: text-bottom;  
        var css = `
        @media only screen and (min-width: 600px) {
            [${this.uniqueTagAttr}] form{ min-width: 20em;}
          }
        [${this.uniqueTagAttr}] {font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;}
        [${this.uniqueTagAttr}] div[alfa-lbl]{font-size: 12px; font-family: sans-serif; width: 230px}
        [${this.uniqueTagAttr}] label{color: ${blu};}
        [${this.uniqueTagAttr}] div[input-wrap]{border: 2px solid ${blu}; margin-bottom: 3px; border-radius: 5px;}
        [${this.uniqueTagAttr}] input{font-family: Verdana;}
        [${this.uniqueTagAttr}] input:not([type=submit]){width: 200px; margin: 0 0 2px 2px;  border: unset; border-bottom: 2px solid white}
        [${this.uniqueTagAttr}] input:not([type=submit]):focus{outline: none; border-bottom: 2px solid orange}
        [${this.uniqueTagAttr}] input:invalid:not(:placeholder-shown){border-color: red;}
        [${this.uniqueTagAttr}] img[btn-tgl-pass],
        [${this.uniqueTagAttr}] img[btn-userid]{width: 24px; cursor: pointer;}
        [${this.uniqueTagAttr}] img[btn-userid]{vertical-align: middle;}
        [${this.uniqueTagAttr}] img[btn-active]{filter: hue-rotate(125deg);}
        [${this.uniqueTagAttr}] div[alfa-msg]{margin-bottom: .25em}
        [${this.uniqueTagAttr}] input[type=submit]{background: ${blu}; border: 4px solid ${blu}; color: white; border-radius: 5px;}
        [${this.uniqueTagAttr}] input[type="submit"]:hover{color: #ffeb3b !important;}
        [${this.uniqueTagAttr}] a{font-size: .75em; color: ${blu}; visibility:hidden;}
        `;

        var style = $dce('style')
        style.setAttribute(`${this.uniqueTagAttr}-style`, '')
        style.innerText = css
        $dqs('head').append(style);     
    }
 
    showPass(ev){
        var btn = ev.target
        var type = this.userIdPass.getAttribute('type')
        if(type==='password'){this.userIdPass.setAttribute('type', 'text');     btn.setAttribute(   'btn-active', '')}
        else {                this.userIdPass.setAttribute('type', 'password'); btn.removeAttribute('btn-active')}
    }
    switchUserID(ev){
        this.email_as_id = !this.email_as_id
        this.form.parentNode.removeChild(this.form);
        this.buildForm()
        this.btnState(this.email_as_id, ev)
    }
    btnState(state, ev){
        var btn = ev.target
        if(state){btn.setAttribute(   'btn-active', '')}
        else {    btn.removeAttribute('btn-active')}
    }
    onUserIdKeydown(ev){
        if(ev.keyCode===13){
            ev.preventDefault()
            this.userIdPass.focus()
            return false;
        }
    }

    submitForm(ev){
        ev.preventDefault()
        
        this.count ++
        if(this.count > this.max_count){
            this.messageLine('Maximum number of attempts exceeded!', {error:true})
            return
        } else {
            this.messageLine('attempt '+ this.count, {info:true})
        }

        var userCredentials = {
            user_id:    this.email_as_id ? null : this.userIdInput.value,
            user_email: this.email_as_id ? this.userIdInput.value : null,
            user_pass:  this.userIdPass.value,
        }

        this.authUser(this.server_side_api, userCredentials, function(err, response){
            // if(err) console.error('AlfaAuthUserUI', err)
            // console.log(MODULE_NAME, response)
            if(response && response.responseMeta && response.responseMeta.status===200){
                if(response.responseData.error){
                    this.messageLine(response.responseData.error.message, {error:true, err:response.responseData.error}) // 'Authentication failed.'  
                    if(this.forgotPasswordLink) this.forgotPassA.style.visibility = 'visible'
                } else {
                    this.messageLine('Authenticated OK')
                    this.close()
                    if(typeof this.callback==='function'){this.callback(response.responseData.userInfo)}
                }
            } else {
                var txt = err.status ?  err.status +' ' : ''
                    txt += err.statusText ? err.statusText +' ' : ''
                    txt += err.message ? err.message : ''
                this.messageLine(txt, {error:true, err})
                console.error(MODULE_NAME, err, response)
            }
        }.bind(this))

    }

    messageLine(innerHtml, opts={}){
        this.msg.style.color = opts.error ? 'red' : (opts.info ? 'blue' : 'green')
        this.msg.innerHTML = innerHtml
        if(opts.err){console.error(MODULE_NAME, opts.err)} /* opts.err is an error object */
    }

    /** generic alfa-core
     * each Applet has it's own user handling, thats why apiUrl is needed
     * 
     * @param {String} apiUrl - route to server side auth handler app-authenticate.js
     * @param {Object} userCredentials - {user_id, user_email, user_pass}
     * @param {Function} cb - callback
     */
    authUser(apiUrl, userCredentials, cb){ /* similar code in ALFA_LIB/alfa-auth.js */

        if(this.hash_type){userCredentials.user_pass = this.shortHash(userCredentials.user_pass, {toString: true}) } 
        else { if(!window.hashMD5){
                this.messageLine('hashMD5 module is missing!', {error:true})
                console.error(MODULE_NAME, 'hashMD5 must be imported on hosting page!'); 
                return}             
                            userCredentials.user_pass = window.hashMD5.b64(userCredentials.user_pass)}

        alfaFetch(apiUrl,  {
            method: 'POST',
            body: {userCredentials, hash_type: this.hash_type},
            onSuccess: function(response){
                if (response.responseMeta && response.responseMeta.status===200){ 
                    sessionStorage.setItem('x-access-token', response.responseData.accessToken);
                    var userInfo = response.responseData.userInfo /* userInfo came from server side */
                    if(!this.no_last_user){
                        localStorage.setItem('alfa-auth-last_user_id', userInfo.user_id); // localStorage for next day login
                        localStorage.setItem('alfa-auth-last_user_email', userInfo.user_email);
                    }
                    sessionStorage.setItem('alfa-auth-user_info', JSON.stringify(userInfo));
                    cb(null, response);
                }
                else {
                    cb({status: response.responseMeta.status, statusText: response.responseMeta.statusText}, response);
                }
            }.bind(this),
            onError:  function(err, response){
                cb(err, response);
            }
        });
    }
    
    shortHash(str, opts) { // https://github.com/darkskyapp/string-hash
        if(typeof str!=='string' | typeof str==='undefined') return null
        var hash = 5381,
            i    = str.length;
        while(i) {
            hash = (hash * 33) ^ str.charCodeAt(--i);
        }
        hash = hash >>> 0;
        if(opts && opts.toString) return hash.toString(16)
        else return hash
    }
    md5Hash(str, opts={}, cb){
        var hashStr=''
        if(!window.hashMD5){ /** load hashMD5 module on demend */
            this.loadMD5module(function(){
                cb(calculate(str, opts))
            }.bind(this))
        } else {
            cb(calculate(str, opts))
        }
        function calculate(str, opts){return window.hashMD5.b64(str, opts)}
    }

    icon(){
        // return ' data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGIAAABUCAYAAAB5huK+AAAAAXNSR0IB2cksfwAAAAlwSFlzAAAATwAAAE8BY4r91wAADUtJREFUeNrtnQtQm1UWgKPUAkkIr/JqeLVQaosCQiEJCQESQniW96OFFuQl0OJUXd1Vq67drrNud111VwkEwiM/pDzCmwKB8Ki1RcG6bdVZd2frbncddeq23anOap323//+ARrSJPz5SUIK/5k5U6aQ3OR+Oeeec+65NySShmxL6zmxVQj9xU3QetshRgoTahx15klhd0Hrde8UmYJRMpZC0iUx1cok+6hGmBJZB1PZ9ajacQg1llI59UvzSmbVwS5x0MeFr85vWgaBVaZIt2aIFiZfvKSLDyR09ao+r0ApkfWwLQIk4/nZzSiE7Bdm7WhRDcgfLwdgyxTB9txGeIsAmnSNbx9DVEGo4erMbxulJ7ZdsGWpLEEThqtA+jkKYnt6VztwR0u/RCA4xkjgyPJxJokQo4pXctfrZNZy6yAjc787fziF5CGUfrVoOuBfB64Ezjn6oQMxbaYR37S+CnUYYM69kmWDJPvopqX/tEHcEbdKGU5Ml2nFM7njT3e9UD3sxJN+QlInQ0OiJmKaTC+Fxy7QNke8e9dFRUkuLgPhkwx9REyTeYSeAN2NptRBLJjJmDleBKdyemtg/sDP/NK65PSkzk9IobVfk0JEX3qndnwQkCV/K3DfQMp6B7EtFUIDIx0gxApTDi6smRTQoiT/s2WJ0GiBohFjg59BSAfCPJDXeAjbZ3mHlOsyevNNWSMQXkmy0yCW1pbc6FIUDKL0xPYLBAij+MP27zSTGUMURBggkAguPOVHgMApzrzWz1YDQd06HKLXT1RnVhC78wafALnJaiGoW4ZrfNslAoSBgmTpt6lGgnC3JFAPcyoUTAIERgnMHzpiDJekzUUh0dR7BAisC3Ri27whERJmEKiLEutcKxiPj8aSIxu/pbBE1xG9Zg4lM0X/teO2fJr70pyzxYHYHCEyOoQl94TkIUH7h0M1x6x6468P2bBEsCk+AFjWL3cB9IVFgQgvnfS3ZYpM+qb9M/uPaI7rkab0tGXWmh3CYvEOmdDrFgVid25/jvoeh9FBIG/AP71TqW1sGrf5CojUwPjmVLDbFlIw9LpFgdiZ3fsrUKowFQjgejYzxFd1jR9ZPp7ulz1SvCNnpMhMWhJTpdxjcYv1zqyublOCWNDviahpBbHnyXpV3QumA0HjNNwKLZvbRIDQB4Ij7jbDAvkjYRF6pOj4x1YOMZJb6l0hpomc6uGYqolMAoQOeTS/911TZNTaFmw3AXSDAKFDHHmQ0mwJlY499uD9g3nBBcOHQgqGq82hwYXDNcySEa5FgXAXygbNAUJlEdKf7snoGeI7izuA5lRrRi3sl94ptRgQjJLRgzYmzKo1QHymPrZr6qQnec0ya0S5TZaVWbsKpFdMuU6AQAA09YaXjMVojg2atyhmbqBebCymJ7aft7iin1eS7G8mS+qixHBI4UiitnG5lcooZBGfd+JJLznxoYsmVx500Zkv/QR5v6MHj807WhwIbsW4LWhiNkXBb2d27xvEfoQhO3Sxzf829sINni+8eNyfAGGAxFROCEA0YcwkziupndizxhXOxkuvgHDSGJYAorGEp844ESBwik9KRxM5sm5VluDMa/4m44V5CmmdyJp1+vnu7TiHZ7NI1c8kWXdd6msGQvDkJHpEDE+UFJg/cBTrOEH7B1525rfA+oqO4HX4pMhmNiSIoKJzVnachlt4GgUeK56MwjJG1tELFBsks8YSqYE99cD8/hc2HIjKE+et7LkNPxracGbDEMG5L55jYRljT/GEK9amBZAJB+b1/mHtQEjXBkTIgfEH7bkSHK0zdUjeMFqNdRwXfvOllWAAdwdeC7dqirxWILbtlV1eep3mBBFWNHQCT/2Jip5ybfrBkLGY5dMuSMi8FXm8h6Yiz+fhLOjwxPI8vMMzm5hlyrCd2QNP+6R2ttET2hXUqCZUPRLaFN6pndDuvMGnuZXKsMiKKSvDYXRAoPvDbCAiSieibFdRBAQAAzK7G831aQ05cErgFgddtEcWdfJCuwxVy6Ea1cEaVRmcxmm44yGELkY8PiowZKyATPmUDVP8DzO05UNDxiiLg6TQTQB9ySyfoJsMQMHQr8nojQAi1WkmHP1WwN1Q2Q2wX0bvL7COu2vf6LMmAcF/8qzrwzl9x0GUZMxGs8XJcYmTXn4kfyC17MTnRuniQD7FMaBACSyPaqQyPbASkP+wykYxRXxGA8EuV6RuT+vssWHWI5FOrcqcTbgppNoVE8FeSdD51Vls65+B2zRJwzRbdd+Ga5x03uQgdmTK6+yjG9FzC6AUYeruDS1v9gYeAMWvXXzIhd/ytTkaHVS30bR+Wfm7T62MDgJxPc9SFhYrc0++hl4zFMK+V+Y2Az9uhm7EZSGzHacRrjjxqbXRQIQVjWSR0fb3NQWAG8QWfutNc0JQL146xTZ/azQQDsjCthZnEIwBwjG25StTdqpjsQzETf1r1SAYJWPbLcQSDAYRtH+4ytYMHSZY6lxhB4dLVwXi0f2n8slr+InCC4JfM+1uQVaMuvXUZ97bghtEQHb/S5T7EIRX8smxtVgX9J56yugawQ0iKK+72ZLeEBYQSc+c3WJjAS7pnsoysw4+8OpHzrhAeKfKIUsycSwgArJ63rYwK16yCr8M+W9wgdidK2+93yzCLqrxzppNNlt1jwiN23gbTXg11glQWsEFYkdm3y/vpzWCU6mMWKvgwhZxPaGFQ8f5NaetWRUzD0SUjKbStGTdvMNToQaDiK6e5JqjLGAsEP4Z3bVrYcEAfkhBf7nm6wk9OFKsPn9o52JWz1u4Ejo7TgO8WCizgPVCLwi3eOiMWV8jW3VJZWTZWLlW157TS6WptaKqrrRomyFpFNBGsSZHoQcGC33TuuvsOI1XaVENt8AVPsAUVeeOV74JGE8lU10pkao6F/Kmf9ILQtB61ZwgQJd6bPVUua7XE3d4iqG5wQQKgssuV/ROkn6Ap5IZf+Ss1c7sPufoQzPhu/IGyv0z5b/3TJL1uwmkZzyE0GWXOOk1UlgdvGnPH9EDHZRI0cIGSh1MXriaedHC0MPjCFAq8jfWEbXoY8Bjkee44R7fenlrQtusu7Ctf0dmz1u78waq0p47w9C7hx0nNY/VslVuxjNFnqzv9TjFtnyjCcIjXnp7GQgbVuNtU29DFh07t8Uz87R74hFlqGP8cHBYQReHX30qIb56ON4hVvrYtuyJkJQj48EeadPupa/Nua56wEffXrEQBwqY5FXsSajOa4jhwLw+vRBoXMllbesVldsEk+zUOiuAb9v73Ace66Wb7ur38AP6QIDJ35Ut70Yimqg9xWP7nGIl/zE0EFFt/ohgZumIXghbE9tu6oo2qRwJDO4fvaF+JTXir/6+nlobHWNbdUIILhi8p6/JO+XkF1jDc9W8NcDcqkmdEHJfnN2EzOlNXYDBc7gKpDDJPaF9iKpmLmjre/LJD9cLCPd46DttLsee2wBHlk84aHuMfXTT7EowVN8oUAfza6bjV+izuqkvjwHPg6x910gZPz+zi8K+9wZ3RyQK8svofueRwkkePXOG7Zk1w7FE3ZoxHR3+xHmdnR30ROiSLhCssnEHndFWXItOGKrruxvgoILhQJ1RZZHCyim2acUNKBREAjSn+tQIZY2adXoqW7wQIorQCMZSlcyqhQMyO2U6a2MpHTLtX6SBrA+5fe/otybprObZDlXjQj3MrVQE6nWJMc0/YEkkgRH4pHa13h1UKBsxVTeDqY/5IiGyzi4JVpkiQ9dBGRtGHRzx+MhxfRNKTzw5u+jfFy+yT3r6PZ0Q8l+ed0RCVBjrOgNgscvHEzV33wooIDNcbIdh3weKttXU6Tw/wSydoAI3RNVTig7a16cXxra0zlnQZW6HuKOco3OB+vOEZtiwWlwDHFKktNH6ZNzq09t90weSXBIGhK6Jlq80nnxv1Zv/tNa5xx4rndFn6SBsD8iS64WxmSXpDipU+Or6fVTFuCOoLhhS1wJ/6yaUTZM2igTknArD0iHuxD9Zi3cMe/R7mgxz68Dz8A9PPULaSEJhS26sNFHA/fik9ckNed78V+b9nHnNMMVACFTV5fOfkzaaBBWM8LG4DWA5nsmdmGAcPH6eDM5Z4Cmzg6sqGKWKGNJGFHryyX4sJQywZrgJIL0weNUTfiCCouCIMkGeRk+SdZA2soC2Ryy+HF1IBS1aYbArlGQaV4wr1FeV8cXEdzllPX/Wl8rBtrAC6/FKap/QgMClsPBDAF9Dmvrs+3QSISSS8MjpSArGb3cBuQqyDlz3z+z7rUMsNIbnAMtSMoj8+9iBU4EEAfWmggplKNaQc3FHEP9+RT3arRFRMkZA0G4ZZzwcFk4KmfJsBIiseDVq7ZWEaBePBJmEwqoz6nmJxW1fz5QOCTHDBgirTBHul9Z5yXqVx87QhgaGCEnWZB8yShShxMziFH7Nae9dub1NYL+BzNLsSBFr/Rk94ouelxbfeTi7pyHhqfd9iJk0kux/ee7B2ENTEX7p8jfpiW3T7vHQFVd+y3eucVIYVeRndyF0BdHJHZk9bwpqphirGe//CddXka9bDosAAAAASUVORK5CYII='
        return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGIAAABUCAYAAAB5huK+AAAAAXNSR0IB2cksfwAAAAlwSFlzAAAATwAAAE8BY4r91wAADndJREFUeJztnQlUE3cexxHFbcsVScItIHKjqNRaj4qI1Fpt+1rPevWwSo5JQkIIGkAOoRYqAlqLSrUe1KrgEcg19yTRVtvVanerrbvvtXa13YrVqnW3FlR24j53ORJIhsxMtH7f+73nQ2b+v//vw/+c3/zHw6OTpq/WhsXK9LXBAPYNX4y3c1ZAHY/MNcYVIB2BAH41Uo6gk4r0i/M+bPmThy1NKYGXcUVE6xNZlo5HRq/5CCx3I7JhfMkmXXQXCBPUOqGPwMS6g3804whNV16pQmLuQZhdAydwxcQ1W7/Il5hu8kXIp4FCiAgUwo+MgvEEEBEMEGf9hOY7tmIcmQ19It4APeYRo0T2df/PQIC4nFYAvlS9SzvIZj/2SE4rezMYPCoPqfAVmNs6x9o7y3x3TE7LIo8gAP+xSysAsJ/mVLaMYtvxh1G3bt0akFbQIiFhtHeOeYQU0Xn4C9AurSGzXD+LbYcfZl28eHHASDWyoXPMuQLwjEe3LunmTt3xR90RzVq6viWxc9x9xfquIEJllr+w4di1a9e8bty44clG2WwpWGK2D8JbYDHR7QCGYZ4zSxtTRqqg1cOz4ZZwADzDEZnOccTIWbKv/DwuB9oyJg9eqK4/EEi3L2wqXPGJfRCkEXQVvKpe6/VcBbwgTIZ/5+3AHNtXQNwOlcDHnl0LzcrdcnAwXX6xJVZALKqEvOOUYAs5W7jt7KLHV2i+PTwHw5fUtDxULYRxEIuqoPBhMvRcf1ehXBFxZXSuId3V/rElRkGcPn3ac7gCtbhqS4AvJq4tWGcIdqWPbIlREE+tNChdBeG+DcuGTaUfobZ3Lh8gMQYiI6+RFyAifnQ1COs4k16KPPCLTsZApCiN+a6GcN9CJeiR3soGQXBwRUWFN1NWWVnpTXbDA52JD2MgQiX4SbpAkC2t/e0tTY/bKndcIbQwSIyf4QiJ75i0CBlmnl8BjnQrEJsatN7+AqydLhBWG69qzuheLlB1MMTfznYzExYqxc66FYgpRYeforvS8XJdYfdyJwI1iXSX25txhOhPbgViRC60jO5KD5NjPXz9+uuvB0VkY392ZOXuavMhJxFj1Ui1W4GIz4Gr6K44T4T+/ejRoz22PuZWQ5zJBdDCZOAAwJSNkB4EphUjGXm7jni5FYhYJa6hvRsQmX5YXtPEcYW/bIgRELzlh/R0g/AX4K1TpB/wXeEvG2IEhI/giIH+PhlvDVPWPQJhT0uroQiuiLhENwjvLHPbpCL0LVcEhQ3RDmKUGtlCN4T7Fgzgl1wRFDZEOwi+wIgzBcJXgHV0L1/5gY47Vg29MSbXqGLKUkl7phSdVb6tweHn/bSDCANAHVMg+GLi985l70W/fIwvxs4zVX7XrpJcZCrRBrcBMaEAEZH9N0NdE9Yl2WG8oIrVlbW/O62sV2wx+oZnoyforzTRPqlQN7tz2cUfGAO4IvwWWyAis5HjbgPCqsxCfUiUHD5OI4Tr41bpFpc17eyx9Ty5EJkbLEaOc7OgM0xalAzRL16HJrkVCKumroFjOSLzb3SAiM+B61zlJ1tiDETJ+42DQyXoaVdD8BOY2yetbB7vKj/ZEqPPrDPfNs53NYhYOWxWVTk+TXRXMQqifOvuQdE5+Ocuaw1C4ubzxfqHIlOd8bymvM1NjyeqkC39ndKGSPG/vlLW7PBg6O5iJdOvqv5jrwQlBFKFQC7cLgs3NAfR4RtbYi33dVoZkugvMlOa44/IgSodLWfEKqSWKybaerufr4DoiJJj+NJaI4+u+vYl1kBMzt4e6C/AL1MBMX7lgZcdKWPh+qYIH4FjyQPWLYnRKuNquurbl1gDsawW5weILZReH55dgb7gSBkZazSxzjyvTsmDHX7G7GqxBmJC7sGhHKHpBhUQ4wrBfEfKqK+v9xyuxHBHYARL8AsZRZrhdNW3L8XkEF+xAiI1H3yPCgSrBYqxf6xYu9+hfNetW7d6Ti3VxJLXJduzCKkm+T0E6fV+DQ0NnjPf0QdPWqXNTJLrCiNlcGOIBMP9BBDhd++VXQyPlEH7k3P0hVMKdJkzihuDduzY4fBbT5LNLY8nqbBt1tkkIyDa2toGTC/WzvYVmHodQPuy0WpD3cp9GoezJKhq7Uewz9P54PyobMwyRGy+Qo45ffrmQwaTIyJ+HpaDWiYXGeZvOoj4OFLWum0feaWuRA/6iXVf0Qri1bXaoDgltM+HwgsqtiwiGz2VVmAcW7kHcjmQV8o1IaOVhvUBYuJmf/3kkfeIlxnL5pYf6PMVgh2HMK/Mkh0Sl4M4cuTIwBfXaEYk5YLrA4S4y7PB/YTm38Nl6InUVS2vrahuDu+vv83NzQMnFxnn8sT4T6700zpO8QHin1OKjK/s3Lmz1y7r+++/H+ASEHOqDPxnisAl0Qq0hSfGfnF18O12CQJTR6gM+SRp7ZTHqPgtroO50XIEI4N2ly4fvQXmu8PlMCyvNwX06kx/QLzx7v6wBAW4nSs2tbKR5nivokL0u/iaKd7OQnizQhMzVI59yZSfkXL0VFYtHG3XISogNBrNwDEqsJgcoFh7GtYfEIvqWuJ4UuIK074GA8iVFZWH41wGYmIh+BY5ANPWnOkEsWyDISxciv6NLX9DJdiZxetaeu6bOQtiYY2exwPMLh3YmAJRuFszOEyKnmLbZ3L2d6Kgwdh15ucsiIn5urHkmsAtWoOzIFLzwDy2/b1vT6tBRb9AjFZpl7NdCSogppXon/QXmf/Ntr/3jSMk/vVCOTSaMohEuf4dtitBBUSEDIXY9rW7xahQkDKIpJXmPWxXwFkQM8uMKT4MJb05Yz4CS8eSKu1ISiCGSYyNbFfAWRDxCpixxGhnLV4O1lICkaDE9rLtvDMgXlh9iE/2x7S/JmDXv6x7p1n+SvpwlWyVPSY5XAC/9OZmLY/KGFHBdvCdAZFWoJ/hzdKax5vsesashKszi3XDpqr3h4xTg29yRF13oa1wMksNmU6DSC82vMjWdgYVELEKaBsbfvmJiPbUNRZld39S85oV3eOXqIQ2Ow0iM785kAuYHpgFnTUPlmmffLJMHen5GmXhwYYeubkpBYZ4v26tIgRAP6G0xZG1SRf0VD4kGC7V7woU4+fI5tbqLzS1MZWO3x1EQpV9EHwxtYQFqkbG4ffpxVCPlnBf6SX4S9bZUudr+CL8xy4gQrKPfuEIiM7aq20a9HL5AX5KrjZuaoHu2RFKvTxWAb4fLoUNQQD+WZAEP88Vob9wshDqlSOv5Ynw64EAcT5Ygp0IARAD2eVsHZFryH2hTD9tRH6a3f1+nghjbHwgA3wnQaZ/rUFrsXlAyvy3td7BAPZV9+vIet3udtwofvnixYu0nDZ54cKFgdXV1bzXNmKxYwR1ydNKsPSoN+vTR67+bFZa2fF5k8s/nxMo12ckC/ekZ5ZZ0lKWv5e8bCMUX1NTwzt//rxTJ7/cV2trK+cJATMQ/MWW30atMi+158uCtYbwQBH2ma1r/VZAHR6+Avz/zZzsWha9e/hJ6uF2L5Hweb12a2R9E1RQ86S8w/PGrWqRhEpwSkfe+QuIO2kV2qXroGMDbPmh3qQJiJbbv7ePAO3wCJYQVzv/MDwb+Xz5+mZ/poNGl4Z0O+m5s6Wo4M2df3fjrmbvhBzsM2dmhQEi4rfphc12W8Kimn3RkVKoV8B8EdbhMVQCdzklwOpEjAKCltfqHMpEcHeR49SvNv+KyUF1slrbI6n5rSodN1iMGh0aE7LMbTPXwAsJgrDZEuZUfxoQqiT6bGXWD3x4zKnQp1md6kFJYroekw19+KRsz7yk19+b5c42TbIpwR6IUKnpjB0QbRMLoWRb10iqmryGyi1G717GF47Ici0lF5phr9yxSm00HzA71NWRE5AvPAq2oQMTVXCdqwcvJi1Wie21F5AoGdhk77rEHGSbveuWbP/UP0YN22wZ/mIS4jtH7UJQb8PDgwH8W0f9j5aBH9+70Gw2e8Ur0UayqbF2+ld/LATAT9sLygQ19Lr9Pt/cMVHdXJW1a5/NN5B04H6vJLnB2Pl6cjp87cWyRrsQFpQfHhUmQR1OI7Lee5Ja/+r/biCuMw6eWAiKyf7qZ7YD66zxAOLWyZMnbSadTS9DooaIzDbHCav5CEy3U0qJKnuBLf7A6D9K9d8xI0BkbptbrrMLYfEOQ3ioFHYql4uccV3LUB/qmZ/VSBwe9Hzx4dQ4QDNv6LK9D4yptzfZnFx88803nlEyuM/TEGIVWFWa6oDNlgGC4KBoKfR+qkJj9yXKiatNo/jSI04n1CUoEO0PP/xgc7B/6DRedWi6A13E7ViZcUN7e7vNBe3Zs2ftBuu5Ak1kgMhEKasxowR+lr6au5muX7/uGa7oexpptbEq4+6S7bDDmYML3jGmh0hsfyyrLyPHkjN01tstNV6tnedIMrT1+UBqXvPuc+fO9bnVs2xdY3IgOXhTgUD60j6l2Di7rzIeOu05cWpQQg76saOBisu37F6ynrDbMtLetaTzpNQgWC0xF9rZtH8fpT20B16i9w/x+QDhUJK0NTE5UobvLi8v79EyJq0yJA8RWyhD4AGmy/KNLay9SOkWerkSTCMHVoeDGKdADok2QlzrtZcuXRowvRSabZ1yUoXAFZt+ebHMMIHtOLiFMovhef4Ck8Nvn/LE+LcxUkN9lBTS+QmpvaJsNY7QfCd1pdHuOuQPqWcKjS8FkH+dVINKoSVcHae2vzf1h9ZzpdqnrZ8DpRtCkAS/lFnY8tA856FFWbUaTkIutLn7ZytdYWQ31j5SBW7OqYMf2BObGRWCIJ4ZReDzsQrk9BMuSYAwd4SJ4ePpxdD0Y8eO/aE+dOgyTSs1jk9SwR9xAeJnZxLTrL87RIS3JijBXTPLdA/8wV5uo0W1uuBpxdCMOIWxLlSKHQ0UYxfI2dNNngjruGfkv4MA/EIIgFrilGBdZgk046U1jZS/lfcfbJcmigZtTPYAAAAASUVORK5CYII='
        // return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGIAAABUCAYAAAB5huK+AAAACXBIWXMAAABPAAAATwFjiv3XAAAN00lEQVR42u2de3TU1bXHP+c3M5m8SIBAIo8ABpC3PCJEsbZWrdTbp1rWtdy76gMjjyugJTxEHhZUHtqqeEUgdNWlty5vrZXrWlWaCt7WWwupvBbyfgTKwySEkJBkkszjt+8fmQkzw0wymczvN4TwXWvWmvzO+Z2z9/7OOWeffR5RBCHtP6So3sUdbjuJ4uY6YgUF6JCscXJAOoUHVqoXg5MBuHW5zN9+mtVIqNTraDe8RAB4bVyTO4HsndNUtS+Z0Ytlyd5zLEcj0PjShoquo2UE/6i9pNw7lKyieapc/XiNDNx8jGPofpnFm9EGuDmHhhtBi7cuHRIexJZIustJGkCAFXVAo5FClajSZ8mu6nrGBjBmhXEZPLbrefWbeOtxLSH5CfnE4eG7AWQIZHfhWWWbJuLy0NQaBNDgnmEM//RpdTDegl+LSJ0pb9Y2Mr2ZDAF7Aoc1lyKgS7qlDz+7ToJxqF2nZtjs/KV5/FXQWM+NGh7vAwEs8OUy9U68hb3WMXkI+fhPDdy4AwbgdBvn4y1kZ8C7c9TRhK4EeKXW5m8Kqmu5ZIYgI5+XiZcqmVHnZGyVi8GeahLwQEoWF1Jt7LbZ+PTMGrU63gYzEik2cPo5SIp8ucxLIyW8rXKMqvzuFTJn60le9dZ82UHwd5vl8neLlcrcG5hfvFz9Ot6GizW6FohUVXv/aMBh2twgabqc23qSV1EQMHH0d5t9aRpgAY9O9+Kv2WSbLjXxNpzRMIUI+yyRehe9rpi5twZvXpebVJ4Q6bVAJplvInNgPBGPSkOjo501eeM0X1ezxSzDmA1DiejzlKxDwx6TWhTgBvKlwRTLmAxDiTjrYAaWGBbYNLjbxy2WR401i/kwjIjsufIWHgyJ4O4tZZWBNokLrO0vIjTK6vi+IesZGng8ZIZLHrlYZn31T9bGIYTvnDSCCX9aqPZGqZZBUjWQYQgRXgMPmCc/DU6a/Dvp/lUpa0kAEk3+2En48xF2R6uWIUT0XyT3GTr6KKhxcF/w4/eLGBOwrmImFOjtqNkQc7kbuM1oIpTGN694vkltQ6dpwUVM/ugwqAefRKuSIWOES2dUQOgi1hCouEj/kGlvKTVqiSwtuUiWXTWvEhsKHSyDu7K9eIV6O9oyDCFCKQyLVzVVANjDJ+9boZYD1BoqRCCK2/m+IR1IWQXpZgRPcjdIuvG1mANjzKVjfJBOgW8ryrWAmBMxWSSJLow03I8XuP0FWWJwLaYh5kQUz+NjXBjvQir422mWG1yLaYj5YH2qgqHGzdf94FtUCoEbF8gKPGRpmjnzax0s3ewU71qhNkZbRsxNpmzUihnqC6iEEFw8JlJSgemTuhKdqV1nSX7V62p8NO/HvGsa1oOPzIrzSD2BIfEpck+zVsrkjwWqHNwSrS4xJ+LASjUXDQydSnk3wg3K5ucBz99Vn8YlvOGdWSfYiNqLM8Z9LVQq2YbLMDIsMDiN+ceWqTeDk/L6Mw2hGg8NpnycNCI0Jls4OWkkt0erkmG7OAatlb7H9nM65mQIZKVRVPYr1aHXr03bxXFstjqDG0MWhpJS+ZVRcscLhgYi8rJ5OqYtQockK7Unl6s/GWwX02EoETueU68qC7EZuL0ta3xvJhpvFvNheGhOCpVKSaC4XV2Ul8gJA8j561K1zyTbmAoz5sDUvany0p6UsksNZLbZvRSaTi6tV6q9oearGaYQAXBXPyZuPsaxaFpG3xR+fybCvDlz5f0TjfyEBsLPrhWkJHC67j9VP7P0bw2m7X0dVE5pVGOFQLKNzyLJ+shzMuBEVSskeMusc5Ddb668Z5b+rcE0Ik69hN7mva8AOozIoD6SrJ/XkBHxEq0C0ehmlv6twTQi3ge9pYhpSxIerCQvkqzHf6l2olHdfAoqHASwgj2bqWbpfwVcNPrLadr5iGHPyh8PlvIvbabeG1dio4q4LeXNldwdlS2Mf1YSKVR/aa2c3A2S3nCCH5U18J16NyPdLro11jdpoFLwJFqpzLCxPzOdortq+fDll1VdW1RLf1K+rK4nFycOU4gYtlhmHPyadVG3Px2yUvl72WvKlDlEzkJ5+kQ5K7CQckUL9j9UE/TcCjUjMli2d5V6JdK6MmfJufIKehlPxGNyHEVOuztBHZQGI/pz+1eL1RcxldGLwfPko6MX+UFzK4ymK/U0vZuexH9Xv64eiuSVG56S9wwh4lsLZfzRRp49V8OPgNgu0qgmZft0Y9GCfrwxe7Zq97m/MQtl5p6LvIEbsND++JjvfSuM68m0SFbuYkbEuGdkUUkNUy46GOE7KowWA6VaUlSH5CQqHG+onlGX9ahcQqNL8wJPrOVsWquo4jeqRQ+t3RO6jNnytwtOJu46DwFN2t9gsYavfAs4HPSIpojJv5Pu7xdxoVlmo+RskrUr+SK5t9A13BagqEXIfEr+i3yRCw4m4ubyQcQOcLXQvS9JpuEk+MPbM+z8kqrH35WscFnajOHPyC/Ka/i35slTBzC+P4qOU2YaCT54zwFu2kZpqOSoRDlwgaUdkQAAporgjlbzdsJXZ/6V+1zaLM7QBTIpbmcQ2okbn5aNhu5SjwRer2/ofHnN/3GbiWhwc2cc1Yga45fL+JIa8mN6uDJaWOBQJbMnrZLRvkdtJqK+kSHx1iMa7D/L76+2q/F2nuMPvu9tJiI1hZ5Xm0Kt4Z7lMtbhpt9VdRmegoo6ciZvkFEQBRHnakjraOPD3ov88qqUWcFnB1gJURCRkUJ9R2sR5x18O+4DtG/3Y5AcFQ18D6IgwuWg9Kr8dYVB7lJ5uNX1CaPggSE92HzvYLLYQMqQLBZfca+uGya+IA+1mYj+vdjSkVpESRVT4yKvwOAerD28Ut1fNE+Vg3IcflG9MKQrrwUsGSs4fp78NhNRvFitb25m/hddXaWorOcmU1uwd0Py2N68fnSNmhOcfCmBV4IjfBV1DA0mIrJGXKjUTb1YnWrnC1zeZxbv277Le4PPIcdCwaBzzRGVq5EVQa7YQYNbs1m3e7maHSp5eBJ3BlvZA70vcyPQJZWUSE8hHnleLfR9LyiQlPcayMnKZNzZS+S5dAbVOunX6CbNopHlAQ1XU41YuTxw+d19ClweyHyDmm/vrAWwg6bjUopq0bnYPZXTdo2jo29g+8ctCZoAkW09aCe8OliFZ7c/F3gBuz+2lvBWcAu1JfqvR/iMsSnyteFocP9aGb3lECkTB5C99StsY/pi7ZZKmkWQT49QkZiK9e5B1P1xN+VT76bu1w+rqO+3AOBfRbyXQYdGUH8dVTfmDSD2S2fBP19Sa8Lm+5l4sKFd4SJZQDFdpLmp6HDfaG77ZLbaHjPLxxEiYlMP4QxLhA7Z6ezLSGC9w0q/I6UsaHNU1ttVjujNgv0rwpNgmyPiqiMs0SrhSRGnb0NWU0DMTaGyxduIMcNMkeZxzB863JjBn0vWqHv9HydPF93hRkXUMrwBxDF9WbRnmVoZKkvuBknfuYcqnIQmWIBk0Fxuv22QTTFza/J0uRBv+8UKmk/ZYFihbz1PBT92rFcaTipadQS8DkNePwrCkQCwczdVuGi5lTnBet9ACj4+wmZ/yR1uujNNJN3OX5MsfFRah26zXJ2OqstJwtCeFB9apf43VLpFp1aH1DYV+rbqyVQpBbJCtgxv95WTxk92LFEfhCqioEBSXr5EbaskNJV3UQFY8qXY42F8QIjY5yZGs6XEZPRKY+fXr6iQJzpTZsreOic3X2FQgd5d+PzcK+qb4crVnpBy3UPP4H8zADBmAD/es1j9T1ihnvCOvRGMNyk2dmkAnkI1AY1TAb65/xq0dhV/FJQ5wt+Gk5PBByF/SArOVXHHqEUS1pj6RpVps1AZ8C9pNLhrIA+EI+H+52QkM0QC3POWoMOQ7rx3OesmNWBUL1YH+O8d5KO7wm8mTkrgHayE9lassK+MHw6cJ38I975ro8pIT+Y8OmCB7w3jwW3PqA/D5f+wjH1tugJDgyQnvw2ZffRSmXT4PLkNtR0kvOfB/vj3eWPTFFUWMv0RKcdKz7Da6JDZhc3lr6r7w9bxUznSfyDTTz2vtoVKvnWFDNt+lgMRjQl+9aIoZ5PK6hiGbicy58i/l9fwTovLpAK42c5b6raoKskXafN6uBvuuokHtj2jPuwURADwmEir+650SLZzzLFODY602AdflkkfnGBLyLlKSxCwW3A2blB2iM+mkrggpxs/b9X708DRwCD7dDkUSZmz1krfD46yhcYoBBIY3JMC35+dp0UACTPksNPJTa3+/Jo8nv0UqpHhsuStlu/sOE5RVK69gM3KPtd6dbPvUaciAoi8L9cBK3vZoMYEJzVfb+GJoJxghAmudpquyYfvDuWeiHapa4Cb0YkzpcT/8c2/kBnH9rSDBAXfzuEbwUmdr0UAd7wgj39+nMKINk17Y0rdUyiqvMRwEugbVbTBS0L/bjx4ao26Yt7SKYkAuG2JTPl7Kb+NqJvyD/dEu15hgWG9eeDgstCTwU5LBMA3XpS8/zvJ9khjQlGhaaxh/EDG/mO+2hMuW6cmwgc1TXaIiwnNcbVYwNt12RL4h2udmtBa9k43WIeCbFB5Y/vwcEYidc0bIKL9iTbN0LFZqRybxZRISKAd1V2zuPU5ufNMDavO1JDXvIQc6WqdFXol88XwG1i8dYGK6NoKH64TEQYFBZLyRXceOHSWR2qEwW4X2aLRtCsEwOk1nnCmp53DI/rw9mcLo78t//8BVKY32uiCjpQAAAAASUVORK5CYII='
    }
    iconEye(){
        return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAASCAMAAAAACFwnAAAAAXNSR0IB2cksfwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAelQTFRFCmbCCmbCAAAACmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCh2aOZgAAAKN0Uk5TGhIAC0GHv9rh4tvEkEoQDxsGN/T/8tW7utHu+Z8JL6PzmD81ifixPQF86PzW+lUE5v7vkgckvtRFrPtHDSzGw/3SNDzej5akdVwIdutTTViZLjHjHODC3GcDUfDfQwywExel0Gw5J4MlEU7LCp4OKoImr24Ckbaam64Uq0n1ZbcgcudqBVR3tXNmHcUZ3eSMRpQtvKCogfdSlSOGqSmc2KZWFW0Lv3oAAAHYSURBVHicY2BgZEIAZhZWNnYOTi5uHl4+JGF+AQYER1CIR1hERFRMXEJSSkREWoZFFiGHUCYnrwCUVFQCSgoqq4iKiKiqqWugK9PQ1NLW0RXV04dyWQwMdaSNjE1QlZmaGZpbWFpZg9g2/CBDbIXtlOwdHJ00kJQ5uxi68rPouoE0uHNyOIHEPAw9vbx1fXzhyjT8VLX8mZgCFAKZmIKCRUQkwTIhoWFMTCzhhhGRUGVR0TGxQLM5wuKYmOJFRES0EpgSI5kik6yYgdqSVVMgylKlOU2BLFkRGSamOBEQ4JQ3SGNiShfJAApnJov4g5VlaWfbAPk5IrlMTHkiUJDPxFQgUggU1i8SKQYrKykVkc8ButCozIRJIxmiSgoYeuUiwAiKVHFQM4G4TbDCsLKKianaB2h3TS1IlW4dE1OVoxjQEW7a9XGwABEsN8xuYPIXaQTq1gsXEWlqbmFianVoY0poNyyzQYRbS4dUdGdXd20+iJORXwIkezh6M3z7avtzkGNBY4KjYf3EWslJsEjsmqw7Zap2qH8kWtT3TJOebiXCWQeOxZIZOiIO0w1nzsJMIUxdKpyGQHfFzy6YEwNkaAXMxZaQQEHJO89RAeTT6fMXLFykgSQDAMWiXV7UNtNdAAAAAElFTkSuQmCC'
        // return 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDI0IDI0IiBmaWxsPSJub25lIiBzdHJva2U9IiMwMDY2ZmYiIHN0cm9rZS13aWR0aD0iMyIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbGluZWpvaW49InJvdW5kIiBjbGFzcz0iZmVhdGhlciBmZWF0aGVyLWV5ZSI+PHBhdGggZD0iTTEgMTJzNC04IDExLTggMTEgOCAxMSA4LTQgOC0xMSA4LTExLTgtMTEtOHoiPjwvcGF0aD48Y2lyY2xlIGN4PSIxMiIgY3k9IjEyIiByPSIzIj48L2NpcmNsZT48L3N2Zz4='
    }
    iconEmail(){
        return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAARCAMAAADwppwCAAAAAXNSR0IB2cksfwAAAAlwSFlzAACUQQAAlEEBJp7zKgAAALdQTFRFAAAACmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCLWnj+wAAAD10Uk5TAGjv//qg7XcMOeQ232Et6FS0/hge0tE4LGwE/UYX0G0Ut/DOr56DfMsprgo/cAJ4KAHKDh2nv6jhrffj2rsVlTAAAACgSURBVHicndBrE4FAGIbhV5uQJCJkO1HkVMiZ//+7bGYP7YwZM+5PO881ux8W4J9qCpJS6xQUrdGs1NJ0CqhtdMR9s2v16LFvD4ZcTMcCATBiMnYmUAUmZHcFTAkQwQCeH5BdhjCaYW9umADyU2GcYN1YLNOVDOE62cB252Z5uheA7EO5f8ryY8FBPZ3ZXopfXOj5ekPRnRc/nq/f//ulN2LbD5uVNsjQAAAAAElFTkSuQmCC'
        // return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAARCAMAAADwppwCAAAAAXNSR0IB2cksfwAAAAlwSFlzAACUQQAAlEEBJp7zKgAAARRQTFRFCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCAAAACmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCCmbCpNgZMgAAAFx0Uk5TJMb7//zJKv3iZVpg2s6s5KIKAAaR65z+bDjso/JISjMLB0M5qgOapgUClFPvifdJL/Sdivo/Ab4ECel0qJiG7vA87VRG84ibRDdFT/GeCEGQTdinkvZjHh1tmeoGwTLLAAAA9klEQVR4nGXRa1OCQBgF4NWlqMzQ4yJIXgIDgdIyDdMuYlmZdrWLlf//f4RQA+T5sDN7nnln9kJIIkmXwq2skgS/tr7xL6lNLk2SW0Imi1hyTMxLhMoFZTsb74ulMiWSjIqS2Ql7lRU1BICqsKv+9bohlrAA3gNoQs0MessW9xACqvt5X6x64wABcD7gsHnkidVqHwPRCTgdWjtxbL6DOHSN3qlwdi5e9Jn6C4tTocvcAbTL5hWG1zdqCOZtf+Tt7sbeMrlnug9UhvmgDMMLTlxbDyYeDeUp8iJO2X3OvUhk+vr2/jH7jGT21ah/T8lovvwdlM7HP9ajK1tZW1VOAAAAAElFTkSuQmCC'
    }
// [${this.uniqueTagAttr}] label,  [${this.uniqueTagAttr}] input:not([type="submit"]) {width: 180px}
// [${this.uniqueTagAttr}] form div {width: 180px}
// input font-size: 1em; 

}

class AlfaAuthUser {

    constructor(settings = {}){
        Object.assign(this, settings)
        this.auth_failed_redirection = this.auth_failed_redirection || '/auth-failed.html'
    }

    verifyJWT(callback){ /* similar code in ALFA_LIB/alfa-auth.js */
        var apiUrl = this.server_side_api +'/verify'

        alfaFetch(apiUrl,  {
            method: 'GET',
            contentType: 'text/plain',      
            onSuccess: function(response){
                /* response.responseData is not sent (unless unknown error) */
                if(response.responseMeta.status===277){ if(callback) callback(null, response)}
                else{                                   if(callback) callback(true, response);}
            },
            onError:  function(err, response){
                if(callback) {callback(err, response);}
                else {
                    window.location.replace(this.auth_failed_redirection); // location.replace() blocks browser back button - not anymore
                } 
            }.bind(this)
        });
    }

    clear(){
        sessionStorage.removeItem('x-access-token')
        localStorage.removeItem('alfa-auth-last_user_id') // localStorage for next day login
        localStorage.removeItem('alfa-auth-last_user_email')
        sessionStorage.removeItem('alfa-auth-user_info')
    }
    clearUser(){
        sessionStorage.removeItem('x-access-token')
        sessionStorage.removeItem('alfa-auth-user_info')
    }
}

import alfaFetch from './alfa-fetch.mjs'


 


export {AlfaAuthUserUI, AlfaAuthUser}
