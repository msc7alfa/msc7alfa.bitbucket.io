/** alfa-setproperty-bypath */

    /** setPropertyByPath  9.11.8
     *  @returns parent property from path 
     *      or null if error occurred
     *      or value at path if opts.get=true
     * 
     * @param {Object}  obj   - an object with properties, in prevoius version it is instance of alfa-autopage, which has dsApp property
     * @param {String}  path  
     * @param {*}       value 
     * @param {Function} errCallback(err) - optional
     * @param {Object}  opts {
     *                      get:true - return a value at path instead parent prop
     *                  }
     */
    function setPropertyByPath(obj, path, value, errCallback, opts) {

        if(typeof path!=='string'){
            var err = {fn:'setPropertyByPath()', message: `path is not a string, value: '${value}' ${JSON.stringify(opts)}` }
            if(typeof errCallback==='function') errCallback(err);
            return null;
        }

        if(!path) {
            var err = {fn:'setPropertyByPath()', message: `path: '${path}', value: '${value}'` }
            if(typeof errCallback==='function') errCallback(err);
            return null;
        }

        var prop = obj; // find parent prop

        // in case when needed to create new prop in the middle of chain
        var lastFoundProp = obj;
        var newPropName; 

        var propNames = path.split('.');
        for (var i = 0; i < propNames.length - 1; i++){
            
            prop = prop[propNames[i]];

            if(typeof prop==='undefined'){
                // create new prop
                newPropName = propNames[i];
                lastFoundProp[newPropName] = {};
                lastFoundProp = lastFoundProp[newPropName];
                prop = lastFoundProp;
                // console.log(obj, {lastFoundProp, i, newPropName})
            } else {lastFoundProp = prop;}
        }
        var propName = propNames[i];

        if(opts && opts.get){return prop===null ? null : prop[propName];} // ------------- >


        /** ***********************************************************************************************************
         *  error resulting from undefined or null can be ignored, since these are legitimate values to be stored in ds 
         * */
        var errObj;

        if(typeof lastFoundProp==='undefined') {
            errObj = {fn:'setPropertyByPath()', message: 'lastFoundProp is undefined!', propName }; // `path: '${path}' not found!`
        }
        if(lastFoundProp===null) {
            errObj = {fn:'setPropertyByPath()', message: 'lastFoundProp is null!', propName }; // `path: '${path}' not found!`
        }
        try{
            lastFoundProp[propName] = value;    // *** asigning value to a prop

        } catch(err){
            errObj = {fn:'setPropertyByPath()', message: err.message}
        }

        if(errObj){
            /** ignore, enable for debugging */
            // if(typeof errCallback==='function') errCallback(errObj);
            
            return null; // --- >
        }

        return prop; // parent prop from path
    }

    function getPropertyByPath(obj, path, errCallback){
        return setPropertyByPath(obj, path, null, errCallback, {get:true})
    }


export {setPropertyByPath, getPropertyByPath};

