// alfa-modal-spinner 20.5.14   es6 module

/** example with default values
 * 
    <div id="alfa-modal-spinner"></div>
 
    old:
    <div class="alfa-modal-spinner" 
        keep-modal
        s-speed=".9s"
        s-diameter="80"
        s-thickness="8"
        color-spin="#333"
        color-track="#ccc"
        overlay-rgba="255,  255, 255, 0.5"
        >
    </div> 
 */

// poor man's jQuery:
const $dqs = document.querySelector.bind(document); 
const $dce = document.createElement.bind(document); 


function AlfaModalSpinner (containerSelector, opts){

    opts = opts || {};

    var defaults = {
        speed: '.9s',
        diameter: '80',
        thickness: '8',
        keepModal: false,
        colorSpin: '#333',
        colorTrack: '#ccc',
        overlayRGBA:  '255, 255, 255, 0.5', // dark: '0, 0, 0, 0.39'  // 

        multipleInstances: false,   /** if false, one instasnce keeps track of all pending spinners and keeps spinning until all are cleared */
        
        uniqueAttr: 'alfa-modal-spinner-from-alfalabs-net', // unique attr for CSS top container selector - by using common selector instead of containerSelector, only one <style> tag can be used
        styleTagAttr: 'alfa-modal-spinner-style'   // attr for <style> tag, to prevent creation of new tags for each instance
    };
    Object.assign(this, defaults, opts);
    this.containerSelector = containerSelector;
    
    this.getAttributes();

    this.create();

    this.insertStyleTag();

    this.spinners = [];
}
AlfaModalSpinner.prototype.getAttributes = function(){
    var container = $dqs(this.containerSelector);
    if(!container) {console.error(`[ala-modal-spinner] html element '${this.containerSelector}' not found!`); return;}
    // this.id = container.getAttribute('box-id');
    this.speed =       container.hasAttribute('s-speed') ?  container.getAttribute('s-speed') : this.speed;
    this.diameter =    container.hasAttribute('s-diameter') ?  container.getAttribute('s-diameter') : this.diameter;
    this.thickness =   container.hasAttribute('s-thickness') ? container.getAttribute('s-thickness') : this.thickness;
    this.keepModal =   container.hasAttribute('keep-modal') ? true : this.keepModal;
    this.colorSpin =   container.hasAttribute('color-spin') ?   container.getAttribute('color-spin') :   this.colorSpin;
    this.colorTrack =  container.hasAttribute('color-track') ?  container.getAttribute('color-track') :  this.colorTrack;
    this.overlayRGBA = container.hasAttribute('overlay-rgba') ? container.getAttribute('overlay-rgba') : this.overlayRGBA;
    this.uniqueAttr =  container.hasAttribute('unique-attr') ? container.getAttribute('unique-attr') : this.uniqueAttr;
};
AlfaModalSpinner.prototype.create = function(){  

    this.container = $dqs(this.containerSelector);
    this.container.setAttribute(this.uniqueAttr, ''); 

    this.container.innerHTML='<div class="spinner"></div>';

    if(!this.keepModal) this.container.onclick = this.off.bind(this);
};

AlfaModalSpinner.prototype.on = function(){
    this.container.style.display = 'block';
       
    return this.manageSpinners('add');  /** returns spinner number, NOT index */
};
/** also event handler */
AlfaModalSpinner.prototype.off = function(ev){

    if(typeof ev==='undefined' || ev.target){
        this.container.style.display = 'none';
    } else {
        var pendingSpinners = this.manageSpinners(ev);
        if(pendingSpinners===0)
            {this.container.style.display = 'none';}
        return pendingSpinners;
    }
};

AlfaModalSpinner.prototype.manageSpinners = function(mode){
    if(mode==='add'){
        var n = this.spinners.length + 1;
        this.spinners.push(n)
        return n; /** returns spinner number, NOT index */
    } else {
        var nr = mode;
        var P = this.spinners.indexOf(nr);
        if ( ~P ) this.spinners.splice(P, 1); /** ~ Bitwise NOT */
        return this.spinners.length;
    }
}

AlfaModalSpinner.prototype.insertStyleTag = function(){

    // if ($dqs(`style[${this.styleTagAttr}]`)) return;  ALLOW multiple spinners on page

    var sel = `[${this.uniqueAttr}]`; // instead of ${this.containerSelector}

    /* calling body append will add your new styles to the bottom of the page and override any existing ones */
    var styleTag = $dce('style');

    var diameter = this.diameter +'px';
    var half= (-this.diameter / 2) + 'px';

    styleTag.innerHTML = `
    ${sel} {
        display: none; 
        padding: 0 10px; 
        background: rgba(${this.overlayRGBA}); 
        position: fixed; top:0; left:0;right:0; bottom:0;
    }
    @keyframes spinner { to {transform: rotate(360deg);} }

    ${sel} .spinner:before {
        content: \'\';
        box-sizing: border-box;
        position: absolute;
        top: 50%;
        left: 50%;
        width: ${diameter};
        height:  ${diameter};
        margin-top:  ${half};
        margin-left: ${half};
        border-radius: 50%;
        border: ${this.thickness}px solid  ${this.colorTrack};
        border-top-color: ${this.colorSpin};
        animation: spinner ${this.speed} linear infinite;
    } 

    `;
    styleTag.setAttribute(this.styleTagAttr, '');

    $dqs('head').append(styleTag);
};    

export default AlfaModalSpinner