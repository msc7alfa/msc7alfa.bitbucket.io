/** alfa-json-input-pop   19.4.11c    alfalabs.net   */
// poor man's jQuery:
const $dqs = document.querySelector.bind(document); 
const $dce = document.createElement.bind(document); 
const $dcf = document.createDocumentFragment.bind(document);
const $dct = document.createTextNode.bind(document);

var alfaJsonBoxes = {}; //  for clearAll(), closeAll(), openAll()

/**
 * 
 * @param {*} containerSelector - unique selector for an instance of this particular box, example: '.alfa-json-box[box-id=ajb1]'
 * @param {*} opts - {err, raw}
 */
export default function AlfaJsonInputPop (containerSelector, opts){

    opts = opts || {};

    var defaults = {
        uniqueAttr: 'alfa-json-input-pop-from-alfalabs-net', // unique attr for CSS top container selector - by using common selector instead of containerSelector, only one <style> tag can be used
        // sampleJSON:  '{"a":1, "b":"two"}'
    };
    Object.assign(this, defaults, opts);
    this.containerSelector = containerSelector;
    this.getAttributes();

    this.createUI();
    this.insertStyleTag();
}
AlfaJsonInputPop.prototype.createUI = function(){

    this.container = $dqs(this.containerSelector);
    this.container.setAttribute(this.uniqueAttr, ''); 

    var titleBar = $dce('div');
    titleBar.classList.add('title-bar');
    titleBar.append($dct(this.titleText));

    var closeBtn = $dce('a');
    closeBtn.innerHTML='&#10006;&nbsp;';
    closeBtn.onclick = this.close.bind(this);  // this.titleBar.onclick = this.toggleVisibility.bind(this);
    titleBar.append(closeBtn);

    var content = this.container.innerHTML; 
    this.container.innerHTML='';

    this.txtArea = $dce('textarea');
    this.txtArea.innerHTML = content; // this.sampleJSON;

    this.container .append(titleBar);

    this.container .append(this.txtArea);
}
AlfaJsonInputPop.prototype.content = function(str){
    this.txtArea.innerHTML = str;
}
AlfaJsonInputPop.prototype.getAttributes = function(){
    var container = $dqs(this.containerSelector);
    this.id = container.getAttribute('box-id');
    this.name = container.getAttribute('box-name');
    this.notJson = container.hasAttribute('not-json');
    var title = this.notJson ? '' : 'JSON format';
    this.titleText = container.getAttribute('title-text') || title;
};

AlfaJsonInputPop.prototype.string = function(){
    return this.txtArea.value;
};
AlfaJsonInputPop.prototype.jsonObject = function(){
    var obj = this.validateJson(this.txtArea.value);
    if(obj===null) {
        // alert(`Validation of POST JSON failed:\n${this.validationErr.message}\nit will be ignored.`); 
        return null;}
    else return obj;
}


AlfaJsonInputPop.prototype.open = function(parentEl){

    this.container.style.top = parentEl.offsetTop + 18 +'px';
    this.container.style.left = parentEl.offsetLeft + 'px';
    this.container.style.display = 'block';
};
AlfaJsonInputPop.prototype.close = function(){
    this.container.style.display = 'none';
    if (this.validateJson(this.string())===null) alert(`Validation of JSON failed:\n${this.validationErr.message}`)
};
AlfaJsonInputPop.prototype.validateJson = function(jsonStr){
    if (this.notJson) return jsonStr;
    try {var obj = JSON.parse(jsonStr); return obj;} catch(err){this.validationErr=err; return null; }
}


AlfaJsonInputPop.prototype.insertStyleTag = function(){

    if ($dqs('style[alfa-json-input-pop]')) return;

    var sel = `[${this.uniqueAttr}]`; // instead of ${this.containerSelector}

    /* calling body append will add your new styles to the bottom of the page and override any existing ones */
    var styleTag = $dce('style');

    styleTag.innerHTML = `
    ${sel} {position:absolute; border: 1px solid lime; display:none; background: white;}
    ${sel} .title-bar {background: gainsboro; padding: 0 0 2px 5px; font-size: 90%}
    ${sel} a {cursor:pointer; display:inline-block; float:right; padding-left: 10px; }
    ${sel} textarea {width:100%; min-height: 100px;}
    `;
    styleTag.setAttribute('alfa-json-input-pop', '');

    $dqs('head').append(styleTag);
};