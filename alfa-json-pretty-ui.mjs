/** example for UI element */

const $dqs = document.querySelector.bind(document); 
const $dce = document.createElement.bind(document); 
const $dct = document.createTextNode.bind(document);


/** constructor 
 * 
 *  @param {String} elementSelector - a CSS selector, unique for element to create
 *  @param {Object} options - added to this.cfg, (also cfg optinos can be html attributes, they will be camel capitalized)
 */
function AlfaJsonPretty(elementSelector, options){

    var defaults = {
        author: 'alfalabs.net',
        name: 'AlfaJsonPretty'
    }
    /** ui element has an element on page */
    this.elementSelector = elementSelector;
    this.container = $dqs(elementSelector)

    this.getCfgValues(options, defaults)  /** element configuration comes from options and html attributes */

    this.createElement()
    this.insertStyleTag()

}
/** get configuration from defauls, options and HTML element attributes  */
AlfaJsonPretty.prototype.getCfgValues = function(options, defaults){

    options = options || {}
    this.cfg = Object.assign({}, defaults, options)

    /** get cfg from html element attributes */
    Array.from(this.container.attributes).forEach(function(attr){
        var name = camelize(attr.name)
        this.cfg[name] = attr.value
    }.bind(this));

    // log('getAttributes() this.cfg:', this.cfg)
}

AlfaJsonPretty.prototype.load = function(obj, opts){
    opts = opts || {}

    if(!opts.notClear) this.clear();

    var json;
    if(opts.isJSON) {json = obj;}
    else json = this.safeStringify(obj, null, 4);

    var pre = $dce('pre');
    pre.setAttribute('class', 'json-display');

    if(this.container.children.length > 0){
        pre.style = 'border-top: 1px solid gainsboro;'
    }
    this.container.append(pre);

    json = this.syntaxHighlight(json);

    if (opts.raw) pre.append($dct(json)); // to see HTML of the colorized JSON
    else pre.innerHTML = json;

}
AlfaJsonPretty.prototype.add = function(obj, opts){
    opts = opts || {}
    opts.notClear = true;
    this.load(obj, opts)
}
AlfaJsonPretty.prototype.clear = function(){
    this.container.innerHTML = '';
}


AlfaJsonPretty.prototype.createElement = function(){

    /** assing attributes */
    if(this.cfg.elemCss){this.container.style = this.cfg.elemCss}
}


// AlfaJsonPretty.prototype.getHtml = function(obj){
//     var jsonStr = this.safeStringify(obj)
//     return this.syntaxHighlight(jsonStr)
// }

/** could be called from outside:
 *      
    txtElem.setAttribute(alfaJsonBox.uniqueAttr, '');
    var colorizedTxt = alfaJsonBox.syntaxHighlight(txtElem.innerText);
    txtElem.innerHTML = `<span class="json-display">${colorizedTxt}</span>`;
 */
AlfaJsonPretty.prototype.syntaxHighlight = function(json) {
    if(!json) return;
    
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
};


AlfaJsonPretty.prototype.safeStringify = function(obj, replacer, spacer) {
    // https://github.com/davidmarkclements/fast-safe-stringify
    var arr = [];

    decirc(obj, '', [], undefined);
    var res = JSON.stringify(obj, replacer, spacer);
    while (arr.length !== 0) {
        var part = arr.pop();
        part[0][part[1]] = part[2];
    }
    return res;

    //////////// helpers

    function decirc (val, k, stack, parent) {
        var i;
        if (typeof val === 'object' && val !== null) {
            for (i = 0; i < stack.length; i++) {
                if (stack[i] === val) {
                    parent[k] = '[Circular]';
                    arr.push([parent, k, val]);
                    return;
                }
            }
            stack.push(val);
            // Optimize for Arrays. Big arrays could kill the performance otherwise!
            if (Array.isArray(val)) {
            for (i = 0; i < val.length; i++) {
                decirc(val[i], i, stack, val);
            }
            } else {
            var keys = Object.keys(val)
            for (i = 0; i < keys.length; i++) {
                var key = keys[i];
                decirc(val[key], key, stack, val);
            }
            }
            stack.pop();
        }
    }
};//end:safeStringify


AlfaJsonPretty.prototype.insertStyleTag = function(){
    var styleTag = $dce('style');
    styleTag.setAttribute('alfa-json-pretty-ui'+'-style', '');
    var sel = this.elementSelector;

    styleTag.innerHTML = `
        ${sel} {border: 1px solid silver}
        ${sel} .json-display {border: unset}
        ${sel} .json-display {font-size: 14px; margin: .4em 0}
        
        ${sel} .json-display .string { color: green; }
        ${sel} .json-display .number { color: rgb(139, 0, 0); }
        ${sel} .json-display .boolean { color: blue; }
        ${sel} .json-display .null { color: rgb(161, 1, 161); }
        ${sel} .json-display .key { color: rgb(0, 70, 161); }   
        
    `;

    /* NOTE: calling body.append will add your new styles to the bottom of the page and override any existing ones */
    $dqs('head').append(styleTag);
}

export default AlfaJsonPretty;



    // helpers

    /** not used, but interesting: https://gist.github.com/jpetitcolas/4481778
     *  from: https://ourcodeworld.com/articles/read/608/how-to-camelize-and-decamelize-strings-in-javascript
     * @param text 
     * delimiter: underscore or hyphen
     */
    function camelize(text) {
        return text.replace(/^([A-Z])|[\s-_]+(\w)/g, function(match, p1, p2, offset) {
            if (p2) return p2.toUpperCase();
            return p1.toLowerCase();        
        });
    }
