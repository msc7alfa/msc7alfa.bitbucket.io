/* https://github.com/RickStrahl/jquery-resizable */

'use strict'
const $dqs = document.querySelector.bind(document)
const $dce = document.createElement.bind(document)

var startPos, startTransition;

/** there are two panels, only one is being resized, other follows */
class AlfaResizablePanelsUI {

    /**
     * @param elementSelector - the panel to be resized
     * @param opts 
     */
    constructor(elementSelector, opts={}){

        var defaultOptions = {
            flexDirection: 'row',   /** @required */

            handleSelector: null, // selector for handle that starts dragging
            resizeWidth:  false,   // resize the width
            resizeHeight: false,  // resize the height
            resizeWidthFrom: 'right', // the side that the width resizing is relative to
            resizeHeightFrom: 'bottom', // the side that the height resizing is relative to
            
            onDragStart: null, // hook into start drag operation (event passed)
            onDragEnd: null,  // hook into stop drag operation (event passed)
            onDrag: null,    // hook into each drag operation (event passed)
            touchActionNone: true, // disable touch-action on $handle  // prevents browser level actions like forward back gestures
            // instanceId: null, // instance id

            openCloseBtnSelector: null,

            /* needed for CSS: */
            containerSelector: null,
            otherPanelSelector: null,

        };
        this.opt = Object.assign(defaultOptions, opts)
        // this.opt.instanceId =  this.opt.instanceId || 'rsz_' + new Date().getTime();
        
        this.elementSelector = elementSelector
        if(this.opt.flexDirection==='row'){
            this.opt.resizeWidth = true
            this.handleBorder = 'right' 
        } else {
            this.opt.resizeHeight = true
            this.handleBorder = 'bottom'
        }
        this.setControlBtn()

        this.setStyle()

        this.$el = $dqs(elementSelector)
        // this.$el.classList.add('resizable');

        this.$handle = $dqs(this.opt.handleSelector);
        this.$handle.addEventListener('mousedown', this.startDragging.bind(this)) //   .on("mousedown." + this.opt.instanceId + " touchstart." + this.opt.instanceId, startDragging);
    }

    startDragging(e) {
         
        e.preventDefault();  // Prevent dragging a ghost image in HTML5 / Firefox and maybe others  

        // this.isDragging = true - WORKAROUND when removeEventListener() is not working
        
        this.startPos = getMousePos(e);
        this.startPos.width =  parseInt(this.$el.clientWidth, 10);
        this.startPos.height = parseInt(this.$el.clientHeight, 10);

        startTransition = this.$el.style.transition
        this.$el.style.transition =  'none'

        if (this.opt.onDragStart) {
            if (this.opt.onDragStart(e, this.$el, this.opt) === false)
                return;
        }

        this.doDragHandler = this.doDrag.bind(this) /* make a variable for removeEventListener() */
        document.addEventListener('mousemove', this.doDragHandler)

        this.stopDraggingHandler = this.stopDragging.bind(this)
        document.addEventListener('mouseup', this.stopDraggingHandler  );
        
        document.addEventListener('selectstart', noop); // disable selection

        // TODO:
        // if (window.Touch || navigator.maxTouchPoints) {
        //     $(document).on('touchmove.' + this.opt.instanceId, doDrag);
        //     $(document).on('touchend.' + this.opt.instanceId, stopDragging);
        // }
        // $("iframe").css("pointer-events","none");
    }

    doDrag(e) {

        // if(!this.isDragging) return - WORKAROUND when removeEventListener is not working
            
        var pos = getMousePos(e), newWidth, newHeight;

        if (this.opt.resizeWidthFrom === 'left')
            newWidth = this.startPos.width - pos.x + this.startPos.x;
        else
            newWidth = this.startPos.width + pos.x - this.startPos.x;

        if (this.opt.resizeHeightFrom === 'top')
            newHeight = this.startPos.height - pos.y + this.startPos.y;
        else
            newHeight = this.startPos.height + pos.y - this.startPos.y;

        if (!this.opt.onDrag || this.opt.onDrag(e, this.$el, newWidth, newHeight, this.opt) !== false) {
            if (this.opt.resizeHeight)
                this.$el.style.height = newHeight +'px'                 

            if (this.opt.resizeWidth)
                this.$el.style.width = newWidth   +'px'                 
        }
    }

    stopDragging(e) {
        e.stopPropagation();
        e.preventDefault();

        this.isDragging = false

        document.removeEventListener('mousemove', this.doDragHandler)
        document.removeEventListener('mouseup',   this.stopDraggingHandler);
        document.removeEventListener('selectstart', noop);
        // TODO:
        // if (window.Touch || navigator.maxTouchPoints) {
        //     $(document).off('touchmove.' + this.opt.instanceId);
        //     $(document).off('touchend.' + this.opt.instanceId);
        // }

        // reset changed values
        this.$el.style.transition = startTransition
        // $("iframe").css("pointer-events","auto");

        if (this.opt.onDragEnd){ this.opt.onDragEnd(e, this.$el, this.opt) }

        return false;
    }

    setControlBtn(){
        var btn = $dqs(this.opt.openCloseBtnSelector)
        if(!btn) return

        btn.addEventListener('click', this.btnClicked.bind(this))
    }
    btnClicked(ev){
        var btn = ev.target
        if(this.$el.style.display===''){
            this.$el.style.display = 'none'
            this.$handle.style.display = 'none'
            btn.classList.add('open')
            // btn.innerHTML = 'open'
        } else {
            this.$el.style.display = ''
            this.$handle.style.display = ''
            btn.classList.remove('open')
            // btn.innerHTML = 'close'
        }
    }

    setStyle(){
        var contSel = this.opt.containerSelector
        var styleAttr = 'alfa-resizable'
        if($dqs(styleAttr)) return

        var urlHor = "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAAAQCAMAAADeZIrLAAAAAXNSR0IB2cksfwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAWVQTFRFAAAAf39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/Zg+ZugAAAHd0Uk5TADyq4vrjPQJBrOT54aU5AUex5fvgozIJ/f/8ohGnnAsOrv6VBgzFxsi607YFlp4Ej6+BMDM29vEpQyKOmI2rcNnMCtTLA8HqFA3nE9/Y0MQIz916ipF7YSH48x8o7+keKvQVd32EbmIHsm94bBppu7xnHL24ZsDb7i/oAAABdUlEQVR4nIXUV1PCUBAF4BWCKAFRCUJAFEOxEBEbdsGCvVeKFRFQwa6/X5py15m97BP3zHc2JBMAKE2TRisIOk0z8Eff0moQRKOpzdwAtnd0WkTJ2lU72qx2uTIOp41X63ZZqk7s6eU5d59SdYLHWz77/PLf9Hs5+wf+mDLo5uwfqu8LqKVgWGYmSPZGTAxTQnoSjrL7xgDG7WwgTFC9yTDrpqYpN6OwzjELczKaeaJndmK3QF1gEbsIRHGwRPSWV7BbjRHQgN0aiDhYJ3obEnbRTQJasNNBGAdbRG97B7tdHwH/fWEJ9nCwT/QODrHzU6/REXYBOMbBCdGDU+zOKBfE7hziCfacTFHFi0vWXV1T7kZg3W0a4I4NMlQP9PcMs2fpv6MQuy9XClTmppz0DxRsrvp+T5523gfmgVf2xSK1u0o8cvYD5J9qb1yyoPKcmnHUnk/ud1/x+UWrfX1759XKkyp8GKXPr2IjF89+66RALl3+/AOOKlaxxfR3zAAAAABJRU5ErkJggg==')"
        var urlVer = "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAABgCAMAAAAjDwKSAAAAAXNSR0IB2cksfwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAWVQTFRFAAAAf39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/f39/Zg+ZugAAAHd0Uk5TACF60Pb52Y4wBXf4//2WDAOexQlwoxr6PGmqu+K842cc/D1vogLGffMGH4rE5+rMmDMIExQKASiRz9/g1DYEhO+lEQenyHj7QWysveThuGY5/pyVugtu6fGPDh572MuNKQ0q3atDr7LTrkexwOW2YhX0MoFhwSKtRJ8NAAABY0lEQVR4nO2UV1PCUBCFVwU0kkgnFkRRrjR7wRIVURRRUCOKYu9dwPr7TZvkLrz45PjAecr5Jjd3N3tmASQ1NDaZzJbmFlDFtFpZRVybTfZ2B6vL6ZKAm6XkAfDyNODaoYNF6oQuDHzgw4CvBd0Y+KGn+qO9VtoH+gD6aRCUKiVBww/IpQMJhbUbHBGt32hscGh4ZHQM6vpnGp+YnIrrjkzPzM4J88GFRdW7EkvqKE3JZdmvpIxhr8pkjY5DikDURIP0OmRwpDZgE4Ms5KpjKWBghi0McrCNQRK8YXRiByDDGV7clTpk8joR9/aVZgoHqj8sHqntxo9PnKeWs/MLYvwgYru0/80o6vq9rhzX5nT25lazkaK2H8U7ZZTMvTH9B5mE6Hw8Svs4QAPxCQo4Us/wgsEr+DFI1+a0hIEAZQw8UHmjPf8O+JW8VCmJGb6k7E9S1k4FPhit38pnSfB/ub/l5x8c1UAbYZtc4gAAAABJRU5ErkJggg==')"
        //urlVer = 'url(./ellipsys-vert-16.png)'; urlHor = 'url(./ellipsys-hor-16.png)'

        var handleCss, otherCss
        if( this.opt.flexDirection==='row'){
            otherCss = 'width: 100%;'
            handleCss = `flex: 0 0 auto;
            width: 6px;  
            background: ${urlVer} center center no-repeat silver;
            background-size: 4px;
            border-right: 1px solid grey;
            cursor: col-resize;  `
        } else {
            otherCss = 'height: 100%;'
            handleCss = `flex: 0 0 auto;
            height: 6px;
            background: ${urlHor} center center no-repeat silver;
            background-size: 24px;
            border-bottom: 1px solid grey;
            cursor: row-resize;`
        }
       
        var css = `
        ${contSel} {display: flex; flex-direction: ${this.opt.flexDirection}; overflow: hidden; }
        ${contSel} ${this.elementSelector}       {flex: 0 0 auto;}
        ${contSel} ${this.opt.otherPanelSelector}{flex: 0 0 auto; ${otherCss}}
        ${contSel} ${this.opt.handleSelector}    {${handleCss}}
        `;

        var style = $dce('style')
        style.setAttribute(styleAttr, '')
        style.innerText = css
        $dqs('head').append(style);     
    }

}

export {AlfaResizablePanelsUI}


function getMousePos(e) {
    var pos = { x: 0, y: 0, width: 0, height: 0 };
    if (typeof e.clientX === "number") {
        pos.x = e.clientX;
        pos.y = e.clientY;
    } else if (e.originalEvent.touches) {
        pos.x = e.originalEvent.touches[0].clientX;
        pos.y = e.originalEvent.touches[0].clientY;
    } else
        return null;

    return pos;
}
function noop(e) {
    e.stopPropagation();
    e.preventDefault();
};

