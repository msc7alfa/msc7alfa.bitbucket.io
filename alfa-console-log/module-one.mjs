
/** alternative 1    */
import AlfaConsoleLog from '../alfa-console-log.mjs'; 
var alfaConsoleLog = new AlfaConsoleLog('module-one')

var log =     alfaConsoleLog.log.bind(alfaConsoleLog)
var logInfo = alfaConsoleLog.logInfo.bind(alfaConsoleLog)
var logWarn = alfaConsoleLog.logWarn.bind(alfaConsoleLog)
var logErr =  alfaConsoleLog.logErr.bind(alfaConsoleLog)

/** alternative 2   NOT WORKING!     */
// import AlfaConsoleLog from '../alfa-console-log.mjs'; 
// import {alfaConsoleLog_init, log, logInfo, logWarn, logErr} from '../alfa-console-log-make.mjs';
// var alfaConsoleLog = new AlfaConsoleLog('module-one.mjs')     
// alfaConsoleLog_init(alfaConsoleLog)


// not workin! /////////////
// import {alfaConsoleLog, log, logInfo, logWarn, logErr} from '../alfa-console-log-2.mjs';

function ModuleOne(){

    this.id = 'M1'
    alfaConsoleLog.setCfg({
        // moduleName: 'module-one.mjs',
        id: this.id,
        color: 'magenta'
    })
    log('constructor for ModuleOne')
    logInfo('ModuleOne info')

    setTimeout(function(){log.call(this, 'from moduleOne again')}, 1000)

}
// ModuleOne.prototype = function(){}

export default ModuleOne