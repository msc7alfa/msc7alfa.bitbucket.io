/** the Template Literal replaces placeholders with values from data source by binding paths specified in placeholders
 *      
 */
class AlfaTemplateLiterals {

    constructor(args){

        this.brackets = {left: '[[', right: ']]'}
        this.alfaDatabinding = args ? args.alfaDatabinding : null /** this argument works on a single page, setting it once per page
                                                                    in <alfa-autopage> with multiple pages NOT!
                                                                    action in alfa-mixin-databinding using make() method must pass current instance of alfaDatabinding */
    }

    

    parseBetween(string, brackets, err) {

        brackets = brackets || this.brackets.left + this.brackets.right

        try {
    
            // http://stackoverflow.com/questions/7201400/regex-to-grab-strings-between-square-brackets
    
            var str = string; // to preserve value of string parameter
            var matches = [];
    
            switch (brackets) {
    
                case '()':
                    str.replace(/\((.*?)\)/g, function (g0, g1) { matches.push(g1); });   break;
    
                case '[]':
                    str.replace(/\[(.*?)\]/g, function (g0, g1) { matches.push(g1); });   break;
                case '[[]]':
                    str.replace(/\[\[(.*?)\]\]/g, function (g0, g1) { matches.push(g1); });   break;
    
                case '{}':
                    str.replace(/\{(.*?)\}/g, function (g0, g1) { matches.push(g1); });   break;
    
                case '<>':
                    str.replace(/\<(.*?)\>/g, function (g0, g1) { matches.push(g1); });   break;
    
                default:
                    err = err || {};
                    err.message = 'brackets "' + brackets + '" not supported.';
                    console.warn('parseBetween()', err);
                    return null;
            }
    
            return matches;
    
        } catch (e) { console.warn('parseBetween()', e); return null; }

    }

    /** this is main function
     *  @param strTemplate - Number [[ds.one]] has [[ds.two]] legs and looks at [[ds.girl]]
     * @param opts = {
     *          alfaDatabinding: @required when called from <alfa-autopage> actions
     *          css: apply styling to all replaced items @result will be an HTML code
     *          bracket: surround all replaced items with brackets
     *          }
     */
    make(strTemplate, opts){
        opts = opts || {}

        if(opts.alfaDatabinding){this.alfaDatabinding = opts.alfaDatabinding} /** from <alfa-autopage> current instance is passed */

        var matches = this.parseBetween(strTemplate)

        matches.forEach(function(bound_path){

            var value = this.alfaDatabinding.get(bound_path, console.error)
            var replace = this.brackets.left+bound_path+this.brackets.right
            
            if(opts.brackets){
                var L = opts.brackets.charAt(0)
                var R = opts.brackets.charAt(1)
                value = `${L}${value}${R}`
            }
            if(opts.css){
                value = `<span style="${opts.css}">${value}</span>`
            }

            strTemplate = strTemplate.replace(replace, value)

        }.bind(this))
        return strTemplate
    }
}


export  {AlfaTemplateLiterals};
