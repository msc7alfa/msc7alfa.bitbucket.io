const $dce = document.createElement.bind(document)
const $dqs = document.querySelector.bind(document)

class AlfaAuthUserUI {

    constructor(settings){
        this.max_count = 5
        settings = settings || {}
        Object.assign(this, settings)

        this.uniqueTagAttr = 'alfa-authenticate-ui'
        this.setStyle()
    }

    /**
     * @param cb - callback on successful authentication @optinal
     * @param opts - {email_as_id: true}
     */
    open(cb, opts){

        this.callback = cb

        opts = opts || {}
        Object.assign(this, opts)
        this.count = 0

        this.buildPopup()
        this.overlay.style.display = 'block';
        this.popContainer.style.display = 'flex';

        this.userIdInput.focus()

        
    }
    close(){
        $dqs('body').removeChild(this.overlay);
        $dqs('body').removeChild(this.popContainer);
    }

    buildPopup(){

        this.overlay = $dce('div');
        // this.overlay.setAttribute(`${this.uniqueTagAttr}-overlay`,'');
        this.overlay.setAttribute('style', 'display: none; position: fixed; left:0; top:0; bottom:0; right:0; background: black; opacity:.25;');
        $dqs('body').appendChild(this.overlay);
    
        this.popContainer =  $dce('div');   // popContainer is full screen as overlay
        this.popContainer.setAttribute(this.uniqueTagAttr,''); // make target for CSS to select by attribute
        this.popContainer.setAttribute('style', `
            display: none; 
            position: fixed; left:0; top:0; bottom:0; right:0; 
            justify-content: center; 
            align-items: center;`);
        this.popContainer.setAttribute('tabindex', '0');
    
        this.popBox = $dce('div'); // centered popup box
        this.popBox.setAttribute('style', 'display:flex; flex-direction:column;  max-width:90%; max-height:85%; min-width:180px; color:black; background:white; box-shadow:2px 2px 4px #000000;');
    
        // this.titlebar = $dce('div');
        // this.titlebar.innerHTML = 'Authenticate'

        this.img = $dce('img')
        this.img.src = this.icon()
        this.img.style = 'width: 98px; align-self: center; padding-top: 1.5em;'
    
        this.popBody = $dce('div');
        this.popBody.setAttribute('style', 'padding: 20px; overflow-y: auto'); //  max-height: 85%;
    
        this.popBox.appendChild(this.img);
        this.popBox.appendChild(this.popBody);
       
    
        this.popContainer.appendChild(this.popBox);
        $dqs('body').appendChild(this.popContainer);
     
        this.buildForm()
    }

 
    buildForm(){

        var last_user_id = localStorage.getItem('alfa-auth-last_user_id')
        var last_user_email = localStorage.getItem('alfa-auth-last_user_email')

        this.form = $dce('form')
        this.form.style = 'display: flex; align-items: center; flex-direction: column; min-width: 20em;'
        
        this.label1 = $dce('label')
        this.form.append(this.label1)

        this.userIdInput = $dce('input')
        this.userIdInput.style.marginBottom = '.25em'
        // this.userIdInput.setAttribute('autofocus', '') - too early
        if(this.email_as_id){ 
            this.label1.innerText = 'user email'
            this.userIdInput.setAttribute('placeholder', 'user email')
            this.userIdInput.type = 'email'
            if(last_user_email){this.userIdInput.value = last_user_email}
        } else {
            this.label1.innerText = 'user ID'
            this.userIdInput.setAttribute('placeholder', 'user ID')
            if(last_user_id){this.userIdInput.value = last_user_id}
        }
        this.userIdInput.setAttribute('required', '')
        this.form.appendChild(this.userIdInput)

        // this.label2 = $dce('label')
        // this.form.append(this.label2)
        // this.label2.innerText = 'password'

        this.userIdPass = $dce('input')
        this.userIdPass.setAttribute('placeholder', 'password')
        this.userIdPass.type = 'password'
        this.userIdPass.setAttribute('required', '')
        this.form.appendChild(this.userIdPass)


        this.msg = $dce('div')
        this.msg.innerHTML = '&nbsp;'
        this.msg.style.marginBottom = '.2em'
        this.form.appendChild(this.msg)

        this.submitBtn = $dce('input')
        this.submitBtn.style = 'background: #3F51B5; border: 4px solid #3F51B5; color: white;'
        this.submitBtn.type = 'submit'
        this.submitBtn.value = "Authenticate"
        // this.form.appendChild($dce('br'))
        this.form.appendChild(this.submitBtn)

       

        this.form.addEventListener('submit', this.submitForm.bind(this))

        this.popBody.appendChild(this.form)
    }

    submitForm(ev){
        ev.preventDefault()
        
        this.count ++
        if(this.count > this.max_count){
            this.msg.style.color = 'red'
            this.msg.innerHTML = 'Maximum number of attempts exceeded!'
            return
        } else {
            this.msg.style.color = 'blue'
            this.msg.innerHTML = 'attempt '+ this.count 
        }

        var userCredentials = {
            user_id:    this.email_as_id ? null : this.userIdInput.value,
            user_email: this.email_as_id ? this.userIdInput.value : null,
            user_pass:  this.userIdPass.value,
        }

        this.authUser(this.server_side_api, userCredentials, function(err, response){
            // if(err) console.error('AlfaAuthUserUI', err)
            // console.log('AlfaAuthUserUI', response)
            if(response.responseMeta && response.responseMeta.status===200){
                if(response.responseData.error){
                    this.msg.style.color = 'red'
                    this.msg.innerHTML = response.responseData.error.message    // 'Authentication failed.'  
                } else {
                    this.msg.style.color = 'green'
                    this.msg.innerHTML = 'Authenticated OK'
                    this.close()
                    if(typeof this.callback==='function'){this.callback(response.responseData.userInfo)}
                }
            } else {console.error('AlfaAuthUserUI', err, response)}
        }.bind(this))

    }

    /** generic alfa-core
     * each Applet has it's own user handling, thats why apiUrl is needed
     * 
     * @param {String} apiUrl - route to server side auth handler app-authenticate.js
     * @param {Object} userCredentials - {user_id, user_email, user_pass}
     * @param {Function} cb - callback
     */
    authUser(apiUrl, userCredentials, cb){ /* similar code in ALFA_LIB/alfa-auth.js */

        if(this.short_hash){userCredentials.user_pass = this.shortHash(userCredentials.user_pass, {string: true}) } 
        else {              userCredentials.user_pass = hashMD5.b64(userCredentials.user_pass)}

        alfaFetch(apiUrl,  {
            method: 'POST',
            body: {userCredentials, short_hash: this.short_hash},
            onSuccess: function(response){
                if (response.responseMeta && response.responseMeta.status===200){ 
                    sessionStorage.setItem('x-access-token', response.responseData.accessToken);
                    var userInfo = response.responseData.userInfo /* userInfo came from server side */
                    if(!this.no_last_user){
                        localStorage.setItem('alfa-auth-last_user_id', userInfo.user_id); // localStorage for next day login
                        localStorage.setItem('alfa-auth-last_user_email', userInfo.user_email);
                    }
                    sessionStorage.setItem('alfa-auth-user_info', JSON.stringify(userInfo));
                    cb(null, response);
                }
                else {
                    cb({status: response.responseMeta.status, statusText: response.responseMeta.statusText}, response);
                }
            }.bind(this),
            onError:  function(err, response){
                cb(err, response);
            }
        });
    }

    verifyJWT(callback){ /* similar code in ALFA_LIB/alfa-auth.js */
        var apiUrl = this.server_side_api +'/verify'

        alfaFetch(apiUrl,  {
            method: 'GET',
            contentType: 'text/plain',      // response is either 200 or 403
            onSuccess: function(response){
                if (response.responseMeta.status===200){ if(callback) callback(null, response); }
                else {
                    if(callback) callback(true, response);
                }
            },
            onError:  function(err, response){
                if(callback) {callback(err, response);}
                else {window.location.replace('/auth-failed.html');} // location.replace() blocks browser back button
            }
        });
    }

    clear(){
        sessionStorage.removeItem('x-access-token')
        localStorage.removeItem('alfa-auth-last_user_id') // localStorage for next day login
        localStorage.removeItem('alfa-auth-last_user_email')
        sessionStorage.removeItem('alfa-auth-user_info')
    }
    clearUser(){
        sessionStorage.removeItem('x-access-token')
        sessionStorage.removeItem('alfa-auth-user_info')
    }


    shortHash(str, opts) { // https://github.com/darkskyapp/string-hash
        if(typeof str!=='string' | typeof str==='undefined') return null
        var hash = 5381,
            i    = str.length;
        while(i) {
            hash = (hash * 33) ^ str.charCodeAt(--i);
        }
        hash = hash >>> 0;
        if(opts && opts.string) return hash.toString(16)
        else return hash
    }

    icon(){
        return ' data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGIAAABUCAYAAAB5huK+AAAAAXNSR0IB2cksfwAAAAlwSFlzAAAATwAAAE8BY4r91wAADUtJREFUeNrtnQtQm1UWgKPUAkkIr/JqeLVQaosCQiEJCQESQniW96OFFuQl0OJUXd1Vq67drrNud111VwkEwiM/pDzCmwKB8Ki1RcG6bdVZd2frbncddeq23anOap323//+ARrSJPz5SUIK/5k5U6aQ3OR+Oeeec+65NySShmxL6zmxVQj9xU3QetshRgoTahx15klhd0Hrde8UmYJRMpZC0iUx1cok+6hGmBJZB1PZ9ajacQg1llI59UvzSmbVwS5x0MeFr85vWgaBVaZIt2aIFiZfvKSLDyR09ao+r0ApkfWwLQIk4/nZzSiE7Bdm7WhRDcgfLwdgyxTB9txGeIsAmnSNbx9DVEGo4erMbxulJ7ZdsGWpLEEThqtA+jkKYnt6VztwR0u/RCA4xkjgyPJxJokQo4pXctfrZNZy6yAjc787fziF5CGUfrVoOuBfB64Ezjn6oQMxbaYR37S+CnUYYM69kmWDJPvopqX/tEHcEbdKGU5Ml2nFM7njT3e9UD3sxJN+QlInQ0OiJmKaTC+Fxy7QNke8e9dFRUkuLgPhkwx9REyTeYSeAN2NptRBLJjJmDleBKdyemtg/sDP/NK65PSkzk9IobVfk0JEX3qndnwQkCV/K3DfQMp6B7EtFUIDIx0gxApTDi6smRTQoiT/s2WJ0GiBohFjg59BSAfCPJDXeAjbZ3mHlOsyevNNWSMQXkmy0yCW1pbc6FIUDKL0xPYLBAij+MP27zSTGUMURBggkAguPOVHgMApzrzWz1YDQd06HKLXT1RnVhC78wafALnJaiGoW4ZrfNslAoSBgmTpt6lGgnC3JFAPcyoUTAIERgnMHzpiDJekzUUh0dR7BAisC3Ri27whERJmEKiLEutcKxiPj8aSIxu/pbBE1xG9Zg4lM0X/teO2fJr70pyzxYHYHCEyOoQl94TkIUH7h0M1x6x6468P2bBEsCk+AFjWL3cB9IVFgQgvnfS3ZYpM+qb9M/uPaI7rkab0tGXWmh3CYvEOmdDrFgVid25/jvoeh9FBIG/AP71TqW1sGrf5CojUwPjmVLDbFlIw9LpFgdiZ3fsrUKowFQjgejYzxFd1jR9ZPp7ulz1SvCNnpMhMWhJTpdxjcYv1zqyublOCWNDviahpBbHnyXpV3QumA0HjNNwKLZvbRIDQB4Ij7jbDAvkjYRF6pOj4x1YOMZJb6l0hpomc6uGYqolMAoQOeTS/911TZNTaFmw3AXSDAKFDHHmQ0mwJlY499uD9g3nBBcOHQgqGq82hwYXDNcySEa5FgXAXygbNAUJlEdKf7snoGeI7izuA5lRrRi3sl94ptRgQjJLRgzYmzKo1QHymPrZr6qQnec0ya0S5TZaVWbsKpFdMuU6AQAA09YaXjMVojg2atyhmbqBebCymJ7aft7iin1eS7G8mS+qixHBI4UiitnG5lcooZBGfd+JJLznxoYsmVx500Zkv/QR5v6MHj807WhwIbsW4LWhiNkXBb2d27xvEfoQhO3Sxzf829sINni+8eNyfAGGAxFROCEA0YcwkziupndizxhXOxkuvgHDSGJYAorGEp844ESBwik9KRxM5sm5VluDMa/4m44V5CmmdyJp1+vnu7TiHZ7NI1c8kWXdd6msGQvDkJHpEDE+UFJg/cBTrOEH7B1525rfA+oqO4HX4pMhmNiSIoKJzVnachlt4GgUeK56MwjJG1tELFBsks8YSqYE99cD8/hc2HIjKE+et7LkNPxracGbDEMG5L55jYRljT/GEK9amBZAJB+b1/mHtQEjXBkTIgfEH7bkSHK0zdUjeMFqNdRwXfvOllWAAdwdeC7dqirxWILbtlV1eep3mBBFWNHQCT/2Jip5ybfrBkLGY5dMuSMi8FXm8h6Yiz+fhLOjwxPI8vMMzm5hlyrCd2QNP+6R2ttET2hXUqCZUPRLaFN6pndDuvMGnuZXKsMiKKSvDYXRAoPvDbCAiSieibFdRBAQAAzK7G831aQ05cErgFgddtEcWdfJCuwxVy6Ea1cEaVRmcxmm44yGELkY8PiowZKyATPmUDVP8DzO05UNDxiiLg6TQTQB9ySyfoJsMQMHQr8nojQAi1WkmHP1WwN1Q2Q2wX0bvL7COu2vf6LMmAcF/8qzrwzl9x0GUZMxGs8XJcYmTXn4kfyC17MTnRuniQD7FMaBACSyPaqQyPbASkP+wykYxRXxGA8EuV6RuT+vssWHWI5FOrcqcTbgppNoVE8FeSdD51Vls65+B2zRJwzRbdd+Ga5x03uQgdmTK6+yjG9FzC6AUYeruDS1v9gYeAMWvXXzIhd/ytTkaHVS30bR+Wfm7T62MDgJxPc9SFhYrc0++hl4zFMK+V+Y2Az9uhm7EZSGzHacRrjjxqbXRQIQVjWSR0fb3NQWAG8QWfutNc0JQL146xTZ/azQQDsjCthZnEIwBwjG25StTdqpjsQzETf1r1SAYJWPbLcQSDAYRtH+4ytYMHSZY6lxhB4dLVwXi0f2n8slr+InCC4JfM+1uQVaMuvXUZ97bghtEQHb/S5T7EIRX8smxtVgX9J56yugawQ0iKK+72ZLeEBYQSc+c3WJjAS7pnsoysw4+8OpHzrhAeKfKIUsycSwgArJ63rYwK16yCr8M+W9wgdidK2+93yzCLqrxzppNNlt1jwiN23gbTXg11glQWsEFYkdm3y/vpzWCU6mMWKvgwhZxPaGFQ8f5NaetWRUzD0SUjKbStGTdvMNToQaDiK6e5JqjLGAsEP4Z3bVrYcEAfkhBf7nm6wk9OFKsPn9o52JWz1u4Ejo7TgO8WCizgPVCLwi3eOiMWV8jW3VJZWTZWLlW157TS6WptaKqrrRomyFpFNBGsSZHoQcGC33TuuvsOI1XaVENt8AVPsAUVeeOV74JGE8lU10pkao6F/Kmf9ILQtB61ZwgQJd6bPVUua7XE3d4iqG5wQQKgssuV/ROkn6Ap5IZf+Ss1c7sPufoQzPhu/IGyv0z5b/3TJL1uwmkZzyE0GWXOOk1UlgdvGnPH9EDHZRI0cIGSh1MXriaedHC0MPjCFAq8jfWEbXoY8Bjkee44R7fenlrQtusu7Ctf0dmz1u78waq0p47w9C7hx0nNY/VslVuxjNFnqzv9TjFtnyjCcIjXnp7GQgbVuNtU29DFh07t8Uz87R74hFlqGP8cHBYQReHX30qIb56ON4hVvrYtuyJkJQj48EeadPupa/Nua56wEffXrEQBwqY5FXsSajOa4jhwLw+vRBoXMllbesVldsEk+zUOiuAb9v73Ace66Wb7ur38AP6QIDJ35Ut70Yimqg9xWP7nGIl/zE0EFFt/ohgZumIXghbE9tu6oo2qRwJDO4fvaF+JTXir/6+nlobHWNbdUIILhi8p6/JO+XkF1jDc9W8NcDcqkmdEHJfnN2EzOlNXYDBc7gKpDDJPaF9iKpmLmjre/LJD9cLCPd46DttLsee2wBHlk84aHuMfXTT7EowVN8oUAfza6bjV+izuqkvjwHPg6x910gZPz+zi8K+9wZ3RyQK8svofueRwkkePXOG7Zk1w7FE3ZoxHR3+xHmdnR30ROiSLhCssnEHndFWXItOGKrruxvgoILhQJ1RZZHCyim2acUNKBREAjSn+tQIZY2adXoqW7wQIorQCMZSlcyqhQMyO2U6a2MpHTLtX6SBrA+5fe/otybprObZDlXjQj3MrVQE6nWJMc0/YEkkgRH4pHa13h1UKBsxVTeDqY/5IiGyzi4JVpkiQ9dBGRtGHRzx+MhxfRNKTzw5u+jfFy+yT3r6PZ0Q8l+ed0RCVBjrOgNgscvHEzV33wooIDNcbIdh3weKttXU6Tw/wSydoAI3RNVTig7a16cXxra0zlnQZW6HuKOco3OB+vOEZtiwWlwDHFKktNH6ZNzq09t90weSXBIGhK6Jlq80nnxv1Zv/tNa5xx4rndFn6SBsD8iS64WxmSXpDipU+Or6fVTFuCOoLhhS1wJ/6yaUTZM2igTknArD0iHuxD9Zi3cMe/R7mgxz68Dz8A9PPULaSEJhS26sNFHA/fik9ckNed78V+b9nHnNMMVACFTV5fOfkzaaBBWM8LG4DWA5nsmdmGAcPH6eDM5Z4Cmzg6sqGKWKGNJGFHryyX4sJQywZrgJIL0weNUTfiCCouCIMkGeRk+SdZA2soC2Ryy+HF1IBS1aYbArlGQaV4wr1FeV8cXEdzllPX/Wl8rBtrAC6/FKap/QgMClsPBDAF9Dmvrs+3QSISSS8MjpSArGb3cBuQqyDlz3z+z7rUMsNIbnAMtSMoj8+9iBU4EEAfWmggplKNaQc3FHEP9+RT3arRFRMkZA0G4ZZzwcFk4KmfJsBIiseDVq7ZWEaBePBJmEwqoz6nmJxW1fz5QOCTHDBgirTBHul9Z5yXqVx87QhgaGCEnWZB8yShShxMziFH7Nae9dub1NYL+BzNLsSBFr/Rk94ouelxbfeTi7pyHhqfd9iJk0kux/ee7B2ENTEX7p8jfpiW3T7vHQFVd+y3eucVIYVeRndyF0BdHJHZk9bwpqphirGe//CddXka9bDosAAAAASUVORK5CYII='
    }

    setStyle(){
        var css = `
        [${this.uniqueTagAttr}] label,  [${this.uniqueTagAttr}] input:not([type="submit"]) {width: 180px}
        [${this.uniqueTagAttr}] label{font-size: 10px; font-family: sans-serif; color: #3f51b5}
        [${this.uniqueTagAttr}] input:invalid:not(:placeholder-shown){border-color: red;}
        [${this.uniqueTagAttr}] input[type="submit"]:hover{color: yellow !important;}
        `;
        var style = document.createElement('style');

        if (style.styleSheet) {
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }

        $dqs('head').append(style);
    }
}



import alfaFetch from './alfa-fetch.mjs'


 


export {AlfaAuthUserUI}
