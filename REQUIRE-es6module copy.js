// OLD !!!

const fs = require('fs');
const path = require('path');

const MODULE_NAME = 'REQUIRE-es6module';

/** allows to use NodeJS require() for es6 modules
 *  NOTE:
 *  es6 modules should be in the same folder as this module (require-es6module)
 * 
 *  use:
    const requireEs6 = require('../../REQUIRE-es6module');
    const myModule = requireEs6('./my-module.mjs')
 * 
 *  es6module synax restrictions:
        
        keyword 'export' should be first on the line of code, MUST be at least 4 chars from last NewLine char (to allow 1 tab)
        
        keyword 'export' may be used accidentally as a word in file:
         - placing string 'export' further than 4 chars on line is allowed.
         - placing keyword as a string, close to the line beginning, in quotes "export" or 'export' is allowed, quote `export` is NOT allowed!
 * 
 * 
 */
module.exports = function (filepath, errCb){

    filepath = path.join(__dirname, filepath)

    /** WARNING
     *  __dirname when symlink?
     */
    // console.log(MODULE_NAME, 'REQUIRE-es6module __dirname:', __dirname)

    var fileStr;
    try{
        fileStr = fs.readFileSync(filepath)
    } catch(err){
        errHandler(err);
        return;
    }
    
    fileStr = fileStr.toString();
    var newFileStr;


    /** 1. find keyword 'export default' and it's position */

    newFileStr = replaceKeyword('export default');

    /** 1a. if previous not found, find keyword 'export' and it's position */
    if(!newFileStr) {
        newFileStr = replaceKeyword('export');
    }

    if(!newFileStr){errHandler({message: `Module code not converted. 'export' keyword not found!`, filepath});}

    /** 2. replace 'import' */
    newFileStr = replaceInString(newFileStr)
    

    console.log(`[${MODULE_NAME}] ${filepath} ------------------------------------------------
    `, newFileStr)

    fileStr = undefined;
    return  eval(newFileStr); // ---- >

    /** returns newFileStr with replaced keyword or null */
    function replaceKeyword(keyword2replace){

        var P = checkKeywordPosition(fileStr, keyword2replace, 0);
        if (P===-1) return null;

        return replaceBetween.call(fileStr, {
            start: P, 
            end:   P + keyword2replace.length+1, 
            what: 'module.exports = '});
    }

    function errHandler(err){
        if(typeof errCb==='function') errCb(err);
        else console.error(`[${MODULE_NAME}] ERR: ${filepath}`, err)
    }

}

/** check if 'export' keyword is first on line of code and not a part of comment or string */
function checkKeywordPosition(wholeStr, findStr, start){
    
    var P = wholeStr.indexOf(findStr, start)
    if(P < 0) return -1;

    // var reject = false;
    var i = P;
    do{
        i--;
        var previousChar = wholeStr.charAt(i)

        // console.log(MODULE_NAME, {P, i, findStr, previousChar},  `
        // `)

        if(previousChar==='\n') return P;    /* keyword at the beginning of line, OK */
        if(previousChar==='/') {break;};    /* comment, reject */
        if(previousChar==='*') {break;};    /* comment, reject */
        if(previousChar==='"') {break;};    /* string, reject */
        if(previousChar==='`') {break;};    /* string, reject !!! not workin' */
        if(previousChar==="'") {break;};    /* string, reject */

    } while(i+5 > P) /** allow 4chars (like tab) before NewLine */

    return checkKeywordPosition(wholeStr, findStr, P+1)

}

/** replace text between two indices
 *  use:
    var s = replaceBetween.call(myStr, {
            start: P, 
            end:   P + str2replace.length + 1, 
            what: 'module.exports = '});
 */
function replaceBetween(args) {
    return this.substring(0, args.start) + args.what + this.substring(args.end);
};


/** ************************************ */
// https://github.com/jameswomack/replace-require-with-import/blob/master/index.js

const r1     = /^(let|var|const) +([a-zA-Z_$][a-zA-Z0-9_$]*) +\= +(require)\((('|")[a-zA-Z0-9-_.\/]+('|"))\)/gm // const createStore = require('redux')
const r2     = /^(let|var|const) +([a-zA-Z_$][a-zA-Z0-9_$]*) +\= +(require)\((('|")[a-zA-Z0-9-_.\/]+('|"))\)\.([a-zA-Z][a-zA-Z0-9]+)/gm // const createStore = require('redux').createStore
const r3     = /^(let|var|const) +(\{\s*([a-zA-Z_$][a-zA-Z0-9_$]*)\s*\}) +\= +(require)\((('|")[a-zA-Z0-9-_.\/]+('|"))\)/gm // const { createStore } = require('redux')


function replaceInFile(fp) {
    const result = FS.writeFileSync(fp, FS.readFileSync(fp, 'utf-8')
      .replace(r3, `import { $3 } from $5`)
      .replace(r2, `import { $7 as $2 } from $4`)
      .replace(r1, `import $2 from $4`), 'utf-8')
    console.log(`> ${fp}`)
    return result
}

function replaceInString(str) {
    const result = str
        .replace(r3, `import { $3 } from $5`)
        .replace(r2, `import { $7 as $2 } from $4`)
        .replace(r1, `import $2 from $4`)

    return result
}