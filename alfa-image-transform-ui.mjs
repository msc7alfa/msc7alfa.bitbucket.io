 // alfa-image-transform-ui 19.3.6NEW   https://stackoverflow.com/questions/46647138/zoom-in-on-a-mousewheel-point-using-scale-and-translate

//  import showInfo from './alfa-image-transform-ui/show-info.js'; // forDEBUG
import AlfaConsoleLog from './alfa-console-log.mjs'; 
var alfaConsoleLog = new AlfaConsoleLog('alfa-image-transform-ui')
// var log =     alfaConsoleLog.log.bind(alfaConsoleLog)
var log = function(){}
var logInfo = alfaConsoleLog.logInfo.bind(alfaConsoleLog)
var logWarn = alfaConsoleLog.logWarn.bind(alfaConsoleLog)
var logErr =  alfaConsoleLog.logErr.bind(alfaConsoleLog)


 const $dce = document.createElement.bind(document)
 const $dqs = document.querySelector.bind(document)
 
/** C O N S T R U C T O R
 * 
 * @param {Object} $viewport - !!jQuery element 
 * @param {} callbacks
 * @param {Object} opts - see defaults
 */
function ImageTransform($viewport, callbacks, opts){

    callbacks = callbacks || {}
    callbacks.onload = callbacks.onload || function(){}
    callbacks.showInfo = callbacks.showInfo || function(){}
    callbacks.zoomSetting = callbacks.zoomSetting || function(){}
    callbacks.imageSetting = callbacks.imageSetting || function(){}
    callbacks.playList = callbacks.playList || function(){}
    this.callbacks = callbacks;

    opts = opts || {};

    var defaults = {
        maxScale: 16,
        scaleStepWheel: 0.25,    // scale increment added for each mouse wheel click
        scaleStepClick: 2,      // scale multiplier for each mouse btn click (-1 - use this.scaleInContainer to match original img size)
        transition: '2s',       // animation time for mouse btn click
        noTransitions: false,   // prevent transitoins defined in playlist
        arrowStep: 20,           // keyboard arrows movement steps in px
        showCaptions: true,
        clickMode: 'zoom',       // zoomToPoint
        noFocusCursor: 'pointer',  /** cursor before focus */

        playTransition: {       /** for autoplay of slideshow */
            zoomToPoint: {x:.5, y:.5}, 
            scale: 1.25, 
            transition:'5s'
        },

        /** this should come from loadingSettings */
        // navTransition: {        /** for next, prev */
        //     zoomToPoint: {x:.5, y:.5}, 
        //     scale: 1.25, 
        //     transition:'2s'
        // }
    };
    // this.cfg = Object.assign({}, opts, defaults)
    this.cfg = Object.assign(defaults, opts )

    // this.setClickMode(this.cfg.clickMode)

    this.$viewport = $viewport;   

    build_html.call(this, opts);

    this.pos = {x: 0, y: 0};
    this.scale = 1;
    this.imageRotation = 0

    /** image events     */
    this.$image.addEventListener('load', this.onLoad.bind(this))

    this.$image.onerror = function(e){
        var msg = e.message || ''
        this.ShowErrorMessage(`<dd style="color:red">ERROR loading image: '${this.imageSrc}' ${msg}</dd`)
        logErr(this.imageSrc, msg, e);
    }.bind(this);

    /** mouse events     */
    // this.$container.on('wheel DOMMouseScroll', function(e){
    this.$container.addEventListener('wheel', function(ev){
        ev.preventDefault(); // prevent page scrolling
        // self.zoom(ev, 'wheel');
        this.zoom(ev, 'wheel');
    }.bind(this));


    document.body.style.cursor = this.cfg.noFocusCursor 
    /** events */

    // first click sets focus, not zoom
    window.onblur = function(){
        this.clickMode = null
        document.body.style.cursor = this.cfg.noFocusCursor  
        // logWarn('onblur', this.clickMode)
    }.bind(this)
   

    this.$container.addEventListener('click', function(ev){
        
        /** allow first click to set focus to the window */
        if(!this.clickMode){this.setClickMode(this.cfg.clickMode); return} // --- >
        if(this.clickMode==='zoomToPoint'){this.setZoomToPoint(ev); return} // --- >
        if(this.clickMode==='mouseNavigation'){this.callbacks.playList('prev', ev); return} // --- >
        
        if (ev.ctrlKey) {this.clearTrans(); return;} // --- >

        var originalScale = false;
        var zoom = 1; // zoom in
        if(ev.altKey) zoom = -1; // zoom out
        if(ev.shiftKey) originalScale = true;

        this.zoom(ev, 'click', {zoom, originalScale});

    }.bind(this));

    this.$container.addEventListener('contextmenu', function(ev){
        if(this.clickMode==='mouseNavigation'){
            ev.preventDefault()
            this.callbacks.playList('next', ev); 
            return} // --- >
    }.bind(this));

    /** K E Y B O A R D     E V E N T S */
    $dqs('body').addEventListener('keydown', function(ev) {
        // console.log('ev.key=', ev.key);
        
        switch(ev.key){
            case 'ArrowRight':
            case 'ArrowLeft':
            case 'ArrowUp':
            case 'ArrowDown':
                this.arrow(ev.key); break;
            case 'R': case 'r':
                this.rotate(90); break;
            case 'C': case 'c':
                if(this.clickMode==='zoomToPoint'){ this.setClickMode('zoom'); }
                else                              { this.setClickMode('zoomToPoint');}
                break;
            case 'X': case 'x':
                this.clearTrans(); break;
            case 'M': case 'm':
                if(this.clickMode==='mouseNavigation'){ this.setClickMode('zoom'); }
                else                                  { this.setClickMode('mouseNavigation');}
                break;
            case 'Delete':
                this.clear(); break;
        }
    }.bind(this))

}//end: ImageTransform() Constructor

ImageTransform.prototype.setClickMode = function(clickMode){
    // logWarn('setClickMode()', clickMode)
    this.clickMode = clickMode
    if(clickMode==='zoomToPoint'){document.body.style.cursor = 'crosshair' } // cell
    if(clickMode==='zoom'){document.body.style.cursor = 'default' }
    if(clickMode==='mouseNavigation'){document.body.style.cursor = 'ew-resize' }
}

ImageTransform.prototype.rotate = function(deg){

    this.imageRotation += deg

    this.$image.style.transformOrigin = '50% 50%'
    this.$image.style.transform = `rotate(${this.imageRotation}deg)`

    var playlist_index = this.loadingSettings.playlist_index
    this.callbacks.imageSetting({playlist_index, imageRotation: this.imageRotation})
    
    // var w = this.$image.naturalWidth
    // var h = this.$image.naturalHeight

    // this.$image.naturalWidth = h
    // this.$image.naturalHeight = w

    // this.updateImageData()
}
ImageTransform.prototype.clearPreviousTransforms = function(){
    if(this.imageRotation!==0){
        this.imageRotation = 0
        // this.$image.style.visibility = 'hidden'
        this.$image.style.transformOrigin = 'unset'
        this.$image.style.transform = 'rotate(0deg)'
        /** make visible in onload event of other image */
        // this.$image.style.visibility = 'unset'
    }

    this.$image.style.visibility = 'hidden'
}
 
/**
 * 
 * @param {*} image OPTIONAL, needed when called from image-navigator
 */
ImageTransform.prototype.updateImageData = function(image){
     
    var img = image ? image : this.$image;

    // this.size = {w: $(img).width(), h: $(img).height()}; // NOTE: image is stretched to container, this is a size of container
    this.size = {w: img.clientWidth, h: img.clientHeight}; // NOTE: image is stretched to container, this is a size of container

    // 1. find how image fits to viewport - margins, y=vertical or x=horizontal
    this.renderedImgInfo = this.getRenderedImgInfo(img); // pass optional image, it can be from image-navigator, not from here
    this.marg = {
        y: this.renderedImgInfo.top, // y=height of margin, margins are Horizontal above and below image, 
        x: this.renderedImgInfo.left // x=width of margin,  margins are Vertical on left and right of image, 
    };

    // 2. what is the scale of image in container
    if (this.marg.y){ this.scaleInContainer = img.naturalWidth / this.size.w; }
    if (this.marg.x){ this.scaleInContainer = img.naturalHeight / this.size.h; }
    if (this.marg.x===0 & this.marg.y===0) { 
        this.scaleInContainer = img.naturalWidth / this.size.w; 
    }
    /** 3. make sure that image is visible after cleanup of previous */
    this.$image.style.visibility = 'unset'

    log(`updateImageData: ${img.getAttribute("src")}\n`, 'renderedImgInfo:', this.renderedImgInfo, 'scaleInContainer:', this.scaleInContainer); // log( 'marg y:', !!this.marg.y, ', x:',!!this.marg.x);

};

function build_html(opts){

    /** 1. <img img-scalable> */
    this.$image = new Image()
    // this.$image = $dce('img');
    this.$image.style = 'width: 100%; height: 100%; object-fit: contain';
    // used only when needed in load(): this.$image.onload = this.onImageLoad.bind(this)

    /** 2. <div scalable-img-container> */

    this.$container = $dce('div')
    this.$container.classList.add('zoom-container')
    this.$container.style = 'height: 100%; transform-origin: 0 0'

    // if (!opts.fromNavigator) {
    //     // this.$container.css({'height':'100%'}); !! height is NOT SET ????
    //     // this.$container.height('100%');
    //     log('fromNavigator');
    // } 
    this.$container.append(this.$image);

    // 3. 
    this.$viewport.append(this.$container);

     /** 4. caption - can be used for error messages */
     this.$caption = $dce('div')
    //  this.$container.classList.add('image-caption')
    this.$caption.classList.add('image-caption')
     this.$caption.style = 'position: fixed; bottom: 5px; width: 100%; text-align: center;'
     this.$viewport.append(this.$caption);
}

/** LOAD
 * load image to <img> $image
 * @param {*} imageSrc 
 * @param {*} {
 *              imageRotation, 
 *              zoomToPoint
 *              defaultTransition
 */
ImageTransform.prototype.load = function(imageSrc, loadingSettings){
    
    loadingSettings = loadingSettings || {}
    this.loadingSettings = loadingSettings
    // if(loadingSettings.noTransitions) this.cfg.noTransitions = true

    this.cfg = Object.assign(this.cfg, loadingSettings)

    // this.cfg.noTransitions = loadingSettings.noTransitions



    this.clearTrans(loadingSettings);

    if (typeof imageSrc ==='string') {
        this.imageSrc = imageSrc    /** for error msg when load fails */
        this.clearPreviousTransforms()
        if(!this.isSupported(imageSrc)){ return; }
        
        this.isImageLoaded = false
        this.$image.src = imageSrc; /* NOTE: img.onload function MUST preceed the actual load on this line. 
                                        onload event runs updateImageData() */
        if(this.cfg.showCaptions){
            var caption = loadingSettings.caption || imageSrc
            this.$caption.innerHTML = caption
        } else {this.$caption.innerHTML = ''}
    }
    else {
        logInfo('load() imageSrc:', imageSrc)
        // console.error('TODO');
    }

    /** if transforms are part image load(loadingSettings), they will be done in image.onload event */
   
};

ImageTransform.prototype.onLoad = function(ev){

    this.$image.style.visibility = 'visible'

    this.isImageLoaded = true
    this.updateImageData.call(this)
    this.callbacks.onload(this)

    if(this.loadingSettings.defaultTransition){
        if(this.loadingSettings.imageRotation){this.rotate(this.loadingSettings.imageRotation)}
        // this.zoomToPoint({x:.5, y:.5}, {scale: 1.25, transition:'15s'})
        this.zoomToPoint(playTransition.zoomToPoint, playTransition.scale, playTransition.transition )
    }
    else if(this.loadingSettings.imageRotation || this.loadingSettings.zoomToPoint){  /** list of transformations */
        if(this.loadingSettings.imageRotation){this.rotate(this.loadingSettings.imageRotation)}
        if(this.loadingSettings.zoomToPoint)  {this.zoomToPoint(this.loadingSettings.zoomToPoint, this.loadingSettings)}
    }
}

/** check image file extension */
ImageTransform.prototype.isSupported = function(imageSrc){
    if(imageSrc.toUpperCase().endsWith('.TIF')){
        /** transparent image:  from https://png-pixel.com/ */
        this.$image.src ='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII='
        this.ShowErrorMessage(`<dd style="color:red">Image type not supported: '${imageSrc}' </dd>` )
        return false
    } else return true
}
ImageTransform.prototype.clear = function(){
    // this.$image.src = ''
}

/** remember mouse click coords */
ImageTransform.prototype.setZoomToPoint = function(ev){

    var img = this.$image

    var ratio = img.clientWidth / img.clientHeight

    /** height is constant for all thumbnails, it will be used to calculate coords */
    var zoomToPoint = {
        y: ev.offsetY / img.clientHeight,
        x: ev.offsetX / img.clientWidth
    }
    var playlist_index = this.loadingSettings.playlist_index
    this.callbacks.imageSetting({playlist_index, zoomToPoint})
    // logInfo('setZoomToPoint', zoomToPoint)
}

/** do animated zooming to point when image is loaded from playlist
 * 
 * @param zoomPoint {x, y}  point as a fraction of whole width/height
 * @param opts {
 *              originalScale - bool
 *              transition    - time seconds  
 *             }
 */
ImageTransform.prototype.zoomToPoint = function(zoomPoint, opts){
    
    if(this.cfg.noTransitions) return // --- >

    var imgInfo = this.getRenderedImgInfo()

    opts  = opts || {}
    opts.zoom_point = {
        x: imgInfo.width  * zoomPoint.x,
        y: imgInfo.height * zoomPoint.y,
    } 

    /* symulate click:   */
    // var e = {}
    // e.pageX = window.innerWidth *  zoomPoint.x
    // e.pageY = window.innerHeight * zoomPoint.y
    // this.zoom(e, 'click', opts)

    // opts  = opts || {}
    // opts.zoom_point = {
    //     x: window.innerWidth  * zoomPoint.x,
    //     y: window.innerHeight * zoomPoint.y,
    // }

    this.zoom(null, 'set', opts)
    // this.zoom(null, 'click', opts)
}

/** Z O O M   and   P A N
 * 
 * @param {Event} e - click or wheel event
 * @param {String} mode - wheel|click|set
 * @param {Object} opts - { zoom            - zoom in or out
 *                          originalScale, 
 *                          scale,
 *                          transition  - time sec
 *                        }
 */
ImageTransform.prototype.zoom = function(e, mode, opts){

    

    opts = opts || {};
    // var mode = opts.mode;
    if(!this.isImageLoaded) return // --- >


    var zoom_target = {x: 0, y: 0};
    var zoom_point =  {x: 0, y: 0};

    var center = {x: this.$container.clientWidth/2, y: this.$container.clientHeight/2}; // center of container
    var offset = this.$viewport.getBoundingClientRect()

    // zoom_point is relative to container:
    if (mode=='set'){
        // zoom_point = opts.zoom_point;
        zoom_point.x = opts.zoom_point.x + this.marg.x;
        zoom_point.y = opts.zoom_point.y + this.marg.y;
    } else {
        zoom_point.x = e.pageX - offset.left;
        zoom_point.y = e.pageY - offset.top;
    }
    
    // mode = mode==='set' ? 'click' : mode

    // 1. find zoom delta (in or out) based on mouse wheel direction or click with ALT
    var delta;
    switch (mode){
        // case 'wheel': delta = e.originalEvent.deltaY; break;
        case 'wheel': delta = e.deltaY; break;
        case 'click': delta = opts.zoom || 1; break;
        case 'set':   delta = 1; break;
        default: console.error('mode:`', mode, '` unknown');
    }
    delta = Math.max(-1, Math.min(1, delta)); // cap the delta to [-1,1] for cross browser consistency

log('margin:', this.marg, 'mode:', mode);  // NOTE: scale is not set yet , 'scale:', this.scale
log('INIT pos', this.pos);

    // 2. determine the point on where the image is zoomed in
    zoom_target.x = (zoom_point.x - this.pos.x)/this.scale;
    zoom_target.y = (zoom_point.y - this.pos.y)/this.scale;

    var scaleStepClick = this.cfg.scaleStepClick
    scaleStepClick = scaleStepClick===-1 ? this.scaleInContainer : scaleStepClick

    // 3. set scale value
    var newScale;
    switch (mode){
        case 'wheel': newScale = this.scale + this.cfg.scaleStepWheel * delta; break;
        case 'click':
            if (opts.originalScale){
                newScale = this.scaleInContainer; 
            }
            else {
                if (delta > 0) newScale = this.scale * scaleStepClick;
                if (delta < 0) newScale = this.scale / scaleStepClick;
            }
            break;
        case 'set': 
            if(opts.originalScale){ newScale = this.scaleInContainer;}
            else newScale = opts.scale;
            if(!newScale){console.error('zoom(set, opts) missing opts.scale')}
            // this.cfg.transition = opts.transition ? opts.transition : this.cfg.transition;
    }
    if (newScale < 1 ) return;
    if (newScale > this.cfg.maxScale) {logInfo('maxScale'); return; }

    // 3a. make sure that image area, not margin is clicked, needed only when scale=1
    if (this.scale==1){
        var imgAndMarg = {w: this.renderedImgInfo.width + this.marg.x, h:this.renderedImgInfo.height + this.marg.y};
        var errMsg='';
        if (zoom_point.x > imgAndMarg.w || zoom_point.x < this.marg.x) {errMsg  = 'X' }                                            
        if (zoom_point.y > imgAndMarg.h || zoom_point.y < this.marg.y) {errMsg += 'Y' }
        if(errMsg){logWarn( 'click out of img ', errMsg, zoom_point); return; }     // --- >
    }
    this.scale = newScale;

    // logWarn(mode, 'zoom_point:', zoom_point, 'zoom_target:', zoom_target)

    // 4. calculate pos x and y based on zoom
    this.pos.x = -zoom_target.x * this.scale + zoom_point.x;
    this.pos.y = -zoom_target.y * this.scale + zoom_point.y;

    log('ZOOM pos', this.pos, 'zoom:', zoom_target);
    // logInfo(mode, '$container:', this.pos, 'scale:', this.scale)

    // 5. center on clicked point
    if (mode==='click') {
        this.pos.x += (center.x - zoom_point.x); 
        this.pos.y += (center.y - zoom_point.y); 
    } else if (mode==='set'){
        this.pos.x += (center.x - zoom_point.x); 
        this.pos.y += (center.y - zoom_point.y); 
    }
    log('CENT pos', this.pos, 'scale:', this.scale);

    logInfo(mode, 'zoom_point:', zoom_point, 'zoom_target:', zoom_target, '$container:', this.pos, 'scale:', this.scale)


    /* *********************************************************************
        6. Make sure the image stays in its container area when zooming out    */

    adjustEdge.call(this);

    function adjustEdge(){
        var adj='ADJ: ';

        var quadClicked = this.quadClicked(zoom_point, this.$container);

        /** 6a. horizontal adjustments */
        /** 6aa. left    */
        // if (this.marg.x && mode=='click' && quadClicked.left){
        if (this.marg.x && (mode=='click' || mode=='set') && quadClicked.left){     /** for edge adjust 'set' mode is the same as 'click' mode */
            if (this.pos.x > -this.marg.x * this.scale) {
                this.pos.x = - this.marg.x * this.scale;
                adj +='left+marg';
            }
        } else {
            if(this.pos.x > 0) {
                this.pos.x = 0;
                adj +='left';
            }
        }
        /** 6ab. right   */
        // if (this.marg.x && mode=='click'  && quadClicked.right){
        if (this.marg.x && (mode=='click' || mode=='set')  && quadClicked.right){
            var w = this.size.w - this.marg.x;
            if(this.pos.x + w*this.scale < this.size.w) {
                this.pos.x =  -(w*this.scale - w) + this.marg.x; 
                adj +=`right+marg w=${w}`;  
            }
        } else {
            if(this.pos.x + this.size.w * this.scale < this.size.w) {
                this.pos.x = -this.size.w * (this.scale-1);
                adj +='right';
            }
        }

        /** 6b. vertical adjustments  */
        /** 6ba. top      */
        // if (this.marg.y && mode=='click' && quadClicked.top){
        if (this.marg.y && (mode=='click' || mode=='set') && quadClicked.top){
            if (this.pos.y > -this.marg.y * this.scale) {
                this.pos.y = - this.marg.y * this.scale;
                adj +=' - top+marg';
            }
        } else {
            if(this.pos.y > 0) {
                this.pos.y = 0;
                adj +=' - top';
            }
        }
        // 6bb. bottom 
        // if (this.marg.y && mode=='click' && quadClicked.bottom){
        if (this.marg.y && (mode=='click' || mode=='set') && quadClicked.bottom){
            var h = this.size.h - this.marg.y;
            if(this.pos.y + h*this.scale < this.size.h) {
                this.pos.y = -(h*this.scale - h) + this.marg.y;
                adj +=` - bott+marg h=${h}`;  
            }
        } else {
            if(this.pos.y + this.size.h*this.scale < this.size.h) {
                this.pos.y = -this.size.h * (this.scale-1);
                adj +=' - bott';
            }
        }
        log('EDGE pos', this.pos, adj);
        logInfo('after adjustEdge()', '$container:', this.pos, adj)
    }

    // record zoom setting
    this.callbacks.zoomSetting({scale: this.scale, zoom_target})


    // 7. make CSS transform
    switch (mode){
        case 'wheel': this.$container.style.transition = 'unset'; break;
        case 'click': this.$container.style.transition = this.cfg.transition; break;
        case 'set':   this.$container.style.transition = opts.transition ? opts.transition : this.cfg.transition;
    }
    // log('mode', mode, 'transition:', this.$container[0].style.transition);
    

    // update transform
    // this.$container.css('transform', 'translate('+(this.pos.x)+'px, '+(this.pos.y)+'px) scale('+this.scale+','+this.scale+')');
    this.$container.style.transform = 'translate('+(this.pos.x)+'px, '+(this.pos.y)+'px) scale('+this.scale+','+this.scale+')';

    this.callbacks.showInfo.call(this, delta, zoom_point, zoom_target, center); // forDEBUG

    // var self=this; setTimeout(function(){ log('renderedImgInfo', self.renderedImgInfo);}, 1500);

};//end: ImageTransform.prototype.zoom()


ImageTransform.prototype.quadClicked = function  (clk, $rectElem){
        // which quadrant is clicked
    var clkQuad = {
        top:    clk.y < $rectElem.clientHeight/2 ? true : false,
        bottom: clk.y > $rectElem.clientHeight/2 ? true : false,
        left:   clk.x < $rectElem.clientWidth/2 ? true : false,
        right:  clk.x > $rectElem.clientWidth/2 ? true : false,
    };
    var quadTxt = clkQuad.top ? 'top' : clkQuad.bottom ? 'bott' : 'mid';
    quadTxt += clkQuad.left ? '-left' : clkQuad.right ? '-right': '-mid'; 
    log('quad=', quadTxt, clk);
    return clkQuad;
};

/**
 * move image using keyboard arrows
 * @param {*} arrow 
 */
ImageTransform.prototype.arrow = function(arrow){
    switch (arrow){
        case 'ArrowRight':  this.pos.x += this.cfg.arrowStep; break;
        case 'ArrowLeft':   this.pos.x -= this.cfg.arrowStep; break;
        case 'ArrowUp':     this.pos.y -= this.cfg.arrowStep; break;
        case 'ArrowDown':   this.pos.y += this.cfg.arrowStep; break;
    }

    this.$container.style.transition = 'unset';
    this.$container.style.transform = `translate(${(this.pos.x)+'px'}, ${(this.pos.y)+'px'}) scale(${this.scale}, ${this.scale})`;

    log('pos', this.pos);
};

ImageTransform.prototype.setZoom = function(settings){

};

/** clear transformations */
ImageTransform.prototype.clearTrans = function(opts){
    opts = opts || {};
    this.scale = 1;
    this.pos = {x: 0, y: 0};

    var transition = opts.noTransition ? 'none' : this.cfg.transition;

    this.$container.style.transition = 'transition'
    this.$container.style.transform = 'translate(0, 0) scale(1, 1)' 

    this.callbacks.showInfo.call(this, 0, null, null, null);
    this.$caption.style.bottom = '5px'
};
  
/**
 * get image size after being fit inside container
 * NOTE: returned value does not change after CSS transform
 *  @param {*} image OPTIONAL, needed when called from image-navigator
 */
ImageTransform.prototype.getRenderedImgInfo = function (image) {
    var img = image ? image : this.$image;
    var position = window.getComputedStyle(img).getPropertyValue('object-position').split(' ');
    // return this.getRenderedSize(
    return getRenderedSize(
                true,               // object-fit: contain (chyba?)
                img.width,          // scaled img width in container
                img.height,         // scaled img height in container
                img.naturalWidth,   // original image size
                img.naturalHeight,  // original image size
                position
    );
    
    function getRenderedSize(contains, cWidth, cHeight, width, height, position){
        var oRatio = width / height,
            cRatio = cWidth / cHeight,
            pos0 = parseInt(position[0]),
            pos1 = parseInt(position[1]);
        return function() {
            if (contains ? (oRatio > cRatio) : (oRatio < cRatio)) {
                this.width = cWidth;
                this.height = cWidth / oRatio;
            } else {
                this.width = cHeight * oRatio;
                this.height = cHeight;
            }      
            this.left = (cWidth - this.width)*(pos0/100);
            this.right = this.width + this.left;

            this.top = (cHeight - this.height)*(pos1/100);
            this.bottom = this.height + this.top;

            // this.position = position;
            return this;
        }.call({});
    }
};

ImageTransform.prototype.ShowErrorMessage = function(txt){

    this.$caption.style.bottom = '50%'
    this.$caption.innerHTML = txt
}

ImageTransform.prototype.help = function(){
    return `first click - set focus
      click - zoom in
  alt+click - zoom out
shift+click - 1:1 scale
 crtl+click - remove transformations
          X - remove transformations
 mousewheel - zoom to point
          M - toggle mouse playlist navigation
          R - rotate
          C - toggle click mode: pick zoom Center point | zoom
     arrows - move zoomed img`
//    callbacks.showHelp(txt)
}

export default ImageTransform

/**  helper */
