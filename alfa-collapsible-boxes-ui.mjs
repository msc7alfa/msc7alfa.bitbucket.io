/*! alfa-collapsible-boxes animated  19.3.28  es6 module Copyright (c) 2018 alfalabs.net license MIT */

/** manages collapsible boxes
 * 
 *** example of group:  ******************
 * 
    <script>
        'use strict';
        var myBoxes1; // declare as global here, because script type="module" has local scope

        // public methods
        myBoxes1.openAll({noAnim:true})
        myBoxes1.openById('box2', {noAnim:true})
    </script>
    <script type="module">
        // create instance, elementsSelector is a class selector to group boxes by
        myBoxes1 = new AlfaCollapsibleBoxes({
            elementsSelector: '.alfa-cbox1',
            oneAtATime: false,      // accordeon mode if true
            boxesClosed: true,       // all boxes are collapsed on initial load (good for menu items and subitems)
            controlsSelector: '.alfa-collapsible-boxes-controls'
        });
    </script>
 
    <section class="alfa-cbox1">
        <div alfa-collapsible-box-header>Item One</div>
        <div alfa-collapsible-box-content>First Item content</div>
    </section>
    <section class="alfa-cbox1">
            <div alfa-collapsible-box-header>Item Two</div>
            <div alfa-collapsible-box-content>Second Item content</div>
        </section>
    <section class="alfa-cbox1">
        <div alfa-collapsible-box-header>Item Three</div>
        <div alfa-collapsible-box-content>Thitd Item content<br></div>
    </section>

 *
 *** example of a box ***********
 * 
    <section 
        class="alfa-cbox1"  - the box group will have the same class
        box-closed          - state on page load
        clear-content-btn   - add button to clear content of <div alfa-collapsible-box-content>
        >
        <div alfa-collapsible-box-header>Item One</div>
        <div alfa-collapsible-box-content>First Item content</div>
    </section>
 * 
  */

 import AlfaConsoleLog from './alfa-console-log.mjs'; 
 var alfaConsoleLog = new AlfaConsoleLog('alfa-collapsible-boxes-ui')
 var log =     alfaConsoleLog.log.bind(alfaConsoleLog)
 var logWarn = alfaConsoleLog.logWarn.bind(alfaConsoleLog)
 var logErr =  alfaConsoleLog.logErr.bind(alfaConsoleLog)


 const $dqs = document.querySelector.bind(document);
 const $dce = document.createElement.bind(document);

export default (function(){
    
    return class {      
      
        setDefaultSettings(){    
            this.els = [];
            this.events = { // array of callbackFn names
                'elementSelected': [],
                'elementOpened': [],
                'elementUnselected': [],
                'elementClosed': []
            };
            this.transitionendevent = this.transitionendEventName();
            this.oneAtATime = true;
            this.noAnim = false;
            // this.openedClass = 'opened'; // in case of accordeon where oneAtATime=true one item is 'opened' selected and had class 'selected'
            this.lastClickedClass = 'last-clicked';
            this.headerSelector =   '[alfa-collapsible-box-header]';
            this.contentSelector =  '[alfa-collapsible-box-content]';
            this.controlsSelector = '[alfa-collapsible-boxes-controls]';
        }

        /**
         * @param {*} settings -  from constructor function arguments
         */
        setCustomSettings(settings){
           
            Object.assign(this, settings); return;

            // polyfill for ES5
            var keys = Object.keys(settings);
            for(var i=0; i<keys.length; i++){
                this[ keys[i] ] = settings[ keys[i] ];
            }
        }

        constructor(settings){    

            // "global" settings for the group:
            this.setDefaultSettings();
            this.setCustomSettings(settings); 
            elementSelectorForGroup = this.elementsSelector;

            var elementsInGroup = document.querySelectorAll(this.elementsSelector);
            if(elementsInGroup.length===0) {logErr('constructor()', `no elemets with selector '${this.elementsSelector}' found!`); return;}

            log('constructor()', settings);

            this.insertStyleTag();
    
            [].forEach.call(elementsInGroup, (el, i)=>{
                
                this.els.push({
                    wrapper: el,
                    header: el.querySelector(this.headerSelector),
                    content: el.querySelector(this.contentSelector)
                    // header: el.querySelector(this.headerSelector),
                    // content: el.querySelector(this.contentSelector)
                });

                if (this.oneAtATime || this.boxesClosed){ // accordeon mode or all boxes closed:
                    this.els[i].content.style.height = 0;
                } else { // multi open mode
                    if (this.els[i].wrapper.hasAttribute('box-closed')){
                        this.els[i].content.style.height = 0;
                    } else {
                        this.els[i].isOpen = true;
                        this.els[i].selected = true;
                        // this.els[i].wrapper.classList.add(this.openedClass);
                        this.els[i].wrapper.setAttribute('opened', '');
                    }
                }
                this.els[i].header.setAttribute('title', 'click to toggle expand-collapse');
                this.createClearContentBtn(this.els[i]);
            });//end: forEach
    
            this.attachEvents();

            this.controlsBtns();
        }
      
        // add extra HTML features
        createClearContentBtn(el){
            if(el.wrapper.hasAttribute('clear-content-btn')){
                var btn = document.createElement('div');
                btn.addEventListener("click", this.clearContent.bind(this, el.content));
                btn.classList.add('btn');
                btn.setAttribute('title', 'Clear content');
                el.header.append(btn);
            }
        }
        clearContent(content, ev){
            ev.stopImmediatePropagation();
            // log(this);
            content.innerHTML='';
        }


        controlsBtns(){
            var ctrls = $dqs(this.controlsSelector);
            if(!ctrls) return;

            this.openAllBtn = $dqs(`${this.controlsSelector} [open-all]`);
            this.closeAllBtn = $dqs(`${this.controlsSelector} [close-all]`);
            
            if(this.openAllBtn){ this.openAllBtn.addEventListener('click', function(ev){ this.openAll.call(this);}.bind(this) );}
            if(this.closeAllBtn){this.closeAllBtn.addEventListener('click',function(ev){ this.closeAll.call(this);}.bind(this) );}
            // if(this.openAllBtn){ this.openAllBtn.addEventListener('click', this.openAll.bind(this));}
            // if(this.closeAllBtn){this.closeAllBtn.addEventListener('click',this.closeAll.bind(this));}
        }

        // public methods /////////////////////////////

        /** public method
         * @param {*} id    - box-id
         * @param {*} opts 
         */
        openById(id, opts){
            var elem = this.findElemById(id);
            if(elem) this.open(elem, opts);
        }
        /** public method
         * @param {*} id    - box-id 
         * @param {*} opts 
         */
        closeById(id, opts){
            var elem = this.findElemById(id);
            if(elem) this.close(elem, opts);
        }
        /** public method
         * @param {mouseEvent} ev
         * @param {*} opts 
         */
        closeAll(opts){
            this.els.forEach((el, i)=>{
                if (el.isOpen) {this.close(el, opts);} 
            });
            this.flipBtnsState(this.openAllBtn, this.closeAllBtn);
            // log('closeAll()');  //  ,'closed:', j
        }

        /** public method
         * @param {mouseEvent} ev
         * @param {*} opts 
         */
        openAll(opts){
            opts = opts || {};
            opts.quiet = true;  // do not fire elementSelected
            this.els.forEach((el, i)=>{
                if (!el.isOpen) this.open(el, opts);
            });
            this.flipBtnsState( this.closeAllBtn, this.openAllBtn);
        }

        flipBtnsState(onBtn, offBtn){
            if(offBtn) {offBtn.setAttribute('disabled', '');}
            if(onBtn) {onBtn.removeAttribute('disabled');}
        }

        //  other methods

        transitionendEventName(){
            var i,
                el = document.createElement('div'),
                transitions = {
                    'transition': 'transitionend',
                    'OTransition': 'otransitionend',
                    'MozTransition': 'transitionend',
                    'WebkitTransition': 'webkitTransitionEnd'
                };

            for (i in transitions) {
                if (transitions.hasOwnProperty(i) && el.style[i] !== undefined) {
                return transitions[i];
                }
            }
        }
  
        expand(el, opts){
            opts = opts || {};
            if (this.noAnim || opts.noAnim) {el.content.style.height = 'auto'; el.isOpen = true; return;} // --- >


            function resetHeight(ev){
                if(ev.target != el.content) return;
                el.content.removeEventListener(this.transitionendevent, bindEvent);
        
                if(!el.isOpen) return;

                requestAnimationFrame(()=>{      
                    el.content.style.transition = '0';
                    el.content.style.height = 'auto';
        
                    requestAnimationFrame(()=>{
                    el.content.style.height = null;
                    el.content.style.transition = null;
        
                    this.fire("elementOpened", el);
                    });
                });
            }
    
            var bindEvent = resetHeight.bind(this);
            el.content.addEventListener(this.transitionendevent, bindEvent);
    
            el.isOpen = true;

            el.content.style.height = el.content.scrollHeight + "px";
        }
    
        collapse(el, opts){
            opts = opts || {};

            function endTransition(ev){     
                if(ev.target != el.content) return;
                el.content.removeEventListener(this.transitionendevent, bindEvent);
        
                if(el.isOpen) return;
        
                this.fire("elementClosed", el);
            }
    
            var bindEvent = endTransition.bind(this);
            el.content.addEventListener(this.transitionendevent, bindEvent);
    
            el.isOpen = false;   
            
            if (this.noAnim || opts.noAnim) {el.content.style.height = 0; return; } // --- >
    
            requestAnimationFrame(()=>{
                el.content.style.transition = '0';
                el.content.style.height = el.content.scrollHeight + "px";
        
                requestAnimationFrame(()=>{
                    el.content.style.transition = null;
                    el.content.style.height = 0;
                });
            });
        }
    
        open(el, opts){
            opts = opts || {};
            log('open()', `"${el.header.innerText}"`, el);
           
            if (!opts.quiet){
                this.fire("elementSelected", el);
                el.selected = true;
            }
            this.expand(el, opts);
            // el.wrapper.classList.add(this.openedClass);
            el.wrapper.setAttribute('opened', '');
            if(this.closeAllBtn) this.closeAllBtn.removeAttribute('disabled');
        }
    
        close(el, opts){
            log('close()', `"${el.header.innerText}"`, el);
            el.selected = false;
            this.fire("elementUnselected", el);
            this.collapse(el, opts);
            // el.wrapper.classList.remove(this.openedClass);
            el.wrapper.removeAttribute('opened');
            if(this.openAllBtn) this.openAllBtn.removeAttribute('disabled');
        }
 
        // header click event handler:
        // also called from toggleBox(), ev is not available then
        toggle(el, ev){
            log('toggle()', `"${ev ? el.header.innerText : ''}"`, el, ev);

            if(ev) ev.stopImmediatePropagation();

            // highlight this element as 'last-clicked'
            this.lastClicked(el);

            
            if(el.isOpen){
                this.close(el);
            } else {
                this.open(el);
                if(this.oneAtATime){        
                    this.els.filter(e=> e!=el && e.selected).forEach(e=>{
                        this.close(e);
                    });
                }
            }
            
            // if(el.selected){ // did not work after openAll()
            //     this.close(el);
            // } else {
            //     this.open(el);
            //     if(this.oneAtATime){        
            //         this.els.filter(e=> e!=el && e.selected).forEach(e=>{
            //             this.close(e);
            //         });
            //     }
            // }
            
        }   

        /** called as a method from outside. NOT title bar event handler
         * 
         * @param {*} boxEl 
         */
        toggleBox(boxID){
            var elem = this.findElemById(boxID);
            this.toggle(elem);
        }

        /**
         * 
         * @param {*} elHeader - the <div.alfa-collapsible-box-header>
         * @param {*} wrapper - optional <section> if called from here
         */
        lastClicked(elHeader, wrapper){
            // 1. clear 'last-clicked' class from all
            var lastClickedClass = this.lastClickedClass;
            this.els.forEach( function(el){
                el.wrapper.classList.remove(lastClickedClass);
            });
            // 2. set 'last-clicked' class
            // elHeader.wrapper.classList.add(lastClickedClass);
            wrapper = wrapper || elHeader.wrapper;
            wrapper.classList.add(lastClickedClass);
        }


        findElemById(id){
            var elem = this.els.find((el, i)=>{
                if (el.wrapper.getAttribute('box-id')===id) return el;
            });
            if (elem) return elem;
            else {logErr({message: `box '${id}' not found!`}); return null;}
        }
    
        attachEvents(){    
            this.els.forEach((el, i)=>{
                el.header.addEventListener("click", this.toggle.bind(this, el));
            });
        }
    
        fire(eventName, el){
            var callbacks = this.events[eventName];
            for(var i=0; i<callbacks.length; i++){
                callbacks[i]( el );
            }
        }
    
        on(eventName, cb){
            if( !this.events[eventName] ) return;       
            this.events[eventName].push(cb);
        }


        // errorLog(err){
        //     console.error(`[alfa-collapsible-boxes]`, err.message);
        // }

        insertStyleTag(){
            var attr = 'alfa-collapsible-boxes-ui'
            /** do not duplicate <style> tags */
            if ($dqs(`style[${attr}]`)) return;

            var styleTag = $dce('style');
            styleTag.setAttribute(attr, '')

            styleTag.innerHTML = `
        /* REQUIRED CSS */
            [alfa-collapsible-box-content] {
                height: auto;
                overflow: hidden;
                transition: height .5s;
            }
        /* generic CSS */
            [alfa-collapsible-box-header] {
                background: gainsboro; 
                border-bottom: 1px solid silver; 
                cursor:pointer;
                padding-left: 15px;
                margin-bottom: 1px;
            }
            [alfa-collapsible-box-header]:hover{
                background: silver;
            }
        /* control buttons */
            [alfa-collapsible-boxes-controls] [open-all], 
            [alfa-collapsible-boxes-controls] [close-all]{color: green; cursor:pointer;}
            [alfa-collapsible-boxes-controls] [open-all][disabled], 
            [alfa-collapsible-boxes-controls] [close-all][disabled]{color: grey; cursor:default;}
        `;
        
            $dqs('head').append(styleTag);  /* calling ('body').append() will add your new styles to the bottom of the page and override any existing ones */

        }
    
    };//end; class

})();

var elementSelectorForGroup;

// // Helper function allows elements and string selectors to be used
// // interchangeably. In either case an element is returned.
// function elementOrSelector (elorsel){
//     function isString(v){return typeof v === 'string' || v instanceof String;}

//     if (isString(elorsel)) {
//         const elem = $dqs(elorsel);
//         if (!elem) {logErr(`Selector '${elorsel}' did not match a DOM element`); }
//         return elem;
//     }
//     return elorsel;
// }

/**  helper */
// function log(){
//     return;
//     var args = Array.prototype.slice.call(arguments); 
//     args.unshift(`%c[alfa-collapsible-boxes] ${elementSelectorForGroup}`, 'color:blue');
//     console.log.apply(null, args);
// }
// function logErr(){
//     var args = Array.prototype.slice.call(arguments); 
//     args.unshift(`%c[alfa-collapsible-boxes] ERR: ${elementSelectorForGroup}`, 'color:red; font-weight: bold;');
//     console.log.apply(null, args);
// }