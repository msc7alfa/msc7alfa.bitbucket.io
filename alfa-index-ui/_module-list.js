/** for node links to work, the must be setup in _node-server/a-routes.js
 * module-name,     type, node, description  */

var list =  `
alfa-index-ui           ,,,     this page code
AAA-template            ,, node, template 
AAA-template-ui         ,,     , template with UI

alfa-fetch              ,, node, fetch with callbacks, timeout, wait spinner
alfa-foreach-sync            ,, node, synchronously enumerate an Array
alfa-databinding
alfa-setproperty-bypath ,,,      set or create property of an object

alfa-drawer-ui          ,,,     UI for a page with animated sliding drawers
image-transform-ui      ,,,     animated zoom

alfa-json-input-pop-ui  ,,,     input popup box attached to an html element
alfa-json-box-ui        ,,,     collapsible boxes for data display in JSON format, not animated
alfa-json-edit-ui       ,,,     &lt;textarea&gt; for JSON editing and validating
alfa-json-pretty-ui     ,,,     display JSON formatted and colorized

alfa-modal-spinner-ui   ,,,     page service for displaying wait spinner
alfa-simple-pop-ui      ,,,     popup box attached to an html element
alfa-modal-popup-ui     ,,,     page service for displaying popup boxes
alfa-audio-clips-ui     ,,,     service to play audio clips
alfa-collapsible-boxes-ui   ,,, animated collapsible boxes
`

export default list
