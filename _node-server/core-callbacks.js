/*
    usage:
    var coreCallbacks = require('./core-callbacks')(req, res , {MODULE_NAME});
*/
var MODULE_NAME;

module.exports = function(req, res, opts){
    opts = opts || {};
    MODULE_NAME = `[${opts.MODULE_NAME}]` || '';
    
    const log = console.log();

    /** callbacks:    */
    return {
        sendResutl:    function(result){
            if (res){
                if (res.headersSent){console.error('headersSent! res:', res);}
                else res.send(result);
            } else {
                log(`${MODULE_NAME} sendResult() non Http request, result:`, result); // forDEBUG only !
            }
        },

        onDone:     function(result){log('(onDone)', result);   },  //  res.send(result);

        onError:    function(err){log(`${MODULE_NAME} (onError)`,    err);}, // this should be only text message, like onInfo, but can be in red color
        onWarn:     function(msg){log(`${MODULE_NAME} (onWarn)`,     msg);},
        onProgress: function(msg){log(`${MODULE_NAME} (onProgress)`, msg);},
        onInfo:     function(msg){log(`${MODULE_NAME} (onInfo)`,     msg);}

    };

};
