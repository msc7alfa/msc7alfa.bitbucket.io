﻿'use strict';

const app = require('./app'); // ExpressJS router
// const app = require('./app'); // ExpressJS router
const http = require('http');

const port = 3001; 


app.set('port', port);

const server = http.createServer(app);

server.listen(port, function listening() {

    console.log('-------------------------------------------------------------'),
    console.log('---  \x1b[31m\x1b[1m NodeJS Server \x1b[0m    with Express                      ---'),
    console.log(`---   Server Listening on port \x1b[34m\x1b[1m:${server.address().port} \x1b[0m                     ---`),
    console.log(`---   public WWW folder: \x1b[32m\x1b[1m ${app.locals.www} \x1b[0m   `)
    console.log('-------------------------------------------------------------');
    

});//end: server.listen()

