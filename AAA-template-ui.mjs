/** example for UI element */



/** dependencies */
// alfa-simple-pop-ui.mjs

import AlfaConsoleLog from '../alfa-console-log.mjs'; 
var alfaConsoleLog = new AlfaConsoleLog('AaaTemplateUI')
var log =     alfaConsoleLog.log.bind(alfaConsoleLog)
var logWarn = alfaConsoleLog.logWarn.bind(alfaConsoleLog)
var logErr =  alfaConsoleLog.logErr.bind(alfaConsoleLog)


const $dqs = document.querySelector.bind(document); 
const $dce = document.createElement.bind(document); 


/** constructor 
 * 
 *  @param {String} elementSelector - a CSS selector, unique for element to create
 *  @param {Object} options - added to this.cfg, (also cfg optinos can be html attributes, they will be camel capitalized)
 */

 /**
  * 
  * @param elementSelector 
  * @param options 
  * 
        D O   N O T  ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !

        export default function AaaTemplateUI(args){
            //code
        }
        this will not work with REQUIRE-es6module.js converter

        D O :

        function AaaTemplateUIargs){
            // code
        }
        at the end:
        export default AaaTemplateUI
  * 
  */
function AaaTemplateUI(elementSelector, options){

    var defaults = {
        author: 'alfalabs.net',
        moduleName: 'AaaTemplateUI'
    }

    /** ui element has an element on page */
    this.elementSelector = elementSelector;
    
    this.container = (elementSelector!==null) ? $dqs(elementSelector) : options.containerElement
    alfaConsoleLog.setCfg({id: this.container.getAttribute('id')})

    this.setCfgValues(options, defaults)  /** element configuration comes from options and html attributes */
   

    this.createElement()
    this.insertStyleTag()

    alfaConsoleLog.setCfg({id: this.container.getAttribute('id')})
    log('constructor() completed')
}
/** get configuration from defauls, options and HTML element attributes  */
AaaTemplateUI.prototype.setCfgValues = function(options, defaults){

    options = options || {}
    this.cfg = Object.assign({}, defaults, options)

    /** get cfg from html element attributes */
    Array.from(this.container.attributes).forEach(function(attr){
        var name = camelize(attr.name)
        this.cfg[name] = attr.value
    }.bind(this));

    // log('getAttributes() this.cfg:', this.cfg)
}

AaaTemplateUI.prototype.exampleMethod = function(){
    log('exampleMethod()', this.cfg);
}

AaaTemplateUI.prototype.createElement = function(){
    this.container.innerHTML = 'aaa-template-ui'+this.elementSelector

    /** assing attributes */
    if(this.cfg.elemCss){this.container.style = this.cfg.elemCss}
}

AaaTemplateUI.prototype.clear = function(){
    this.clearNode(this.container)
}
AaaTemplateUI.prototype.clearNode = function(myNode){
    while (myNode.firstChild) {
        myNode.removeChild(myNode.lastChild);
    }
}

/** if needed */
import popupMesage from './alfa-simple-pop-ui.mjs';
AaaTemplateUI.prototype.popMessage = popupMesage;


AaaTemplateUI.prototype.insertStyleTag = function(){
    var styleAttr = 'AAA-template-ui'+'-style';
    if($dqs(`[${styleAttr}]`)) return;  /** do not insert style if already exists */

    var styleTag = $dce('style');
    styleTag.setAttribute(styleAttr, '');

    var sel = this.elementSelector;

    styleTag.innerHTML = `
        ${sel} {border: 1px solid green}

    `;

    /* NOTE: calling body.append will add your new styles to the bottom of the page and override any existing ones */
    $dqs('head').append(styleTag);
}

export default AaaTemplateUI;



    // helpers

    /** not used, but interesting: https://gist.github.com/jpetitcolas/4481778
     *  from: https://ourcodeworld.com/articles/read/608/how-to-camelize-and-decamelize-strings-in-javascript
     * @param text 
     * delimiter: underscore or hyphen
     */
    function camelize(text) {
        return text.replace(/^([A-Z])|[\s-_]+(\w)/g, function(match, p1, p2, offset) {
            if (p2) return p2.toUpperCase();
            return p1.toLowerCase();        
        });
    }

    // function log(){
    //     // return;
    //     var args = Array.prototype.slice.call(arguments); 
    //     args.unshift('%c[aaa-template-ui]', 'color:blue');
    //     console.log.apply(null, args);
    // }
    // function logWarn(){
    //     var args = Array.prototype.slice.call(arguments); 
    //     args.unshift('%c[aaa-template-ui] WARNING:', 'color:darkorange; font-weight:bold;');
    //     console.log.apply(null, args);
    // }
    // function logErr(){
    //     var args = Array.prototype.slice.call(arguments); 
    //     args.unshift('%c[aaa-template-ui] ERROR:', 'color:red; font-weight:bold;');
    //     console.log.apply(null, args);
    // }